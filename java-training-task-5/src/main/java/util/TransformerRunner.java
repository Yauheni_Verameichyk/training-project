package util;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

public class TransformerRunner
{
    private static final String BASE_PATH = "src\\main\\resources\\xml\\";
    private static final String SCHEMA_PATH = BASE_PATH + "schema.xsl";
    private static final String OLD_XML = BASE_PATH + "paper.xml";
    private static final String NEW_XML = BASE_PATH + "transformeredPaper.xml";

    private static final Logger logger = Logger.getLogger(TransformerRunner.class);

    public static void main(String[] args)
    {
        try
        {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(SCHEMA_PATH));
            transformer.transform(new StreamSource(OLD_XML), new StreamResult(NEW_XML));
            System.out.println("File " + OLD_XML + " was transformed into " + NEW_XML);
        }
        catch (TransformerException e)
        {
            logger.error("Impossible to transform file ", e);
        }
    }

}
