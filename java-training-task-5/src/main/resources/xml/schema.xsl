<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="xml" />
   <xsl:template match="/">
      <papers>
         <xsl:apply-templates />
      </papers>
   </xsl:template>
   <xsl:template match="paper">
      <paper>
         <title>
            <xsl:value-of select="title" />
         </title>
         <type>
            <xsl:value-of select="type" />
         </type>
         <monthly>
            <xsl:value-of select="monthly" />
         </monthly>
         <color>
            <xsl:value-of select="chars/color" />
         </color>
         <pageNumber>
            <xsl:value-of select="chars/pageNumber" />
         </pageNumber>
         <glossy>
            <xsl:value-of select="chars/glossy" />
         </glossy>
         <index>
            <xsl:value-of select="chars/index" />
         </index>
      </paper>
   </xsl:template>
</xsl:stylesheet>