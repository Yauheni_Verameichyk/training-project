package factory;

import static factory.QuadrilateralFactory.createQuadrilateral;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static util.PointUtil.isSelfIntercected;
import static util.ReaderUtil.readPointsFromFile;

import org.junit.Test;

import entity.Quadrilateral;

public class QuadrilateralFactoryTest
{
    private static final String BASE_PATH = "src\\test\\resources\\json\\";
    private static final String SELF_INTERCECTED_QUADRILATERAL = BASE_PATH + "selfIntercectedQuadrilateral.json";
    private static final String SELF_INTERCECTED_QUADRILATERAL2 = BASE_PATH + "selfIntercectedQuadrilateral2.json";

    @Test
    public void shouldReturnChangedQuadrilateral() throws Exception
    {
        Quadrilateral actual = createQuadrilateral(readPointsFromFile(SELF_INTERCECTED_QUADRILATERAL));
        assertThat(isSelfIntercected(actual.getPoints()), equalTo(false));
    }

    @Test
    public void shouldReturnChangedQuadrilateralIfDifferentSidesIntercected() throws Exception
    {
        Quadrilateral actual = createQuadrilateral(readPointsFromFile(SELF_INTERCECTED_QUADRILATERAL2));
        assertThat(isSelfIntercected(actual.getPoints()), equalTo(false));
    }
}
