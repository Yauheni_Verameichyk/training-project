package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static util.ReaderUtil.readPointFromFile;
import static util.ReaderUtil.readPointsFromFile;

import org.junit.Test;

import entity.Point;
import exception.FileReadException;

public class ReaderTest
{
    private static final String SQUARE = "src\\test\\resources\\json\\square.json";
    private static final String WRONG_PATH = "src\\test\\resources\\json\\sqGuare.json";
    private static final String POINT = "src\\test\\resources\\json\\point11.json";

    @Test
    public void shouldReturnSquare() throws FileReadException
    {
        Point[] expected = { new Point(1, 1), new Point(1, 3), new Point(3, 3),
                new Point(3, 1) };
        Point[] actual = readPointsFromFile(SQUARE);
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void shouldFailIfFilePathToPointsIncorrect()
        throws FileReadException
    {
        try
        {
            readPointsFromFile(WRONG_PATH);
            fail();
        }
        catch (FileReadException e)
        {
            assertThat(e.getMessage(), equalTo("Unable to read file "
                    + WRONG_PATH));
        }
    }

    @Test
    public void shouldReturnPoint() throws FileReadException
    {
        Point expected = new Point(1, 1);
        Point actual = readPointFromFile(POINT);
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void shouldFailIfFilePathToPointIncorrect()
        throws FileReadException
    {
        try
        {
            readPointFromFile(WRONG_PATH);
            fail();
        }
        catch (FileReadException e)
        {
            assertThat(e.getMessage(), equalTo("Unable to read file "
                    + WRONG_PATH));
        }
    }
}
