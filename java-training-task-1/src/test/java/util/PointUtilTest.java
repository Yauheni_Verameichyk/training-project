package util;

import static java.lang.Math.PI;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static util.PointUtil.areThreePointsOnOneLine;
import static util.PointUtil.changePointsOrder;
import static util.PointUtil.checkIfNotTriangle;
import static util.PointUtil.getAngleBetweenThreePoints;
import static util.PointUtil.getLineLength;
import static util.PointUtil.getMidPointBetweenThreePoints;
import static util.PointUtil.isSelfIntercected;
import static util.PointUtil.getPointsToSwap;
import static util.ReaderUtil.readPointFromFile;
import static util.ReaderUtil.readPointsFromFile;

import org.junit.Test;

import entity.Point;
import exception.FileReadException;
import exception.UnxpectedFigureException;

public class PointUtilTest
{
    private static final String BASE_PATH = "src\\test\\resources\\json\\";
    private static final String POINT11 = BASE_PATH + "point11.json";
    private static final String POINT13 = BASE_PATH + "point13.json";
    private static final String POINT15 = BASE_PATH + "point15.json";
    private static final String POINT33 = BASE_PATH + "point33.json";
    private static final String TRIANGLE = BASE_PATH + "triangle.json";
    private static final String SQUARE = BASE_PATH + "square.json";
    private static final String SELF_INTERCECTED_QUADRILATERAL = BASE_PATH + "selfIntercectedQuadrilateral.json";

    @Test
    public void shouldReturnTrueIfThreePointsOnOneLine()
        throws FileReadException
    {
        Point a = readPointFromFile(POINT11);
        Point b = readPointFromFile(POINT13);
        Point c = readPointFromFile(POINT15);
        assertThat(areThreePointsOnOneLine(a, b, c), equalTo(true));
    }

    @Test
    public void shouldFailIfNotQuadrilateral()
        throws Exception
    {
        try
        {
            checkIfNotTriangle(readPointsFromFile(TRIANGLE));
            fail();
        }
        catch (UnxpectedFigureException e)
        {
            assertThat(e.getMessage(), equalTo("Three points are located on one line"));
        }
    }

    @Test
    public void shouldReturnAngle() throws FileReadException
    {
        Point a = readPointFromFile(POINT11);
        Point b = readPointFromFile(POINT13);
        Point c = readPointFromFile(POINT33);
        double expected = PI / 2;
        double actual = getAngleBetweenThreePoints(a, b, c);
        assertEquals(expected, actual, 0.01);
    }

    @Test
    public void shouldReturnLengthBetweenTwoPoints() throws FileReadException
    {
        Point a = readPointFromFile(POINT11);
        Point b = readPointFromFile(POINT13);
        double expected = 2;
        double actual = getLineLength(a, b);
        assertEquals(expected, actual, 0.01);
    }

    @Test
    public void shouldReturnLineMidpoint() throws FileReadException
    {
        Point a = readPointFromFile(POINT11);
        Point b = readPointFromFile(POINT33);
        Point expected = new Point(2, 2);
        Point actual = getMidPointBetweenThreePoints(a, b);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldChangePointsOrder()
        throws Exception
    {
        Point[] expected = readPointsFromFile(SQUARE);
        Point[] actual = readPointsFromFile(SELF_INTERCECTED_QUADRILATERAL);
        assertThat(changePointsOrder(actual, getPointsToSwap(actual)), equalTo(expected));
    }

    @Test
    public void shouldReturnFalseIfQuadrilateralNotSelfIntercected()
        throws Exception
    {
        Point[] points = readPointsFromFile(SQUARE);
        assertThat(isSelfIntercected(points), equalTo(false));
    }

    @Test
    public void shouldReturnTrueIfQuadrilateralSelfIntercected()
        throws Exception
    {
        Point[] points = readPointsFromFile(SELF_INTERCECTED_QUADRILATERAL);
        assertThat(isSelfIntercected(points), equalTo(true));
    }
}
