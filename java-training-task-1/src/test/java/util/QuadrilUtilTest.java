package util;

import static factory.QuadrilateralFactory.createQuadrilateral;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static util.QuadrilUtil.areAnglesEqual;
import static util.QuadrilUtil.areSidesEqual;
import static util.QuadrilUtil.getArea;
import static util.QuadrilUtil.getPerimeter;
import static util.QuadrilUtil.isConvex;
import static util.QuadrilUtil.isSquare;
import static util.ReaderUtil.readPointsFromFile;

import org.junit.Test;

import entity.Quadrilateral;
import exception.FileReadException;
import exception.UnxpectedFigureException;

public class QuadrilUtilTest
{
    private static final String BASE_PATH = "src\\test\\resources\\json\\";
    private static final String SQUARE = BASE_PATH + "square.json";
    private static final String RHOMB = BASE_PATH + "rhomb.json";
    private static final String NON_CONVEX_QUADRILATERAL = BASE_PATH
            + "nonConvexQuadrilateral.json";

    @Test
    public void shouldReturnArea() throws FileReadException,
        UnxpectedFigureException
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(SQUARE));
        double expected = 4;
        double actual = getArea(quad);
        assertEquals(expected, actual, 0.01);
    }

    @Test
    public void shouldReturnPerimeter() throws Exception
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(SQUARE));
        double expected = 8;
        double actual = getPerimeter(quad);
        assertEquals(expected, actual, 0.01);

    }

    @Test
    public void shouldReturnTrueIfQuadrilateralConvex()
        throws FileReadException, UnxpectedFigureException
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(SQUARE));
        assertThat(isConvex(quad), equalTo(true));

    }

    @Test
    public void shouldReturnFalseIfQuadrilateralNotConvex()
        throws Exception
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(NON_CONVEX_QUADRILATERAL));
        assertThat(isConvex(quad), equalTo(false));
    }

    @Test
    public void shouldReturnTrueIfSidesEqual() throws Exception
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(SQUARE));
        assertThat(areSidesEqual(quad), equalTo(true));
    }

    @Test
    public void shouldReturnFalseIfSidesNotEqual() throws Exception
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(NON_CONVEX_QUADRILATERAL));
        assertThat(areSidesEqual(quad), equalTo(false));
    }

    @Test
    public void shouldReturnTrueIfAnglesEqual() throws Exception
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(SQUARE));
        assertThat(areAnglesEqual(quad), equalTo(true));
    }

    @Test
    public void shouldReturnFalseIfAnglesNotEqual() throws Exception
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(NON_CONVEX_QUADRILATERAL));
        assertThat(areAnglesEqual(quad), equalTo(false));
    }

    @Test
    public void shouldReturnTrueIfQuadrilateralSquare()
        throws Exception
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(SQUARE));
        assertThat(isSquare(quad), equalTo(true));
    }

    @Test
    public void shouldReturnFalseIfQuadrilateralNotSquare()
        throws Exception
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(NON_CONVEX_QUADRILATERAL));
        assertThat(isSquare(quad), equalTo(false));
    }

    @Test
    public void shouldReturnFalseIfQuadrilateralRhomb()
        throws Exception
    {
        Quadrilateral quad = createQuadrilateral(readPointsFromFile(RHOMB));
        assertThat(isSquare(quad), equalTo(false));
    }
}
