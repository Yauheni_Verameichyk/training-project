package exception;

public class UnxpectedFigureException extends Exception
{
    private static final long serialVersionUID = 1621472213969812095L;

    public UnxpectedFigureException()
    {
    }

    public UnxpectedFigureException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public UnxpectedFigureException(String message)
    {
        super(message);
    }

    public UnxpectedFigureException(Throwable cause)
    {
        super(cause);
    }

}
