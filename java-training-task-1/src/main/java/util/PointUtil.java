package util;

import static java.awt.geom.Line2D.linesIntersect;
import static java.awt.geom.Point2D.distance;
import static java.lang.Math.abs;
import static java.lang.Math.acos;
import static java.lang.Math.pow;

import entity.Point;
import exception.UnxpectedFigureException;

public class PointUtil
{

    public static boolean areThreePointsOnOneLine(Point a, Point b, Point c)
    {
        double minError = 0.001;
        return ((a.getX() == b.getX() & a.getX() == c.getX())
                || (a.getY() == b.getY() & a.getY() == c.getY())
                || abs((c.getX() - a.getX()) / (b.getY() - a.getY())
                        - (c.getY() - a.getY()) / (b.getX() - a.getX())) <= minError) ? true : false;
    }

    public static double getLineLength(Point a, Point b)
    {
        return distance(a.getX(), a.getY(), b.getX(), b.getY());
    }

    public static double getAngleBetweenThreePoints(Point a, Point b, Point c)
    {
        double angle = acos((pow((getLineLength(a, b)), 2)
                + pow((getLineLength(b, c)), 2) - pow((getLineLength(a, c)), 2))
                / getLineLength(a, b) * getLineLength(b, c));
        return angle;
    }

    public static Point getMidPointBetweenThreePoints(Point a, Point b)
    {
        double mx = (a.getX() + b.getX()) / 2;
        double my = (a.getY() + b.getY()) / 2;
        return new Point(mx, my);
    }

    public static void checkIfNotTriangle(Point[] points)
        throws UnxpectedFigureException
    {
        if (areThreePointsOnOneLine(points[0], points[1], points[2])
                || areThreePointsOnOneLine(points[1], points[2], points[3])
                || areThreePointsOnOneLine(points[0], points[1], points[3])
                || areThreePointsOnOneLine(points[0], points[2], points[3]))
        {
            throw new UnxpectedFigureException("Three points are located on one line");
        }
    }

    public static boolean isSelfIntercected(Point[] points)
    {
        return (linesIntersect(
                points[0].getX(), points[0].getY(),
                points[3].getX(), points[3].getY(), points[1].getX(),
                points[1].getY(), points[2].getX(), points[2].getY())
                || linesIntersect(
                        points[0].getX(), points[0].getY(), points[1].getX(),
                        points[1].getY(), points[2].getX(), points[2].getY(),
                        points[3].getX(), points[3].getY())) ? true : false;
    }

    public static int[] getPointsToSwap(Point[] points)
    {
        if (linesIntersect(
                points[0].getX(), points[0].getY(),
                points[3].getX(), points[3].getY(), points[1].getX(),
                points[1].getY(), points[2].getX(), points[2].getY()))
        {
            return new int[] { 2, 3 };
        }
        else
        {
            return new int[] { 1, 2 };
        }
    }

    public static Point[] changePointsOrder(Point[] points, int[] pointsToSwap)
    {
        Point temp = points[pointsToSwap[0]];
        points[pointsToSwap[0]] = points[pointsToSwap[1]];
        points[pointsToSwap[1]] = temp;
        return points;
    }
}
