package util;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.sin;
import static util.PointUtil.getAngleBetweenThreePoints;
import static util.PointUtil.getLineLength;
import static util.PointUtil.getMidPointBetweenThreePoints;

import entity.Point;
import entity.Quadrilateral;

public class QuadrilUtil
{

    public static double getPerimeter(Quadrilateral quad)
    {
        Point points[] = quad.getPoints();
        double perimeter = getLineLength(points[0], points[1])
                + getLineLength(points[1], points[2])
                + getLineLength(points[2], points[3])
                + getLineLength(points[3], points[0]);
        return perimeter;
    }

    public static boolean isConvex(Quadrilateral quad)
    {
        Point points[] = quad.getPoints();
        double minError = 0.1;
        return ((2 * PI - abs(getAngleBetweenThreePoints(points[0], points[1], points[2])
                + getAngleBetweenThreePoints(points[1], points[2], points[3])
                + getAngleBetweenThreePoints(points[2], points[3], points[0])
                + getAngleBetweenThreePoints(points[3], points[0], points[1]))) < minError) ? true : false;
    }

    public static double getArea(Quadrilateral quad)
    {
        Point points[] = quad.getPoints();
        Point midPoints[] = {
                getMidPointBetweenThreePoints(points[0], points[1]),
                getMidPointBetweenThreePoints(points[1], points[2]),
                getMidPointBetweenThreePoints(points[2], points[3]),
                getMidPointBetweenThreePoints(points[3], points[0]), };

        double area = 2
                * getLineLength(midPoints[0], midPoints[1])
                * getLineLength(midPoints[0], midPoints[3])
                * sin(getAngleBetweenThreePoints(midPoints[1], midPoints[0], midPoints[3]));
        return area;
    }

    public static boolean isSquare(Quadrilateral quad)
    {
        return (areSidesEqual(quad) && areAnglesEqual(quad)) ? true : false;
    }

    public static boolean areSidesEqual(Quadrilateral quad)
    {
        Point points[] = quad.getPoints();
        return (getLineLength(points[0], points[1]) == getLineLength(points[1], points[2])
                && getLineLength(points[0], points[1]) == getLineLength(points[2], points[3])
                && getLineLength(points[0], points[1]) == getLineLength(points[3], points[0])) ? true : false;
    }

    public static boolean areAnglesEqual(Quadrilateral quad)
    {
        Point points[] = quad.getPoints();
        return (getAngleBetweenThreePoints(points[0], points[1], points[2]) == getAngleBetweenThreePoints(
                points[1], points[2], points[3])
                && getAngleBetweenThreePoints(points[1], points[2], points[3]) == getAngleBetweenThreePoints(
                        points[2], points[3], points[0])
                && getAngleBetweenThreePoints(points[0], points[1], points[2]) == getAngleBetweenThreePoints(
                        points[1], points[0], points[3]))
                                ? true : false;
    }
}
