package util;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import entity.Point;
import exception.FileReadException;

public class ReaderUtil
{

    private static final Logger LOGGER = Logger.getLogger(ReaderUtil.class);

    public static Point readPointFromFile(String file) throws FileReadException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            Point point = objectMapper.readValue(new File(file), Point.class);
            return point;
        }
        catch (IOException e)
        {
            LOGGER.error("Unable to read file " + file);
            throw new FileReadException("Unable to read file " + file, e);
        }
    }

    public static Point[] readPointsFromFile(String file) throws FileReadException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            Point[] points = objectMapper.readValue(new File(file),
                    Point[].class);
            return points;
        }
        catch (IOException e)
        {
            LOGGER.error("Unable to read file " + file);
            throw new FileReadException("Unable to read file " + file, e);
        }
    }

}
