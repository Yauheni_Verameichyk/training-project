package entity;

import java.util.Arrays;

public class Quadrilateral
{
    private Point points[];

    public Quadrilateral()
    {
    }

    public Quadrilateral(Point[] points)
    {
        this.points = points;
    }

    public Point[] getPoints()
    {
        return points;
    }

    public void setPoints(Point[] points)
    {
        this.points = points;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(points);
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Quadrilateral other = (Quadrilateral) obj;
        if (!Arrays.equals(points, other.points))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Qadril [points=" + Arrays.toString(points) + "]";
    }

}
