package factory;

import static util.PointUtil.changePointsOrder;
import static util.PointUtil.checkIfNotTriangle;
import static util.PointUtil.getPointsToSwap;
import static util.PointUtil.isSelfIntercected;

import entity.Point;
import entity.Quadrilateral;
import exception.UnxpectedFigureException;

public class QuadrilateralFactory
{
    public static Quadrilateral createQuadrilateral(Point[] points)
        throws UnxpectedFigureException
    {
        checkIfNotTriangle(points);
        if (isSelfIntercected(points))
        {
            points = changePointsOrder(points, getPointsToSwap(points));
        }
        return new Quadrilateral(points);
    }

}
