package entity;

import java.util.Objects;

public class Question
{
    private Long questionID;
    private String questionBody;
    private Theme theme;

    public Long getQuestionID()
    {
        return questionID;
    }

    public void setQuestionID(Long questionID)
    {
        this.questionID = questionID;
    }

    public String getQuestionBody()
    {
        return questionBody;
    }

    public void setQuestionBody(String questionBody)
    {
        this.questionBody = questionBody;
    }

    public Theme getTheme()
    {
        return theme;
    }

    public void setTheme(Theme theme)
    {
        this.theme = theme;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(questionID, questionBody, theme);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Question other = (Question) obj;
        return Objects.equals(questionID, other.questionID) && Objects.equals(questionBody, other.questionBody)
                && Objects.equals(theme, other.theme);
    }

    @Override
    public String toString()
    {
        return "Question [questionID=" + questionID + ", questionBody=" + questionBody + ", theme=" + theme + "]";
    }
}
