package entity;

import java.util.Date;
import java.util.Objects;

public class User
{
    private Long userID;
    private String username;
    private String password;
    private Sex sex;
    private Date birthDate;
    private String email;
    private Role role;

    public Long getUserID()
    {
        return userID;
    }

    public void setUserID(Long userID)
    {
        this.userID = userID;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Sex getSex()
    {
        return sex;
    }

    public void setSex(Sex sex)
    {
        this.sex = sex;
    }

    public Date getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Role getRole()
    {
        return role;
    }

    public void setRole(Role role)
    {
        this.role = role;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(userID, username, password, sex, birthDate, email, role);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        User other = (User) obj;
        return Objects.equals(userID, other.userID) && Objects.equals(username, other.username)
                && Objects.equals(password, other.password) && Objects.equals(sex, other.sex)
                && Objects.equals(birthDate, other.birthDate) && Objects.equals(email, other.email)
                && Objects.equals(role, other.role);
    }

    @Override
    public String toString()
    {
        return "User [userID=" + userID + ", username=" + username + ", password=" + password + ", sex=" + sex
                + ", birthDate=" + birthDate + ", email=" + email + ", role=" + role + "]";
    }
}
