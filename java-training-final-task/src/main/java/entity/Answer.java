package entity;

import java.util.Date;
import java.util.Objects;

public class Answer
{
    private Long answerID;
    private String answerBody;
    private Long userID;
    private Long surveyID;
    private Long questionID;
    private Date answerDate;

    public long getAnswerID()
    {
        return answerID;
    }

    public void setAnswerID(Long answerID)
    {
        this.answerID = answerID;
    }

    public String getAnswerBody()
    {
        return answerBody;
    }

    public void setAnswerBody(String answerBody)
    {
        this.answerBody = answerBody;
    }

    public long getUserID()
    {
        return userID;
    }

    public void setUserID(Long userID)
    {
        this.userID = userID;
    }

    public long getSurveyID()
    {
        return surveyID;
    }

    public void setSurveyID(Long surveyID)
    {
        this.surveyID = surveyID;
    }

    public long getQuestionID()
    {
        return questionID;
    }

    public void setQuestionID(Long questionID)
    {
        this.questionID = questionID;
    }

    public Date getAnswerDate()
    {
        return answerDate;
    }

    public void setAnswerDate(Date answerDate)
    {
        this.answerDate = answerDate;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(answerID, answerBody, userID, surveyID, questionID, answerDate);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Answer other = (Answer) obj;
        return Objects.equals(answerID, other.answerID) && Objects.equals(answerBody, other.answerBody)
                && Objects.equals(userID, other.userID) && Objects.equals(surveyID, other.surveyID)
                && Objects.equals(questionID, other.questionID) && Objects.equals(answerDate, other.answerDate);
    }

    @Override
    public String toString()
    {
        return "Answer [answerID=" + answerID + ", answerBody=" + answerBody + ", userID=" + userID + ", surveyID="
                + surveyID + ", questionID=" + questionID + ", answerDate=" + answerDate + "]";
    }
}
