package entity;

import java.util.Objects;

public class Survey
{
    private Long surveyID;
    private String surveyName;

    public long getSurveyID()
    {
        return surveyID;
    }

    public void setSurveyID(Long surveyID)
    {
        this.surveyID = surveyID;
    }

    public String getSurveyName()
    {
        return surveyName;
    }

    public void setSurveyName(String surveyName)
    {
        this.surveyName = surveyName;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(surveyID, surveyName);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Survey other = (Survey) obj;
        return Objects.equals(surveyID, other.surveyID) && Objects.equals(surveyName, other.surveyName);
    }

    @Override
    public String toString()
    {
        return "Survey [surveyID=" + surveyID + ", surveyName=" + surveyName + "]";
    }
}
