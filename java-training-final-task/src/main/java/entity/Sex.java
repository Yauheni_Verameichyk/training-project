package entity;

import org.apache.log4j.Logger;

import exception.DAOException;

public enum Sex
{
    MALE("M"),
    FEMALE("F");

    private static final Logger logger = Logger.getLogger(Sex.class);

    private String value;

    private Sex()
    {
    }

    private Sex(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    public static Sex getByValue(String value) throws DAOException
    {
        for (Sex sex : Sex.values())
        {
            if (sex.getValue().equals(value))
            {
                return sex;
            }
        }
        String message = "Unknown element in column Sex";
        logger.error(message);
        throw new DAOException(message);
    }
}
