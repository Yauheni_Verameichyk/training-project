package entity;

public enum Theme
{
    POLITICS,
    ECONOMICS,
    SOCIETY,
    SCIENCE,
    RELIGION,
    EDUCATION,
    CAREER,
    SPORT,
    AUTO
}
