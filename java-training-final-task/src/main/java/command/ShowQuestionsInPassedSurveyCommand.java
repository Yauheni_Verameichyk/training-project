package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import dao.QuestionDAO;
import dao.SurveyDAO;
import entity.Answer;
import entity.Question;
import entity.Survey;
import entity.User;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowQuestionsInPassedSurveyCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowQuestionsInPassedSurveyCommand.class);

    /**
     * Gets HttpServletRequest object, reads required page number, survey name and current user from it. Gets from the database pairs of
     * question and answer(from certain survey passed by current user) list and all pairs number. Sets current page, pages number and pairs
     * list as attributes to HttpServletRequest object and survey as session attribute. Returns the answeredQuestionsList.jsp page url, to
     * where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have required page number, survey name and current user
     * @return the answeredQuestionsList.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String surveyName = request.getParameter("surveyName");
        try
        {
            Survey survey;
            // Method can accept either surveyName as request parameter or survey as session parameter
            if (surveyName != null)
            {
                SurveyDAO surveyDAO = new SurveyDAO();
                survey = surveyDAO.getByName(surveyName);
            }
            else
            {
                survey = (Survey) session.getAttribute("survey");
            }
            QuestionDAO questionDAO = new QuestionDAO();
            List<Pair<Question, Answer>> answeredQuestionsList =
                    questionDAO.getQuestionsAndAnswersListByUsernameAndSurveyName(user.getUsername(),
                            survey.getSurveyName(), currentPage, recordsPerPage);
            int rowsNumber = questionDAO.getQuestionsAndAnswersNumberByUsernameAndSurveyName(user.getUsername(),
                    survey.getSurveyName());
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            session.setAttribute("survey", survey);
            request.setAttribute("answeredQuestionsList", answeredQuestionsList);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return ANSERED_QUESTIONS_LIST;
    }
}
