package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import dao.QuestionDAO;
import entity.Question;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowQuestionsListForDeletingByThemeCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowQuestionsListForAddingToSurveyByThemeCommand.class);

    /**
     * Gets HttpServletRequest object, reads questions theme and required page number from it. Gets from the database questions list by
     * theme or all questions list if theme is not indicated. Sets theme, current page, pages number and questions list as attributes to
     * HttpServletRequest object. Returns the questionsListForDeleting.jsp page url, to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have questions theme and required page number
     * @return the questionsListForDeleting.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        String theme = request.getParameter("theme");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        List<Question> questionsList;
        int rowsNumber;
        try
        {
            QuestionDAO questionDAO = new QuestionDAO();
            if (theme.length() != 0)
            {
                questionsList = questionDAO.getByTheme(theme, currentPage, recordsPerPage);
                rowsNumber = questionDAO.getQuestionNumberByTheme(theme);
                request.setAttribute("theme", theme);
            }
            else
            {
                questionsList = questionDAO.getAll(currentPage, recordsPerPage);
                rowsNumber = questionDAO.getAllQuestionsNumber();
            }
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            request.setAttribute("questionsList", questionsList);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return QUESTIONS_LIST_FOR_DELETING;
    }
}
