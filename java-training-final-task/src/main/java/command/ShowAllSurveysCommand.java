package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import dao.SurveyDAO;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowAllSurveysCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowAllSurveysCommand.class);

    /**
     * Gets HttpServletRequest object, reads required page number from it. Gets from database all surveys list and surveys number. Sets
     * current page, pages number and surveys list as attributes to HttpServletRequest object. Returns the surveysList.jsp page url, to
     * where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have required page number
     * @return the surveysList.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        try
        {
            SurveyDAO surveyDAO = new SurveyDAO();
            List<Pair<String, Integer>> surveysList =
                    surveyDAO.getAllSurveysAndQuestionsNumber(currentPage, recordsPerPage);
            int rowsNumber = surveyDAO.getAllSurveysNumber();
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            request.setAttribute("surveysList", surveysList);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return SURVEYS_LIST;
    }
}
