package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.UserDAO;
import entity.Survey;
import entity.User;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowUsersPassedSurveyCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowUsersPassedSurveyCommand.class);

    /**
     * Gets HttpServletRequest object, reads required page number and {@link Survey} from it. Gets from the database users list (that passed
     * this survey) and users number. Sets current page, pages number and users list as attributes to HttpServletRequest object. Returns the
     * usersPassedSurvey.jsp page url, to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have required page number and survey
     * @return the usersPassedSurvey.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        HttpSession session = request.getSession();
        Survey survey = (Survey) session.getAttribute("survey");
        try
        {
            UserDAO userDAO = new UserDAO();
            List<User> usersList = userDAO.getBySurveyName(survey.getSurveyName(), currentPage, recordsPerPage);
            int usersNumber = userDAO.getUsersNumberBySurveyName(survey.getSurveyName());
            int pagesNumber = PagingUtil.getPagesNumber(usersNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            request.setAttribute("usersList", usersList);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return USERS_PASSED_SURVEY;
    }
}
