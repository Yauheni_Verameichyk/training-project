package command;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import dao.UserDAO;
import entity.Role;
import entity.Sex;
import entity.User;
import exception.CommandException;
import exception.DAOException;
import util.CommandUtil;

public class RegistrationCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(RegistrationCommand.class);

    /**
     * Gets HttpServletRequest object, reads user data for registration from it. If user with such username already exists sets message
     * about it as attribute to HttpServletRequest object and returns the registration.jsp page url. If username is unique creates new
     * {@link User}, initializes it's fields with data from the request and returns the registrationSuccess.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object which have user data for registration
     * @return the registrationSuccess.jsp page url or registration page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        String username = request.getParameter("username");
        User user = new User();
        try
        {
            UserDAO userDAO = new UserDAO();
            if (userDAO.getByUsername(username) != null)
            {
                request.setAttribute("usernameError",
                        CommandUtil.getMessage((String) session.getAttribute("language"), "user.already.exists"));
                return REGISTRATION;
            }
            user.setUsername(username);
            user.setPassword((CommandUtil.hash(request.getParameter("pass1"))));
            setBirthDate(request.getParameter("birthDate"), user);
            setSex(request.getParameter("sex"), user);
            user.setEmail(request.getParameter("email"));
            user.setRole(Role.USER);
            userDAO.create(user);
            request.setAttribute("username", username);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return REGISTRATION_SUCCESS;
    }

    /**
     * Gets birthDate as String, if String isn't empty parses it into Date object and sets users birthDate
     * 
     * @param birthDate
     * @param user
     */
    private void setBirthDate(String birthDate, User user) throws CommandException
    {
        try
        {
            if (StringUtils.isNotEmpty(birthDate))
            {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                user.setBirthDate(format.parse(birthDate));
            }
        }
        catch (ParseException e)
        {
            String message = "Exception during date reading";
            logger.error(message, e);
            throw new CommandException(message, e);
        }
    }

    /**
     * Gets sex as String, if String isn't empty find corresponding Sex constatant and sets users sex
     * 
     * @param sex
     * @param user
     */
    private void setSex(String sex, User user)
    {
        if (StringUtils.isNotEmpty(sex))
        {
            user.setSex(Sex.valueOf(sex));
        }
    }
}
