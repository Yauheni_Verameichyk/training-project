package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.SurveyQuestionDAO;
import entity.Survey;
import exception.CommandException;
import exception.DAOException;
import util.CommandUtil;

public class AddQuestionsToSurveyCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(AddQuestionsToSurveyCommand.class);

    /**
     * Gets HttpServletRequest object, reads {@link Survey} and question id's that must be added to survey from it. Adds questions to
     * survey. Sets message about success as an attribute to HttpServletRequest object. Creates new
     * {@link ShowQuestionsListForAddingInSurveyByThemeCommand} object, invokes method execute() with the request as an argument, which
     * returns page url, from where current method was invoked.
     * 
     * @param request
     *            HttpServletRequest object which has a {@link Survey} object and question id's that must be added to survey
     * @return questionsListForAddingInSurvey.jsp url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        String[] questions = request.getParameterValues("checkbox");
        Survey survey = (Survey) session.getAttribute("survey");
        try
        {
            SurveyQuestionDAO surveyQuestionDAO = new SurveyQuestionDAO();
            if (questions != null)
            {
                for (String questionID : questions)
                {
                    surveyQuestionDAO.addQuestionToSurvey(survey.getSurveyID(), new Long(questionID));
                }
            }
            request.setAttribute("successMessage",
                    CommandUtil.getMessage((String) session.getAttribute("language"), "questions.were.added"));
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return new ShowQuestionsListForAddingToSurveyByThemeCommand().execute(request);
    }
}
