package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.QuestionDAO;
import entity.Question;
import entity.Survey;
import exception.CommandException;
import exception.DAOException;

public class PassSurveyCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(PassSurveyCommand.class);

    /**
     * Gets HttpServletRequest object, reads {@link Survey} from it. Gets from the database questions list by survey. Sets questions list as
     * attribute to HttpServletRequest object. Returns the answerQuestions.jsp page url, to where servlet will send HttpServletRequest
     * object.
     * 
     * @param request
     *            HttpServletRequest object which have survey
     * @return the answerQuestions.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        Survey survey = (Survey) session.getAttribute("survey");
        try
        {
            QuestionDAO questionDAO = new QuestionDAO();
            List<Question> questionsList = questionDAO.getQuestionsFromSurvey(survey.getSurveyName());
            request.setAttribute("questionsList", questionsList);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return ANSWER_QUESTIONS;
    }
}
