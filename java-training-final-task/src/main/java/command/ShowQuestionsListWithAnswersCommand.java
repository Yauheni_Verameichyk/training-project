package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import dao.QuestionDAO;
import entity.Question;
import entity.Survey;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowQuestionsListWithAnswersCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowQuestionsListWithAnswersCommand.class);

    /**
     * Gets HttpServletRequest object, reads required page number and {@link Survey} from it. Gets from the sdatabase pairs of question and
     * times answered(how many users answered question from survey "surveyName") list and all pairs number. Sets current page, pages number
     * and pairs list as attributes to HttpServletRequest object. Returns the questionsListWithAnswers.jsp page url, to where servlet will
     * send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have required page number and survey
     * @return the questionsListWithAnswers.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        HttpSession session = request.getSession();
        Survey survey = (Survey) session.getAttribute("survey");
        try
        {
            QuestionDAO questionDAO = new QuestionDAO();
            List<Pair<Question, Integer>> questionsList =
                    questionDAO.getQuestionsAndTimesAnsweredFromSurvey(survey.getSurveyName(), currentPage,
                            recordsPerPage);
            int rowsNumber = questionDAO.getQuestionsNumberInSurvey(survey.getSurveyName());
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            request.setAttribute("questionsList", questionsList);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return QUESTIONS_LIST_WITH_ANSWERS;
    }
}
