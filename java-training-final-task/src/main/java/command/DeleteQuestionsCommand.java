package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.QuestionDAO;
import exception.CommandException;
import exception.DAOException;
import util.CommandUtil;

public class DeleteQuestionsCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(DeleteQuestionsCommand.class);

    /**
     * Gets HttpServletRequest object, reads question IDs that must be deleted from it. Deletes questions from the database. Sets success
     * message as attribute to HttpServletRequest object. Creates new {@link ShowQuestionsListForDeletingByThemeCommand} object, invokes
     * method execute with request as argument, which returns page url, from where current method was invoked.
     * 
     * @param request
     *            HttpServletRequest object which have question IDs that must be deleted
     * @return the questionsListForDeleting.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        String[] questions = request.getParameterValues("checkbox");
        try
        {
            QuestionDAO questionDAO = new QuestionDAO();
            if (questions != null)
            {
                for (String questionID : questions)
                {
                    questionDAO.deleteQuestion(new Long(questionID));
                }
            }
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        request.setAttribute("successMessage",
                CommandUtil.getMessage((String) session.getAttribute("language"), "questions.were.deleted"));
        return new ShowQuestionsListForDeletingByThemeCommand().execute(request);
    }
}
