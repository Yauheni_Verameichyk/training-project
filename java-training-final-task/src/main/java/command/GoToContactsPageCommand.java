package command;

import javax.servlet.http.HttpServletRequest;

public class GoToContactsPageCommand implements ICommand
{
    /**
     * Returns the contacts.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the contacts.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request)
    {
        return CONTACTS;
    }
}
