package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import dao.SurveyDAO;
import entity.User;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowSurveysPassedByUserCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowSurveysPassedByUserCommand.class);

    /**
     * Gets HttpServletRequest object, reads required page number and current user from it. Gets from the database pairs of survey (passed
     * by current user) and questions number in survey list and pairs number. Sets current page, pages number and pairs list as attributes
     * to HttpServletRequest object. Returns the passedSurveysList.jsp page url, to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have required page number and current user
     * @return the passedSurveysList.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        try
        {
            SurveyDAO surveyDAO = new SurveyDAO();
            List<Pair<String, Integer>> surveysList =
                    surveyDAO.getByUsername(user.getUsername(), currentPage, recordsPerPage);
            int rowsNumber = surveyDAO.getSurveysNumberByUser(user.getUsername());
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            request.setAttribute("surveysList", surveysList);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return PASSED_SURVEYS_LIST;
    }
}
