package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.SurveyQuestionDAO;
import entity.Survey;
import exception.CommandException;
import exception.DAOException;
import util.CommandUtil;

public class DeleteQuestionsFromSurveyCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(DeleteQuestionsFromSurveyCommand.class);

    /**
     * Gets HttpServletRequest object, reads question id's that must be deleted from survey and {@link Survey} from it. Deletes questions
     * from survey. Sets success message as attribute to HttpServletRequest object. Creates new
     * {@link ShowQuestionsListForDeletingFromSurveyCommand} object, invokes method execute with request as argument, which returns page
     * url, from where current method was invoked.
     * 
     * @param request
     *            HttpServletRequest object which have question id's that must be deleted from survey and survey
     * @return the questionsListForDeletingFromSurvey.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        String[] questions = request.getParameterValues("checkbox");
        Survey survey = (Survey) session.getAttribute("survey");
        try
        {
            SurveyQuestionDAO surveyQuestionDAO = new SurveyQuestionDAO();
            if (questions != null)
            {
                for (String questionID : questions)
                {
                    surveyQuestionDAO.deleteQuestionFromSurvey(survey.getSurveyID(), new Long(questionID));
                }
            }
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        request.setAttribute("successMessage",
                CommandUtil.getMessage((String) session.getAttribute("language"),
                        "questions.were.deleted.from.survey"));
        return new ShowQuestionsListForDeletingFromSurveyCommand().execute(request);
    }
}
