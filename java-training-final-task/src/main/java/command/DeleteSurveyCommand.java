package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.SurveyDAO;
import entity.Survey;
import exception.CommandException;
import exception.DAOException;

public class DeleteSurveyCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(DeleteSurveyCommand.class);

    /**
     * Gets HttpServletRequest object, reads name of survey that must be deleted from it. Deletes survey from the database and all answers
     * for this survey. Returns the surveyDeleted.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object which have name of survey that must be deleted
     * @return the surveyDeleted.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        Survey survey = (Survey) session.getAttribute("survey");
        request.setAttribute("surveyName", survey.getSurveyName());
        try
        {
            SurveyDAO surveyDAO = new SurveyDAO();
            surveyDAO.delete(survey.getSurveyID());
            return SURVEY_DELETED;
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
    }
}
