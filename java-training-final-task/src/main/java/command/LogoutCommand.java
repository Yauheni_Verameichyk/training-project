package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import exception.CommandException;

public class LogoutCommand implements ICommand
{
    /**
     * Sets session attribute "user" equal to null. Creates new {@link GoToMainPageCommand} object, invokes method execute with request as
     * argument, which returns the mainPage.jsp url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the mainPage.jsp url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        session.setAttribute("user", null);
        return new GoToMainPageCommand().execute(request);
    }
}
