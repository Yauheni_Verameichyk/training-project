package command;

import javax.servlet.http.HttpServletRequest;

public class GoToAdminPageCommand implements ICommand
{
    /**
     * Returns the adminPage.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the adminPage.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request)
    {
        return ADMIN_PAGE;
    }
}
