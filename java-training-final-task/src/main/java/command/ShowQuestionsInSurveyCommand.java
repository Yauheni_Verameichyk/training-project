package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.QuestionDAO;
import dao.SurveyDAO;
import entity.Question;
import entity.Survey;
import entity.User;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowQuestionsInSurveyCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowQuestionsInSurveyCommand.class);

    /**
     * Gets HttpServletRequest object, reads required page number, survey name and current user from it. Gets from the database questions in
     * survey list and all questions in survey number. Checks if survey is passed by current user. Sets current page, pages number and
     * questions list as attributes to HttpServletRequest object and survey as session attribute. Returns the questionsListInSurvey.jsp page
     * url, to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have required page number, survey name and current user
     * @return the questionsListInSurvey.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        try
        {
            QuestionDAO questionDAO = new QuestionDAO();
            SurveyDAO surveyDAO = new SurveyDAO();
            String surveyName = request.getParameter("surveyName");
            Survey survey;
            // Method can accept either surveyName as request parameter or survey as session parameter
            if (surveyName != null)
            {
                survey = surveyDAO.getByName(surveyName);
            }
            else
            {
                survey = (Survey) session.getAttribute("survey");
            }
            if (user != null)
            {
                request.setAttribute("isPassed", surveyDAO.isPassed(user.getUsername(), survey.getSurveyName()));
            }
            List<Question> questionsList =
                    questionDAO.getQuestionsFromSurvey(survey.getSurveyName(), currentPage, recordsPerPage);
            int rowsNumber = questionDAO.getQuestionsNumberInSurvey(survey.getSurveyName());
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            session.setAttribute("survey", survey);
            request.setAttribute("questionsList", questionsList);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return QUESTIONS_LIST_IN_SURVEY;
    }
}
