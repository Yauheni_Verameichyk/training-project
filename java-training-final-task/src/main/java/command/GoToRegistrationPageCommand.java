package command;

import javax.servlet.http.HttpServletRequest;

public class GoToRegistrationPageCommand implements ICommand
{
    /**
     * Returns the registration.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the registration.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request)
    {
        return REGISTRATION;
    }
}
