package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.QuestionDAO;
import entity.Question;
import entity.Theme;
import exception.CommandException;
import exception.DAOException;
import util.CommandUtil;

public class CreateQuestionCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(CreateQuestionCommand.class);

    /**
     * Gets HttpServletRequest object, reads question body and theme from it. Creates {@link Question} and inserts it into the database.
     * Sets success message as attribute to HttpServletRequest object. Creates new {@link GoToCreateQuestionPageCommand} object, invokes
     * method execute with request as argument, which returns page url, from where current method was invoked.
     * 
     * @param request
     *            HttpServletRequest object which have survey and question id's that must be added to survey
     * @return the createQuestion.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        try
        {
            QuestionDAO questionDAO = new QuestionDAO();
            Question question = new Question();
            question.setQuestionBody(request.getParameter("questionBody"));
            question.setTheme(Theme.valueOf(request.getParameter("themeName")));
            questionDAO.create(question);
            request.setAttribute("successMessage",
                    CommandUtil.getMessage((String) session.getAttribute("language"), "question.was.created"));
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return new GoToCreateQuestionPageCommand().execute(request);
    }
}
