package command;

import javax.servlet.http.HttpServletRequest;

public class GoToOrderSurveyPageCommand implements ICommand
{
    /**
     * Returns the orderSurvey.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the orderSurvey.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request)
    {
        return ORDER_SURVEY;
    }
}
