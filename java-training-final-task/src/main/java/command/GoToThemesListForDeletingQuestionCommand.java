package command;

import java.util.EnumSet;

import javax.servlet.http.HttpServletRequest;

import entity.Theme;

public class GoToThemesListForDeletingQuestionCommand implements ICommand
{
    /**
     * Sets themes list as attribute to HttpServletRequest object. Returns the themesListForDeletingQuestion.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the themesListForDeletingQuestion.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request)
    {
        EnumSet<Theme> themesList = EnumSet.allOf(Theme.class);
        request.setAttribute("themesList", themesList);
        return THEMES_LIST_FOR_DELETING_QUESTION;
    }
}
