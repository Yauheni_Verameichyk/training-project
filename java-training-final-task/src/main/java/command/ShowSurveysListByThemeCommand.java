package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import dao.SurveyDAO;
import exception.CommandException;
import exception.DAOException;
import util.CommandUtil;
import util.PagingUtil;

public class ShowSurveysListByThemeCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowSurveysListByThemeCommand.class);

    /**
     * Gets HttpServletRequest object, reads required page number and theme name from it. Gets from the database surveys list by theme and
     * surveys number. Sets current page, pages number and surveys list as attributes to HttpServletRequest object. Returns the
     * surveysList.jsp page url, to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have required page number and theme name
     * @return the surveysList.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        String themeName = request.getParameter("themeName");
        try
        {
            SurveyDAO surveyDAO = new SurveyDAO();
            List<Pair<String, Integer>> surveysList =
                    surveyDAO.getSurveysNameAndQuestionsNumberByTheme(themeName, currentPage, recordsPerPage);
            int rowsNumber = surveyDAO.getSurveysNumberByTheme(themeName);
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            request.setAttribute("surveysList", surveysList);
            request.setAttribute("themeName",
                    CommandUtil.getMessage((String) session.getAttribute("language"), themeName.toLowerCase()));
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return SURVEYS_LIST;
    }
}
