package command;

import java.util.EnumSet;

import javax.servlet.http.HttpServletRequest;

import entity.Theme;

public class ShowThemesListCommand implements ICommand
{
    /**
     * Sets themes list as attribute to HttpServletRequest object. Returns the themesListForAddingQuestionsInSurvey.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the themesListForAddingQuestionsInSurvey.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request)
    {
        EnumSet<Theme> themesList = EnumSet.allOf(Theme.class);
        request.setAttribute("themesList", themesList);
        return THEMES_LIST;
    }
}
