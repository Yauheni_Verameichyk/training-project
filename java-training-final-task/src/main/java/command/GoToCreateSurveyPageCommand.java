package command;

import javax.servlet.http.HttpServletRequest;

public class GoToCreateSurveyPageCommand implements ICommand
{
    /**
     * Returns the createSurvey.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the createSurvey.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request)
    {
        return CREATE_SURVEY;
    }
}
