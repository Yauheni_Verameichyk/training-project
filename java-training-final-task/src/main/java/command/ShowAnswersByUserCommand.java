package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import dao.QuestionDAO;
import entity.Answer;
import entity.Question;
import entity.Survey;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowAnswersByUserCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowAnswersByUserCommand.class);

    /**
     * Gets HttpServletRequest object, reads required page number, {@link Survey} and username from it. Gets from the database pairs of
     * {@link Answer} and {@link Question} list and answer and question pairs number. Sets current page, pages number, pairs list and
     * username as attributes to HttpServletRequest object. Returns the answersByUser.jsp page url, to where servlet will send
     * HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have required page number, survey and username
     * @return the answersByUser.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        HttpSession session = request.getSession();
        Survey survey = (Survey) session.getAttribute("survey");
        String userName = request.getParameter("userName");
        try
        {
            QuestionDAO questionDAO = new QuestionDAO();
            List<Pair<Question, Answer>> answersList =
                    questionDAO.getQuestionsAndAnswersListByUsernameAndSurveyName(userName, survey.getSurveyName(),
                            currentPage, recordsPerPage);
            int rowsNumber =
                    questionDAO.getQuestionsAndAnswersNumberByUsernameAndSurveyName(userName, survey.getSurveyName());
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            request.setAttribute("answersList", answersList);
            request.setAttribute("userName", userName);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return ANSWERS_BY_USER;
    }
}
