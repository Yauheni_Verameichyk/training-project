package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.AnswerDAO;
import entity.Answer;
import entity.Survey;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowAnswersForQuestionCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowAnswersForQuestionCommand.class);

    /**
     * Gets HttpServletRequest object, reads required page number, {@link Survey}, question body and question ID from it. Gets from the
     * database answers list (by question ID and survey ID from different users) and answers number. Sets current page, pages number,
     * answers list, question ID and question body as attributes to HttpServletRequest object. Returns the answersForQuestion page.jsp url,
     * to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have required page number, survey, question body and question ID
     * @return the answersForQuestion.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        HttpSession session = request.getSession();
        Survey survey = (Survey) session.getAttribute("survey");
        String questionBody = request.getParameter("questionBody");
        Long questionID = new Long(request.getParameter("questionID"));
        try
        {
            AnswerDAO answerDAO = new AnswerDAO();
            List<Answer> answersList =
                    answerDAO.getByQuestionIDAndSurveyID(survey.getSurveyID(), questionID, currentPage, recordsPerPage);
            int rowsNumber = answerDAO.getAnswersNumberByQuestionIDAndSurveyID(survey.getSurveyID(), questionID);
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            request.setAttribute("answersList", answersList);
            request.setAttribute("questionID", questionID);
            request.setAttribute("questionBody", questionBody);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return ANSWERS_FOR_QUESTION;
    }
}
