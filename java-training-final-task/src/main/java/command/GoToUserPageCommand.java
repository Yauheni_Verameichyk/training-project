package command;

import javax.servlet.http.HttpServletRequest;

public class GoToUserPageCommand implements ICommand
{
    /**
     * Returns the userPage.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the userPage.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request)
    {
        return USER_PAGE;
    }
}
