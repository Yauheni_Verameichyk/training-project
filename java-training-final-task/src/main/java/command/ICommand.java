package command;

import javax.servlet.http.HttpServletRequest;

import exception.CommandException;

public interface ICommand
{
    static final String INCORRECT_USERNAME_OR_PASSWORD = "/WEB-INF/jsp/common/incorrectUsernameOrPassword.jsp";
    static final String MAIN_PAGE = "/WEB-INF/jsp/common/mainPage.jsp";
    static final String SURVEYS_LIST = "/WEB-INF/jsp/common/surveysList.jsp";
    static final String THEMES_LIST = "/WEB-INF/jsp/common/themesList.jsp";
    static final String QUESTIONS_LIST_IN_SURVEY = "/WEB-INF/jsp/common/questionsListInSurvey.jsp";
    static final String REGISTRATION = "/WEB-INF/jsp/common/registration.jsp";
    static final String REGISTRATION_SUCCESS = "/WEB-INF/jsp/common/registrationSuccess.jsp";
    static final String PASSED_SURVEYS_LIST = "/WEB-INF/jsp/common/passedSurveysList.jsp";
    static final String ANSERED_QUESTIONS_LIST = "/WEB-INF/jsp/common/answeredQuestionsList.jsp";
    static final String ANSWER_QUESTIONS = "/WEB-INF/jsp/common/answerQuestions.jsp";
    static final String SURVEY_PASSED = "/WEB-INF/jsp/common/surveyPassed.jsp";
    static final String ADMIN_PAGE = "/WEB-INF/jsp/admin/adminPage.jsp";
    static final String CREATE_SURVEY = "/WEB-INF/jsp/admin/createSurvey.jsp";
    static final String THEMES_LIST_FOR_ADDING_IN_SURVEY =
            "/WEB-INF/jsp/admin/themesListForAddingQuestionsInSurvey.jsp";
    static final String QUESTIONS_LIST_FOR_ADDING_IN_SURVEY =
            "/WEB-INF/jsp/admin/questionsListForAddingInSurvey.jsp";
    static final String QUESTIONS_LIST_FOR_DELETING_FROM_SURVEY =
            "/WEB-INF/jsp/admin/questionsListForDeletingFromSurvey.jsp";
    static final String SURVEY_DELETING_ERROR = "/WEB-INF/jsp/admin/surveyDeletingError.jsp";
    static final String SURVEY_DELETED = "/WEB-INF/jsp/admin/surveyDeleted.jsp";
    static final String USERS_PASSED_SURVEY = "/WEB-INF/jsp/admin/usersPassedSurvey.jsp";
    static final String ANSWERS_BY_USER = "/WEB-INF/jsp/admin/answersByUser.jsp";
    static final String QUESTIONS_LIST_WITH_ANSWERS = "/WEB-INF/jsp/admin/questionsListWithAnswers.jsp";
    static final String ANSWERS_FOR_QUESTION = "/WEB-INF/jsp/admin/answersForQuestion.jsp";
    static final String CREATE_QUESTION = "/WEB-INF/jsp/admin/createQuestion.jsp";
    static final String THEMES_LIST_FOR_DELETING_QUESTION =
            "/WEB-INF/jsp/admin/themesListForDeletingQuestion.jsp";
    static final String QUESTIONS_LIST_FOR_DELETING = "/WEB-INF/jsp/admin/questionsListForDeleting.jsp";
    static final String ORDER_SURVEY = "/WEB-INF/jsp/common/orderSurvey.jsp";
    static final String ABOUT_US = "/WEB-INF/jsp/common/aboutUs.jsp";
    static final String CONTACTS = "/WEB-INF/jsp/common/contacts.jsp";
    static final String USER_PAGE = "/WEB-INF/jsp/common/userPage.jsp";

    String execute(HttpServletRequest request) throws CommandException;
}
