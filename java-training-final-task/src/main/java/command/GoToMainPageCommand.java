package command;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import dao.SurveyDAO;
import entity.Survey;
import exception.CommandException;
import exception.DAOException;
import util.PropertyReader;

public class GoToMainPageCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(GoToMainPageCommand.class);

    private static final String CONFIG_PROPERTIES = "config.properties";
    private static final String POPULAR_SURVEYS_NUMBER = "popular.surveys.number";

    /**
     * Reads popular surveys list from the database. Sets it as attribute to HttpServletRequest object. Returns the mainPage.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the mainPage.jsp url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        try
        {
            SurveyDAO surveyDAO = new SurveyDAO();
            List<Survey> surveysList = surveyDAO.getPopularSurveys(
                    new Integer(PropertyReader.getProperty(POPULAR_SURVEYS_NUMBER, CONFIG_PROPERTIES)));
            request.setAttribute("surveysList", surveysList);
        }
        catch (NumberFormatException | IOException e)
        {
            String message = "Exception during reading from property file";
            logger.error(message, e);
            throw new CommandException(message, e);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return MAIN_PAGE;
    }
}
