package command;

import javax.servlet.http.HttpServletRequest;

public class GoToAboutUsPageCommand implements ICommand
{
    /**
     * Returns the aboutUs.jsp page url.
     * 
     * @param request
     *            HttpServletRequest object
     * @return the aboutUs.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request)
    {
        return ABOUT_US;
    }
}
