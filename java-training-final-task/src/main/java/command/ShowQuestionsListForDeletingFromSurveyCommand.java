package command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.QuestionDAO;
import entity.Question;
import entity.Survey;
import exception.CommandException;
import exception.DAOException;
import util.PagingUtil;

public class ShowQuestionsListForDeletingFromSurveyCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(ShowQuestionsListForDeletingFromSurveyCommand.class);

    /**
     * Gets HttpServletRequest object, reads survey and required page number from it. Gets from the database questions list by survey. Sets
     * current page, pages number and questions list as attributes to HttpServletRequest object and survey as session attribute. Returns the
     * questionsListForDeletingFromSurvey.jsp page url, to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have survey and required page number
     * @return the questionsListForDeletingFromSurvey.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String pageNum = request.getParameter("page");
        int currentPage = PagingUtil.getCurrentPage(pageNum);
        int recordsPerPage = PagingUtil.getRecordsPerPage();
        HttpSession session = request.getSession();
        Survey survey = (Survey) session.getAttribute("survey");
        try
        {
            QuestionDAO questionDAO = new QuestionDAO();
            List<Question> questionsList =
                    questionDAO.getQuestionsFromSurvey(survey.getSurveyName(), currentPage, recordsPerPage);
            int rowsNumber = questionDAO.getQuestionsNumberInSurvey(survey.getSurveyName());
            int pagesNumber = PagingUtil.getPagesNumber(rowsNumber, recordsPerPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("pagesNumber", pagesNumber);
            request.setAttribute("questionsList", questionsList);
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return QUESTIONS_LIST_FOR_DELETING_FROM_SURVEY;
    }
}
