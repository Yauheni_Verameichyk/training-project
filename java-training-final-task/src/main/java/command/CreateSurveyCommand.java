package command;

import java.util.EnumSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.SurveyDAO;
import entity.Survey;
import entity.Theme;
import exception.CommandException;
import exception.DAOException;

public class CreateSurveyCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(CreateSurveyCommand.class);

    /**
     * Gets HttpServletRequest object, reads survey name from it. Creates {@link Survey} and inserts it into the database. Sets themes list
     * as attribute to HttpServletRequest object and survey as session attribute. Returns the themesListForAddingQuestionsInSurvey.jsp page
     * url, to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have survey name
     * @return the themesListForAddingQuestionsInSurvey.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        try
        {
            SurveyDAO surveyDAO = new SurveyDAO();
            Survey survey = new Survey();
            survey.setSurveyName(request.getParameter("surveyName"));
            survey.setSurveyID(new Long(surveyDAO.create(survey)));
            session.setAttribute("survey", survey);
            EnumSet<Theme> themesList = EnumSet.allOf(Theme.class);
            request.setAttribute("themesList", themesList);
            return THEMES_LIST_FOR_ADDING_IN_SURVEY;
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
    }
}
