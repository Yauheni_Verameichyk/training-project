package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.UserDAO;
import entity.User;
import exception.CommandException;
import exception.DAOException;
import util.CommandUtil;

public class LoginCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(LoginCommand.class);

    /**
     * Gets HttpServletRequest object, reads username and password from it. Gets user by this username and password from the database. If
     * user doesn't exist returns the incorrectUsernameOrPassword.jsp page url. If user exists method reads it from the database and sets as
     * session attribute. Creates new {@link GoToMainPageCommand} object, invokes method execute with request as argument, which returns the
     * mainPage.jsp url.
     * 
     * @param request
     *            HttpServletRequest object which have username and password
     * @return the mainPage.jsp url or the incorrectUsernameOrPassword.jsp url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        try
        {
            UserDAO userDAO = new UserDAO();
            User user = userDAO.getByUsernameAndPass(username, (CommandUtil.hash(password)));
            if (user == null)
            {
                return INCORRECT_USERNAME_OR_PASSWORD;
            }
            else
            {
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                return new GoToMainPageCommand().execute(request);
            }
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
    }
}
