package command;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import dao.AnswerDAO;
import entity.Answer;
import entity.Survey;
import entity.User;
import exception.CommandException;
import exception.DAOException;

public class AnswerQuestionsCommand implements ICommand
{
    private static final Logger logger = Logger.getLogger(AnswerQuestionsCommand.class);

    /**
     * Gets HttpServletRequest object, reads {@link User}, {@link Survey}, questions array and answers array from it. Inserts answers into
     * the database or updates existing ones. Returns the surveyPassed.jsp page url, to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which have user, survey, questions array and answers array
     * @return the surveyPassed.jsp page url
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException
    {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String surveyID = request.getParameter("surveyID");
        String[] questions = request.getParameterValues("questionID");
        String[] answers = request.getParameterValues("answer");
        try
        {
            AnswerDAO answerDAO = new AnswerDAO();
            for (int i = 0; i < answers.length; i++)
            {
                // checks if answer field isn't empty
                if (StringUtils.isNotEmpty(answers[i]))
                {
                    Answer answer = answerDAO.getByUserIDSurveyIDQuestionID(user.getUserID().toString(), surveyID,
                            questions[i]);
                    if (answer == null)
                    {
                        answer = new Answer();
                        answer.setAnswerBody(answers[i]);
                        answer.setAnswerDate(new Date());
                        answer.setQuestionID(new Long(questions[i]));
                        answer.setSurveyID(new Long(surveyID));
                        answer.setUserID(user.getUserID());
                        answerDAO.create(answer);
                    }
                    else
                    {
                        answer.setAnswerBody(answers[i]);
                        answerDAO.update(answer);
                    }
                }
            }
        }
        catch (DAOException e)
        {
            logger.error(e.getMessage(), e);
            throw new CommandException(e.getMessage(), e);
        }
        return SURVEY_PASSED;
    }
}
