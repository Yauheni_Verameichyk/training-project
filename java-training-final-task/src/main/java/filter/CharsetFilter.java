package filter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class CharsetFilter implements Filter
{
    private String encoding;

    /**
     * Sets value of encoding variable as UTF-8
     * 
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig config) throws ServletException
    {
        encoding = config.getInitParameter("requestEncoding");
        if (encoding == null)
        {
            encoding = StandardCharsets.UTF_8.toString();
        }
    }

    /**
     * Sets character encoding for request and response as UTF-8
     * 
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain next)
        throws IOException, ServletException
    {
        if (null == request.getCharacterEncoding())
        {
            request.setCharacterEncoding(encoding);
        }
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
        next.doFilter(request, response);
    }

    @Override
    public void destroy()
    {
        // no implementation needed
    }
}
