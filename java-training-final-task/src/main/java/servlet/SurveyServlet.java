package servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import command.AddQuestionsToSurveyCommand;
import command.AnswerQuestionsCommand;
import command.CreateQuestionCommand;
import command.CreateSurveyCommand;
import command.DeleteQuestionsCommand;
import command.DeleteQuestionsFromSurveyCommand;
import command.DeleteSurveyCommand;
import command.GoToAboutUsPageCommand;
import command.GoToAdminPageCommand;
import command.GoToContactsPageCommand;
import command.GoToCreateQuestionPageCommand;
import command.GoToCreateSurveyPageCommand;
import command.GoToMainPageCommand;
import command.GoToOrderSurveyPageCommand;
import command.GoToRegistrationPageCommand;
import command.GoToThemesListForAddingQuestionsInSurveyCommand;
import command.GoToThemesListForDeletingQuestionCommand;
import command.GoToUserPageCommand;
import command.ICommand;
import command.LoginCommand;
import command.LogoutCommand;
import command.PassSurveyCommand;
import command.RegistrationCommand;
import command.ShowAllSurveysCommand;
import command.ShowAnswersByUserCommand;
import command.ShowAnswersForQuestionCommand;
import command.ShowQuestionsInPassedSurveyCommand;
import command.ShowQuestionsInSurveyCommand;
import command.ShowQuestionsListForAddingToSurveyByThemeCommand;
import command.ShowQuestionsListForDeletingByThemeCommand;
import command.ShowQuestionsListForDeletingFromSurveyCommand;
import command.ShowQuestionsListWithAnswersCommand;
import command.ShowSurveysListByThemeCommand;
import command.ShowSurveysPassedByUserCommand;
import command.ShowThemesListCommand;
import command.ShowUsersPassedSurveyCommand;
import exception.CommandException;
import util.ExceptionHandlingUtil;

@WebServlet("/SurveyServlet")
@MultipartConfig
public class SurveyServlet extends HttpServlet
{
    private static final long serialVersionUID = 9221457078543670780L;

    private Map<String, ICommand> commands;

    /**
     * Initializes the "commands" field with HashMap, where key is command, that was received from user and value - class implementing
     * interface ICommand associated with this command.
     * 
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException
    {
        commands = new HashMap<>();
        commands.put("login", new LoginCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("show_all_surveys", new ShowAllSurveysCommand());
        commands.put("show_questions_in_survey", new ShowQuestionsInSurveyCommand());
        commands.put("add_questions_to_survey", new AddQuestionsToSurveyCommand());
        commands.put("answer_questions", new AnswerQuestionsCommand());
        commands.put("create_question", new CreateQuestionCommand());
        commands.put("create_survey", new CreateSurveyCommand());
        commands.put("delete_questions", new DeleteQuestionsCommand());
        commands.put("delete_questions_from_survey", new DeleteQuestionsFromSurveyCommand());
        commands.put("delete_survey", new DeleteSurveyCommand());
        commands.put("go_to_admin_page", new GoToAdminPageCommand());
        commands.put("go_to_create_question_page", new GoToCreateQuestionPageCommand());
        commands.put("go_to_create_survey_page", new GoToCreateSurveyPageCommand());
        commands.put("go_to_themes_list_for_adding_questions_in_survey",
                new GoToThemesListForAddingQuestionsInSurveyCommand());
        commands.put("go_to_themes_list_for_deleting_question", new GoToThemesListForDeletingQuestionCommand());
        commands.put("pass_survey", new PassSurveyCommand());
        commands.put("registration", new RegistrationCommand());
        commands.put("show_all_surveys", new ShowAllSurveysCommand());
        commands.put("show_answers_by_user", new ShowAnswersByUserCommand());
        commands.put("show_answers_for_question", new ShowAnswersForQuestionCommand());
        commands.put("show_questions_in_passed_survey", new ShowQuestionsInPassedSurveyCommand());
        commands.put("show_questions_in_survey", new ShowQuestionsInSurveyCommand());
        commands.put("show_questions_list_for_adding_to_survey_by_theme",
                new ShowQuestionsListForAddingToSurveyByThemeCommand());
        commands.put("show_questions_list_with_answers", new ShowQuestionsListWithAnswersCommand());
        commands.put("show_questions_list_for_deleting_by_theme", new ShowQuestionsListForDeletingByThemeCommand());
        commands.put("show_questions_list_for_deleting_from_survey",
                new ShowQuestionsListForDeletingFromSurveyCommand());
        commands.put("show_surveys_list_by_theme", new ShowSurveysListByThemeCommand());
        commands.put("show_surveys_passed_by_user", new ShowSurveysPassedByUserCommand());
        commands.put("show_themes_list", new ShowThemesListCommand());
        commands.put("show_users_passed_survey", new ShowUsersPassedSurveyCommand());
        commands.put("go_to_main_page", new GoToMainPageCommand());
        commands.put("go_to_order_survey_page", new GoToOrderSurveyPageCommand());
        commands.put("go_to_about_us_page", new GoToAboutUsPageCommand());
        commands.put("go_to_contacts_page", new GoToContactsPageCommand());
        commands.put("go_to_user_page", new GoToUserPageCommand());
        commands.put("go_to_registration_page", new GoToRegistrationPageCommand());
    }

    /**
     * Gets information from .jsp pages as HttpServletRequest object, passes this object to class, that process this information, set
     * results as attributes of request object and return link to needed page, then sends received information to the .jsp page.
     * 
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/jsp");
        String commandString = request.getParameter("command");
        ICommand command = commands.get(commandString);

        try
        {
            request.getRequestDispatcher(command.execute(request)).forward(request, response);
        }
        catch (CommandException e)
        {
            request.getRequestDispatcher(ExceptionHandlingUtil.handleException(request, e)).forward(request, response);
        }
    }
}
