package util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import exception.CommandException;

public class CommandUtil
{
    private static final Logger logger = Logger.getLogger(CommandUtil.class);

    private static final String SOURCE = "locale.message";

    /**
     * Reads and returns property value from properties file, that depends on current local.
     * 
     * @param language
     *            current language that was chosen by user or set by default
     * @param propertyKey
     *            property key in property file
     * @return property value by property key
     */
    public static String getMessage(String language, String propertyKey)
    {
        Locale current = new Locale(language);
        ResourceBundle resourceBundle = ResourceBundle.getBundle(SOURCE, current);
        return resourceBundle.getString(propertyKey);
    }

    /**
     * Gets password that was entered by user and calculate it's hash by MD5 algorithm
     * 
     * @param password
     *            password that was entered by user
     * @return calculated hash
     */
    public static String hash(String password) throws CommandException
    {
        MessageDigest messageDiggest;
        try
        {
            messageDiggest = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e)
        {
            String message = "Exception during password hashing";
            logger.error(message, e);
            throw new CommandException(message, e);
        }
        messageDiggest.reset();
        messageDiggest.update(password.getBytes());
        byte[] digest = messageDiggest.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String hashtext = bigInt.toString(16);
        return hashtext;
    }
}
