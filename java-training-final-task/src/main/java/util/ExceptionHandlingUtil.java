package util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;

public class ExceptionHandlingUtil
{
    public static final String EXCEPTION = "/WEB-INF/jsp/common/error.jsp";

    /**
     * Gets HttpServletRequest object and exception, that was catched in SurveyServlet class. Sets stack trace and exception message as
     * attributes to HttpServletRequest object, which will be return after that to user in SurveyServlet class. Returns the error page url,
     * to where servlet will send HttpServletRequest object.
     * 
     * @param request
     *            HttpServletRequest object which will have exception details as attributes
     * @param exception
     *            exception that was catched in SurveyServlet class
     * @return the error page url
     */
    public static String handleException(HttpServletRequest request, Exception exception)
    {
        request.setAttribute("details", ExceptionUtils.getStackTrace(exception));
        request.setAttribute("message", exception.getMessage());
        return EXCEPTION;
    }
}
