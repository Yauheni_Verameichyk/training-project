package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader
{
    /**
     * Gets property value from property file and returns it as String.
     * 
     * @param property
     *            property key in property file
     * @param fileName
     *            property file name in "resources" folder
     * @return property value by property key
     */
    public static String getProperty(String property, String fileName) throws IOException
    {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Properties prop = new Properties();
        InputStream input = classLoader.getResourceAsStream(fileName);
        prop.load(input);
        return prop.getProperty(property);
    }

    /**
     * Gets content of property file and returns it as a Properties object.
     * 
     * @param fileName
     *            property file name in "resources" folder
     * @return content of property file
     */
    public static Properties getPropertyFile(String fileName) throws IOException
    {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Properties prop = new Properties();
        InputStream input = classLoader.getResourceAsStream(fileName);
        prop.load(input);
        return prop;
    }
}
