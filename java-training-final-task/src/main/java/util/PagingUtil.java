package util;

import java.io.IOException;

import org.apache.log4j.Logger;

import exception.CommandException;

public class PagingUtil
{
    private static final Logger logger = Logger.getLogger(PagingUtil.class);

    static final String RECORDS_PER_PAGE = "records.per.page";
    static final String CURRENT_PAGE = "current.page";
    static final String CONFIG = "config.properties";
    static final String ERROR_MESSAGE = "Exception during paging";

    /**
     * Gets number of required page as String and return it as int. If number of required page is null, read default value from
     * "config.properties" file (by default equal to 1) and return it.
     * 
     * @param pageNum
     *            number of required page as String
     * @return number of required page as int
     */
    public static int getCurrentPage(String pageNum) throws CommandException
    {
        int currentPage;
        if (pageNum == null)
        {
            try
            {
                currentPage = new Integer(PropertyReader.getProperty(CURRENT_PAGE, CONFIG));
            }
            catch (NumberFormatException | IOException e)
            {
                logger.error(ERROR_MESSAGE, e);
                throw new CommandException(ERROR_MESSAGE, e);
            }
        }
        else
        {
            currentPage = new Integer(pageNum);
        }
        return currentPage;
    }

    /**
     * Gets number of rows or entities that is necessary to show and rows number per page. Based on this calculates and return pages number.
     * 
     * @param rowsNumber
     *            number of rows or entities that is necessary to show on page
     * @param recordsPerPage
     *            rows number per page
     * @return pages number
     */
    public static int getPagesNumber(int rowsNumber, int recordsPerPage)
    {
        int pagesNumber;
        if ((rowsNumber % recordsPerPage) != 0)
        {
            pagesNumber = rowsNumber / recordsPerPage + 1;
        }
        else
        {
            pagesNumber = rowsNumber / recordsPerPage;
        }
        return pagesNumber;
    }

    /**
     * Reads records per page number from "config.properties" file (by default equal to 5) and return it.
     * 
     * @return records per page number
     */
    public static int getRecordsPerPage() throws CommandException
    {
        try
        {
            return new Integer(PropertyReader.getProperty(RECORDS_PER_PAGE, CONFIG));
        }
        catch (NumberFormatException | IOException e)
        {
            logger.error(ERROR_MESSAGE, e);
            throw new CommandException(ERROR_MESSAGE, e);
        }
    }
}
