package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import entity.Role;
import entity.Sex;
import entity.Survey;
import entity.User;
import exception.DAOException;

public class UserDAO extends AbstractDAO
{
    private static final Logger logger = Logger.getLogger(QuestionDAO.class);

    private static final String DELETE = "DELETE FROM users WHERE UserID=?;";
    private static final String INSERT =
            "INSERT INTO users(Username, Password, Sex, BirthDate, Email, RoleID) "
                    + "VALUES (?, ?, ?, ?, ?, (SELECT RoleID FROM roles WHERE RoleName = ?));";
    private static final String SELECT_BY_USERNAME_AND_PASSWORD =
            "SELECT users.UserID, users.Username, users.Password, users.Sex, users.BirthDate, users.Email, RoleName "
                    + "FROM users JOIN roles ON users.RoleID = roles.RoleID WHERE Username = ? and Password = ?;";
    private static final String SELECT_BY_USERNAME =
            "SELECT users.UserID, users.Username, users.Password, users.Sex, users.BirthDate, users.Email, RoleName "
                    + "FROM users JOIN roles ON users.RoleID = roles.RoleID WHERE Username = ?;";
    private static final String SELECT_BY_SURVEY_NAME =
            "SELECT distinct users.UserID, users.Username, users.Password, users.Sex, users.BirthDate, users.Email, RoleName "
                    + "FROM surveydb.users JOIN answers ON users.UserID = answers.UserID "
                    + "JOIN surveys ON answers.SurveyID = surveys.SurveyID "
                    + "JOIN roles ON users.RoleID = roles.RoleID WHERE SurveyName = ? LIMIT ?, ?;";
    private static final String SELECT_USERS_NUMBER = "SELECT count(distinct Username) as UsersNumber from users";
    private static final String COUNT_USERS_BY_SURVEYNAME =
            "SELECT count(distinct  users.UserID) as UsersNumber FROM surveydb.users "
                    + "JOIN answers ON users.UserID = answers.UserID JOIN surveys ON answers.SurveyID = surveys.SurveyID "
                    + "JOIN roles ON users.RoleID = roles.RoleID WHERE SurveyName = ?";

    public UserDAO() throws DAOException
    {
    }

    /**
     * Deletes user from the database by UserID
     * 
     * @param id
     *            user ID in the database
     */
    public void delete(Long id) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(DELETE);
            ps.setLong(1, id);
            ps.executeUpdate();
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during element removal";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Inserts user into the database
     * 
     * @param user
     *            User object that should be inserted into the database
     * @return inserted user's UserID
     */
    public long create(User user) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            long generatedKey = setValuesForPreparedStatement(ps, user).executeUpdate();
            return generatedKey;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during element insertion";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads user from the database by Username and Password and returns it.
     * 
     * @param username
     * @param password
     * @return user
     */
    public User getByUsernameAndPass(String username, String password) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_BY_USERNAME_AND_PASSWORD);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst())
            {
                return null;
            }
            rs.next();
            return createUser(rs);
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading user from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads users number from the database and returns it.
     * 
     * @return users number
     */
    public Integer getUsersNumber() throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_USERS_NUMBER);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getInt("UsersNumber");
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads user from the database by Username and returns it.
     * 
     * @param username
     * @return user
     */
    public User getByUsername(String username) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_BY_USERNAME);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst())
            {
                return null;
            }
            rs.next();
            return createUser(rs);
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading user from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns users list from the database that passed {@link Survey}. Ordinal numbers of users, that included in users list and
     * users number depend on "pageNum" and "pageLength".
     * 
     * @param pageNum
     *            page number, on which will be shown users list
     * @param pageLength
     *            number of users on page
     * @return users list that passed {@link Survey}
     */
    public List<User> getBySurveyName(String surveyName, int pageNum, int pageLength) throws DAOException
    {
        List<User> usersList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_BY_SURVEY_NAME);
            ps.setString(1, surveyName);
            ps.setInt(2, (pageNum - 1) * pageLength);
            ps.setInt(3, pageLength);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst())
            {
                usersList = null;
            }
            while (rs.next())
            {
                usersList.add(createUser(rs));
            }
            return usersList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading users from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns all users number from the database.
     * 
     * @return users number
     */
    public int getUsersNumberBySurveyName(String surveyName) throws DAOException
    {
        Connection connection = null;
        PreparedStatement rowsNumberPS = null;
        try
        {
            connection = connectionPool.getConnection();
            rowsNumberPS = connection.prepareStatement(COUNT_USERS_BY_SURVEYNAME);
            rowsNumberPS.setString(1, surveyName);
            ResultSet rowsNumberRS = rowsNumberPS.executeQuery();
            int rowsNumber = 0;
            if (rowsNumberRS.next())
            {
                rowsNumber = rowsNumberRS.getInt("UsersNumber");
            }
            return rowsNumber;
        }
        catch (InterruptedException | SQLException e)
        {
            String message = "Exception during users number reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(rowsNumberPS);
        }
    }

    /**
     * Creates user, initializes it's fields with values gotten from ResultSet rs and return it.
     * 
     * @param rs
     *            ResultSet object that was gotten from the database
     * @return created user
     */
    public static User createUser(ResultSet rs) throws SQLException, DAOException
    {
        User user = new User();
        user.setUserID(rs.getLong("UserID"));
        user.setUsername(rs.getString("Username"));
        user.setPassword(rs.getString("Password"));
        String sex = rs.getString("Sex");
        if (sex != null)
        {
            user.setSex(Sex.getByValue(sex));
        }
        user.setEmail(rs.getString("Email"));
        user.setBirthDate(rs.getDate("BirthDate"));
        user.setRole(Role.valueOf(rs.getString("RoleName")));
        return user;
    }

    /**
     * Initializes PreparedStatement parameters with values of User object
     * 
     * @param ps
     *            PreparedStatement which should be initialized
     * @param user
     *            User object which values are used to initialize PrepareStatement
     * @return initialized PreparedStatement object
     */
    private PreparedStatement setValuesForPreparedStatement(PreparedStatement ps, User user)
        throws SQLException, DAOException
    {
        ps.setString(1, user.getUsername());
        ps.setString(2, user.getPassword());
        if (user.getSex() != null)
        {
            ps.setString(3, user.getSex().getValue());
        }
        else
        {
            ps.setNull(3, Types.VARCHAR);
        }
        if (user.getBirthDate() != null)
        {
            ps.setDate(4, new java.sql.Date(user.getBirthDate().getTime()));
        }
        else
        {
            ps.setNull(4, Types.DATE);
        }
        ps.setString(5, user.getEmail());
        ps.setString(6, user.getRole().toString());
        return ps;
    }
}
