package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import connection.JDBCConnectionPool;
import exception.DAOException;

public abstract class AbstractDAO
{
    private static final Logger logger = Logger.getLogger(AbstractDAO.class);

    protected JDBCConnectionPool connectionPool;

    public AbstractDAO() throws DAOException
    {
        try
        {
            connectionPool = JDBCConnectionPool.getConnectionPool();

        }
        catch (NumberFormatException | IOException | SQLException e)
        {
            String message = "Exception in connection establishment";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
    }

    /**
     * Closes PrepareStatement object.
     * 
     * @param ps
     *            PrepareStatement object that must be closed
     */
    protected void closeStatement(PreparedStatement ps) throws DAOException
    {
        try
        {
            if (ps != null)
            {
                ps.close();
            }
        }
        catch (SQLException e)
        {
            String message = "Exception in statement closing";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
    }

    /**
     * Returns connection to connections pool.
     * 
     * @param connection
     */
    protected void returnConnectionToPool(Connection connection) throws DAOException
    {
        try
        {
            if (connection != null)
            {
                connectionPool.returnConnection(connection);
            }
        }
        catch (InterruptedException e)
        {
            String message = "Exception in connection returning";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
    }
}
