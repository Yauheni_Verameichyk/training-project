package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import exception.DAOException;

public class SurveyQuestionDAO extends AbstractDAO
{
    private static final Logger logger = Logger.getLogger(SurveyQuestionDAO.class);

    private static final String INSERT_QUESTION_INTO_SURVEY =
            "INSERT INTO surveys_m2m_questions(SurveyID, QuestionID) VALUES (?, ?)";
    private static final String DELETE_QUESTION_FROM_SURVEY =
            "DELETE FROM surveys_m2m_questions WHERE SurveyID = ? AND QuestionID = ?";
    private static final String DELETE_ANSWERS = "DELETE FROM answers WHERE SurveyID=? AND QuestionID = ?";
    private static final String SELECT_SURVEY_ID_BY_SURVEY_NAME =
            "SELECT surveys_m2m_questions.SurveyID FROM surveys_m2m_questions "
                    + "JOIN surveys ON surveys_m2m_questions.SurveyID = surveys.SurveyID WHERE SurveyName = ?";

    public SurveyQuestionDAO() throws DAOException
    {
        super();
    }

    public void addQuestionToSurvey(Long surveyID, Long questionID) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(INSERT_QUESTION_INTO_SURVEY);
            ps.setLong(1, surveyID);
            ps.setLong(2, questionID);
            ps.executeUpdate();
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during insertion";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    public void deleteQuestionFromSurvey(Long surveyID, Long questionID) throws DAOException
    {
        Connection connection = null;
        try
        {
            connection = connectionPool.getConnection();
            connection.setAutoCommit(false);
            delete(surveyID, questionID, DELETE_ANSWERS, connection);
            delete(surveyID, questionID, DELETE_QUESTION_FROM_SURVEY, connection);
            connection.commit();
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during removal";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
        }
    }

    /**
     * Deletes answer or question from survey by QuestionID
     * 
     * @param id
     *            question ID in the database
     */
    private void delete(Long surveyID, Long questionID, String query, Connection connection) throws SQLException
    {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query))
        {
            preparedStatement.setLong(1, surveyID);
            preparedStatement.setLong(2, questionID);
            preparedStatement.executeUpdate();
        }
    }

    /**
     * Checks if Survey is empty (not contain any question)
     * 
     * @param surveyName
     *            name of the survey to be checked
     * @return boolean result of is empty check
     */
    public boolean isEmpty(String surveyName) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_SURVEY_ID_BY_SURVEY_NAME);
            ps.setString(1, surveyName);
            ResultSet rs = ps.executeQuery();
            return !rs.isBeforeFirst();
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }
}
