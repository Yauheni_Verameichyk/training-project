package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import entity.Answer;
import entity.Question;
import entity.Survey;
import entity.Theme;
import exception.DAOException;

public class QuestionDAO extends AbstractDAO
{
    private static final Logger logger = Logger.getLogger(QuestionDAO.class);

    private static final String SELECT_ALL =
            "SELECT QuestionID, QuestionBody, ThemeName FROM questions JOIN themes ON questions.ThemeID = themes.ThemeID LIMIT ?, ?;";
    private static final String SELECT_BY_THEME =
            "SELECT QuestionID, QuestionBody, ThemeName FROM surveydb.questions JOIN themes ON themes.ThemeID = questions.ThemeID "
                    + "WHERE ThemeName = ? LIMIT ?, ?;";
    private static final String DELETE = "DELETE FROM questions WHERE QuestionID=?;";
    private static final String DELETE_FROM_SURVEYS = "DELETE FROM surveys_m2m_questions WHERE QuestionID = ?";
    private static final String DELETE_ANSWERS = "DELETE FROM answers WHERE QuestionID=?";
    private static final String INSERT =
            "INSERT INTO questions(QuestionBody, ThemeID) VALUES (?, (SELECT ThemeID FROM themes WHERE ThemeName = ?))";
    private static final String SELECT_QUESTIONS_FROM_SURVEY =
            "SELECT questions.QuestionID, questions.QuestionBody, ThemeName FROM questions "
                    + "JOIN surveys_m2m_questions ON questions.QuestionID = surveys_m2m_questions.QuestionID "
                    + "JOIN surveys ON surveys_m2m_questions.SurveyID = surveys.SurveyID "
                    + "JOIN themes ON themes.ThemeID = questions.ThemeID WHERE surveys.SurveyName = ? LIMIT ?, ?;";
    private static final String SELECT_QUESTIONS_FROM_SURVEY_WITHOUT_PAGING =
            "SELECT questions.QuestionID, questions.QuestionBody, ThemeName FROM questions "
                    + "JOIN surveys_m2m_questions ON questions.QuestionID = surveys_m2m_questions.QuestionID "
                    + "JOIN surveys ON surveys_m2m_questions.SurveyID = surveys.SurveyID "
                    + "JOIN themes ON themes.ThemeID = questions.ThemeID WHERE surveys.SurveyName = ? ";
    private static final String SELECT_QUESTION_AND_ANSWER_BY_USERNAME_AND_SURVEYNAME =
            "SELECT questions.QuestionID, QuestionBody, ThemeName, answers.AnswerID, AnswerBody, answers.QuestionID, "
                    + "answers.SurveyID, answers.UserID, answers.AnswerDate FROM questions "
                    + "JOIN surveys_m2m_questions ON surveys_m2m_questions.QuestionID = questions.QuestionID "
                    + "JOIN answers ON surveys_m2m_questions.QuestionID = answers.QuestionID "
                    + "JOIN surveys ON answers.SurveyID = surveys.SurveyID "
                    + "JOIN themes ON themes.ThemeID = questions.ThemeID JOIN users ON users.UserID = answers.UserID "
                    + "WHERE Username = ? AND SurveyName = ? LIMIT ?, ?;";
    private static final String SELECT_QUESTIONS_AND_TIMES_ANSWERED_FROM_SURVEY =
            "SELECT questions.QuestionID, questions.QuestionBody, ThemeName, (SELECT count(UserID) "
                    + "from answers where questions.QuestionID =  answers.QuestionID "
                    + "AND surveys.SurveyID = answers.SurveyID) AS TimesAnswered "
                    + "FROM questions JOIN surveys_m2m_questions ON questions.QuestionID = surveys_m2m_questions.QuestionID "
                    + "JOIN themes ON themes.ThemeID = questions.ThemeID "
                    + "JOIN surveys ON surveys_m2m_questions.SurveyID = surveys.SurveyID  WHERE SurveyName = ? LIMIT ?, ?";
    private final static String COUNT_QUESTIONS =
            "SELECT count(distinct  QuestionID) as QuestionsNumber from questions";
    private static final String COUNT_QUESTIONS_IN_SURVEY =
            "SELECT count(distinct  questions.QuestionID) as QuestionsNumber FROM questions "
                    + "JOIN surveys_m2m_questions ON questions.QuestionID = surveys_m2m_questions.QuestionID "
                    + "JOIN surveys ON surveys_m2m_questions.SurveyID = surveys.SurveyID WHERE surveys.SurveyName = ?";
    private static final String COUNT_QUESTIONS_AND_ANSWERS_BY_USERNAME_AND_SURVEYNAME =
            "SELECT count(distinct  questions.QuestionID) as QuestionsNumber FROM questions "
                    + "JOIN surveys_m2m_questions ON surveys_m2m_questions.QuestionID = questions.QuestionID "
                    + "JOIN answers ON surveys_m2m_questions.QuestionID = answers.QuestionID "
                    + "JOIN surveys ON surveys_m2m_questions.SurveyID = surveys.SurveyID "
                    + "JOIN users ON users.UserID = answers.UserID WHERE Username = ? AND SurveyName = ?;";
    private static final String COUNT_QUESTIONS_BY_THEME =
            "SELECT count(distinct  QuestionID) as QuestionsNumber FROM surveydb.questions "
                    + "JOIN themes ON themes.ThemeID = questions.ThemeID WHERE ThemeName = ?;";

    public QuestionDAO() throws DAOException
    {
    }

    /**
     * Reads questions from the database and returns pair of questions list and all questions number. Ordinal numbers of questions, that
     * included in questions list and questions number depend on "pageNum" and "pageLength".
     * 
     * @param pageNum
     *            page number, on which will be shown questions list
     * @param pageLength
     *            number of questions on page
     * @return pair of questions list and all questions number
     */
    public List<Question> getAll(int pageNum, int pageLength) throws DAOException
    {
        List<Question> questionsList = new LinkedList<Question>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_ALL);
            ps.setInt(1, (pageNum - 1) * pageLength);
            ps.setInt(2, pageLength);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                questionsList.add(createQuestion(rs));
            }
            return questionsList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading questions from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns all questions number from the database.
     * 
     * @return questions number
     */
    public int getAllQuestionsNumber() throws DAOException
    {
        Connection connection = null;
        PreparedStatement rowsNumberPS = null;
        try
        {
            connection = connectionPool.getConnection();
            rowsNumberPS = connection.prepareStatement(COUNT_QUESTIONS);
            ResultSet rowsNumberRS = rowsNumberPS.executeQuery();
            int rowsNumber = 0;
            if (rowsNumberRS.next())
            {
                rowsNumber = rowsNumberRS.getInt("QuestionsNumber");
            }
            return rowsNumber;
        }
        catch (InterruptedException | SQLException e)
        {
            String message = "Exception during questions number reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(rowsNumberPS);
        }
    }

    /**
     * Deletes question from the database by QuestionID
     * 
     * @param id
     *            question ID in the database
     */
    public void deleteQuestion(Long id) throws DAOException
    {
        Connection connection = null;
        try
        {
            connection = connectionPool.getConnection();
            connection.setAutoCommit(false);
            delete(id, DELETE_ANSWERS, connection);
            delete(id, DELETE_FROM_SURVEYS, connection);
            delete(id, DELETE, connection);
            connection.commit();
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during element removal";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
        }
    }

    /**
     * Deletes answer, question from survey or question from the database by QuestionID
     * 
     * @param id
     *            question ID in the database
     */
    private void delete(Long id, String query, Connection connection) throws SQLException
    {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query))
        {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        }
    }

    /**
     * Inserts question into the database
     * 
     * @param question
     *            Question object that should be inserted into the database
     * @return inserted question's QuestionID
     */
    public long create(Question question) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            long generatedKey = setValuesForPreparedStatement(ps, question).executeUpdate();
            return generatedKey;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during element insertion";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns list of questions from {@link Survey}
     * 
     * @param surveyName
     * @return questionsList
     */
    public List<Question> getQuestionsFromSurvey(String surveyName)
        throws DAOException
    {
        List<Question> questionsList = new LinkedList<Question>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_QUESTIONS_FROM_SURVEY_WITHOUT_PAGING);
            ps.setString(1, surveyName);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                questionsList.add(createQuestion(rs));
            }
            return questionsList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading questions from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads questions from {@link Survey} and returns questions in survey list. Ordinal numbers of questions, that included in questions
     * list and questions number depend on "pageNum" and "pageLength".
     * 
     * @param surveyName
     * @param pageNum
     *            page number, on which will be shown questions list
     * @param pageLength
     *            number of questions on page
     * @return questions in survey list
     */
    public List<Question> getQuestionsFromSurvey(String surveyName, int pageNum, int pageLength)
        throws DAOException
    {
        List<Question> questionsList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_QUESTIONS_FROM_SURVEY);
            ps.setString(1, surveyName);
            ps.setInt(2, (pageNum - 1) * pageLength);
            ps.setInt(3, pageLength);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                questionsList.add(createQuestion(rs));
            }
            return questionsList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading questions from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns questions in survey number from the database.
     * 
     * @return questions in survey number
     */
    public int getQuestionsNumberInSurvey(String surveyName) throws DAOException
    {
        Connection connection = null;
        PreparedStatement rowsNumberPS = null;
        try
        {
            connection = connectionPool.getConnection();
            rowsNumberPS = connection.prepareStatement(COUNT_QUESTIONS_IN_SURVEY);
            rowsNumberPS.setString(1, surveyName);
            ResultSet rowsNumberRS = rowsNumberPS.executeQuery();
            int rowsNumber = 0;
            if (rowsNumberRS.next())
            {
                rowsNumber = rowsNumberRS.getInt("QuestionsNumber");
            }
            return rowsNumber;
        }
        catch (InterruptedException | SQLException e)
        {
            String message = "Exception during questions number reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(rowsNumberPS);
        }
    }

    /**
     * Reads and returns list of pairs of question and answer from {@link Survey} that were passed by {@link User} from the database.
     * Ordinal numbers of pairs, that included in pairs list and pairs number depend on "pageNum" and "pageLength".
     * 
     * @param username
     *            username by which questions and answers will be searched
     * @param surveyName
     *            Survey name by which questions and answers will be searched
     * @param pageNum
     *            page number, on which will be shown users list
     * @param pageLength
     *            number of users on page
     * @return list of pairs of question and answer
     */
    public List<Pair<Question, Answer>> getQuestionsAndAnswersListByUsernameAndSurveyName(
        String username, String surveyName, int pageNum, int pageLength) throws DAOException
    {
        List<Pair<Question, Answer>> questionsAnswersList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_QUESTION_AND_ANSWER_BY_USERNAME_AND_SURVEYNAME);
            ps.setString(1, username);
            ps.setString(2, surveyName);
            ps.setInt(3, (pageNum - 1) * pageLength);
            ps.setInt(4, pageLength);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst())
            {
                questionsAnswersList = null;
            }
            while (rs.next())
            {
                questionsAnswersList.add(Pair.of(createQuestion(rs), AnswerDAO.createAnswer(rs)));
            }
            return questionsAnswersList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading questions from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns number of pairs of question and answer from {@link Survey} that were passed by {@link User} from the database.
     * 
     * @return pairs number
     */
    public int getQuestionsAndAnswersNumberByUsernameAndSurveyName(String username, String surveyName)
        throws DAOException
    {
        Connection connection = null;
        PreparedStatement rowsNumberPS = null;
        try
        {
            connection = connectionPool.getConnection();
            rowsNumberPS = connection.prepareStatement(COUNT_QUESTIONS_AND_ANSWERS_BY_USERNAME_AND_SURVEYNAME);
            rowsNumberPS.setString(1, username);
            rowsNumberPS.setString(2, surveyName);
            ResultSet rowsNumberRS = rowsNumberPS.executeQuery();
            int rowsNumber = 0;
            if (rowsNumberRS.next())
            {
                rowsNumber = rowsNumberRS.getInt("QuestionsNumber");
            }
            return rowsNumber;
        }
        catch (InterruptedException | SQLException e)
        {
            String message = "Exception during questions and answers number reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(rowsNumberPS);
        }
    }

    /**
     * Reads and returns list of pairs of question and how many users answered question from {@link Survey} from the database. Ordinal
     * numbers of pairs, that included in pairs list and pairs number depend on "pageNum" and "pageLength".
     * 
     * @param surveyName
     *            Survey name by which questions and users number will be searched
     * @param pageNum
     *            page number, on which will be shown users list
     * @param pageLength
     *            number of users on page
     * @return list of pairs of question and users number, that answered question
     */
    public List<Pair<Question, Integer>> getQuestionsAndTimesAnsweredFromSurvey(String surveyName,
        int pageNum, int pageLength)
        throws DAOException
    {
        List<Pair<Question, Integer>> questionsList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_QUESTIONS_AND_TIMES_ANSWERED_FROM_SURVEY);
            ps.setString(1, surveyName);
            ps.setInt(2, (pageNum - 1) * pageLength);
            ps.setInt(3, pageLength);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst())
            {
                questionsList = null;
            }
            while (rs.next())
            {
                questionsList.add(Pair.of(createQuestion(rs), rs.getInt("TimesAnswered")));
            }
            return questionsList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading questions from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns questions list from the database by theme. Ordinal numbers of questions, that included in questions list and
     * questions number depend on "pageNum" and "pageLength".
     * 
     * @param theme
     *            theme name by which questions will be searched
     * @param pageNum
     *            page number, on which will be shown users list
     * @param pageLength
     *            number of users on page
     * @return questions list by theme
     */
    public List<Question> getByTheme(String theme, int pageNum, int pageLength) throws DAOException
    {
        List<Question> questionsList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_BY_THEME);
            ps.setString(1, theme);
            ps.setInt(2, (pageNum - 1) * pageLength);
            ps.setInt(3, pageLength);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                questionsList.add(createQuestion(rs));
            }
            return questionsList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading questions from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns questions number by theme from the database.
     * 
     * @return questions number by theme
     */
    public int getQuestionNumberByTheme(String theme) throws DAOException
    {
        Connection connection = null;
        PreparedStatement rowsNumberPS = null;
        try
        {
            connection = connectionPool.getConnection();
            rowsNumberPS = connection.prepareStatement(COUNT_QUESTIONS_BY_THEME);
            rowsNumberPS.setString(1, theme);
            ResultSet rowsNumberRS = rowsNumberPS.executeQuery();
            int rowsNumber = 0;
            if (rowsNumberRS.next())
            {
                rowsNumber = rowsNumberRS.getInt("QuestionsNumber");
            }
            return rowsNumber;
        }
        catch (InterruptedException | SQLException e)
        {
            String message = "Exception during questions number reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(rowsNumberPS);
        }
    }

    /**
     * Creates question, initializes it's fields with values gotten from ResultSet rs and return it.
     * 
     * @param rs
     *            ResultSet object that was gotten from the database
     * @return created question
     */
    public static Question createQuestion(ResultSet rs) throws DAOException, SQLException
    {
        Question question = new Question();
        question.setQuestionID(rs.getLong("QuestionID"));
        question.setQuestionBody(rs.getString("QuestionBody"));
        question.setTheme(Theme.valueOf(rs.getString("ThemeName")));
        return question;
    }

    /**
     * Initializes PreparedStatement parameters with values of Question object
     * 
     * @param ps
     *            PreparedStatement which should be initialized
     * @param user
     *            Question object which values are used to initialize PrepareStatement
     * @return initialized PreparedStatement object
     */
    private PreparedStatement setValuesForPreparedStatement(PreparedStatement ps, Question question)
        throws SQLException, DAOException
    {
        ps.setString(1, question.getQuestionBody());
        ps.setString(2, question.getTheme().toString());
        return ps;
    }
}
