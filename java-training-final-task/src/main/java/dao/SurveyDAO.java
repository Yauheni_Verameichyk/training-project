package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import entity.Survey;
import exception.DAOException;

public class SurveyDAO extends AbstractDAO
{
    public SurveyDAO() throws DAOException
    {
    }

    private static final Logger logger = Logger.getLogger(SurveyDAO.class);

    private static final String SELECT_BY_SURVEY_NAME = "SELECT * FROM surveys WHERE SurveyName = ?";
    private static final String DELETE = "DELETE FROM surveys WHERE SurveyID=?";
    private static final String DELETE_FROM_SURVEYS = "DELETE FROM surveys_m2m_questions WHERE SurveyID = ?";
    private static final String DELETE_ANSWERS = "DELETE FROM answers WHERE SurveyID=?";
    private static final String INSERT = "INSERT INTO surveys(SurveyName) VALUES (?)";
    private static final String SELECT_SURVEYS_AND_QUESTION_NUMBER =
            "SELECT SurveyName, (SELECT COUNT(QuestionID) FROM surveys_m2m_questions "
                    + "WHERE SurveyID = surveys.SurveyID) AS QuestionsNumber FROM surveys LIMIT ?, ?;";
    private static final String COUNT_SURVEYS =
            "SELECT count(distinct  surveys.SurveyID) as SurveysNumber from surveys;";
    private static final String SELECT_SURVEY_BY_THEME =
            "SELECT distinct surveys.SurveyName, (SELECT COUNT(QuestionID) "
                    + "FROM surveys_m2m_questions WHERE SurveyID = surveys.SurveyID) AS QuestionsNumber from surveys "
                    + "join surveys_m2m_questions on surveys.SurveyID = surveys_m2m_questions.SurveyID "
                    + "join questions on surveys_m2m_questions.QuestionID = questions.QuestionID "
                    + "join themes on questions.ThemeID = themes.ThemeID where themes.ThemeName = ? LIMIT ?, ?;";
    private static final String COUNT_SURVEYS_BY_THEME =
            "SELECT count(distinct  surveys.SurveyID) as SurveysNumber "
                    + "from surveys join surveys_m2m_questions on surveys.SurveyID = surveys_m2m_questions.SurveyID "
                    + "join questions on surveys_m2m_questions.QuestionID = questions.QuestionID "
                    + "join themes on questions.ThemeID = themes.ThemeID "
                    + "where themes.ThemeName = ?;";
    private static final String SELECT_SURVEY_PASSED_BY_USER =
            "SELECT distinct surveys.SurveyName, (SELECT COUNT(QuestionID) FROM surveys_m2m_questions "
                    + "WHERE SurveyID = surveys.SurveyID) AS QuestionsNumber FROM surveys "
                    + "JOIN surveys_m2m_questions ON surveys.SurveyID = surveys_m2m_questions.SurveyID "
                    + "JOIN answers ON surveys_m2m_questions.SurveyID = answers.SurveyID "
                    + "JOIN users ON answers.UserID = users.UserID WHERE Username = ? LIMIT ?, ?;";
    private static final String COUNT_SURVEYS_BY_USER =
            "SELECT count(distinct  surveys.SurveyID) as SurveysNumber "
                    + "from surveys JOIN surveys_m2m_questions ON surveys.SurveyID = surveys_m2m_questions.SurveyID "
                    + "JOIN answers ON surveys_m2m_questions.SurveyID = answers.SurveyID "
                    + "JOIN users ON answers.UserID = users.UserID WHERE Username = ?";
    private static final String SELECT_SURVEY_BY_SURVEY_NAME_AND_USERNAME =
            "SELECT distinct surveys.SurveyName  FROM surveys "
                    + "JOIN surveys_m2m_questions ON surveys.SurveyID = surveys_m2m_questions.SurveyID "
                    + "JOIN answers ON surveys_m2m_questions.SurveyID = answers.SurveyID "
                    + "JOIN users ON answers.UserID = users.UserID WHERE Username = ? AND SurveyName = ?";
    private static final String SELECT_POPULAR =
            "SELECT surveys.SurveyID, surveys.SurveyName FROM answers JOIN surveys ON surveys.SurveyID=answers.SurveyID  "
                    + "GROUP BY surveys.SurveyID ORDER BY Count(DISTINCT UserID) DESC limit ?";

    /**
     * Deletes survey from the database by SurveyName
     * 
     * @param surveyID
     */
    public void delete(Long surveyID) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            connection.setAutoCommit(false);
            delete(surveyID, DELETE_ANSWERS, connection);
            delete(surveyID, DELETE_FROM_SURVEYS, connection);
            delete(surveyID, DELETE, connection);
            connection.commit();
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during element removal";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Deletes answer, questions from survey or survey from the database by SurveyID
     * 
     * @param id
     *            Survey ID in the database
     */
    private void delete(Long id, String query, Connection connection) throws SQLException
    {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query))
        {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        }
    }

    /**
     * Inserts survey into the database
     * 
     * @param survey
     *            Survey object that should be inserted into the database
     * @return inserted survey's SurveyID
     */
    public long create(Survey survey) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, survey.getSurveyName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            long generatedKey = rs.getLong(1);
            return generatedKey;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during element insertion";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns list of pairs of Survey name and questions in survey number. Ordinal numbers of pairs, that included in list and
     * pairs number depend on "pageNum" and "pageLength".
     * 
     * @param pageNum
     *            page number, on which will be shown users list
     * @param pageLength
     *            number of users on page
     * @return pair of surveys list and all surveys number
     */
    public List<Pair<String, Integer>> getAllSurveysAndQuestionsNumber(int pageNum, int pageLength)
        throws DAOException
    {
        List<Pair<String, Integer>> surveysList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_SURVEYS_AND_QUESTION_NUMBER);
            ps.setInt(1, (pageNum - 1) * pageLength);
            ps.setInt(2, pageLength);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                surveysList.add(Pair.of(rs.getString("SurveyName"), rs.getInt("QuestionsNumber")));
            }
            return surveysList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns all surveys number from the database.
     * 
     * @return surveys number
     */
    public int getAllSurveysNumber() throws DAOException
    {
        Connection connection = null;
        PreparedStatement rowsNumberPS = null;
        try
        {
            connection = connectionPool.getConnection();
            rowsNumberPS = connection.prepareStatement(COUNT_SURVEYS);
            ResultSet rowsNumberRS = rowsNumberPS.executeQuery();
            int rowsNumber = 0;
            if (rowsNumberRS.next())
            {
                rowsNumber = rowsNumberRS.getInt("SurveysNumber");
            }
            return rowsNumber;
        }
        catch (InterruptedException | SQLException e)
        {
            String message = "Exception during surveys number reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(rowsNumberPS);
        }
    }

    /**
     * Reads and returns list of pairs of survey name and questions number in survey from the database by theme. Ordinal numbers of pairs,
     * that included in pairs list and pairs number depend on "pageNum" and "pageLength".
     * 
     * @param theme
     *            theme name by which surveys will be searched
     * @param pageNum
     *            page number, on which will be shown users list
     * @param pageLength
     *            number of users on page
     * @return list of pairs of survey name and questions number in survey
     */
    public List<Pair<String, Integer>> getSurveysNameAndQuestionsNumberByTheme(String theme, int pageNum,
        int pageLength) throws DAOException
    {
        List<Pair<String, Integer>> surveysList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_SURVEY_BY_THEME);
            ps.setString(1, theme);
            ps.setInt(2, (pageNum - 1) * pageLength);
            ps.setInt(3, pageLength);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst())
            {
                surveysList = null;
            }
            while (rs.next())
            {
                surveysList.add(Pair.of(rs.getString("SurveyName"), rs.getInt("QuestionsNumber")));
            }
            return surveysList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading surveys from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns surveys number by theme from the database.
     * 
     * @return surveys number by theme
     */
    public int getSurveysNumberByTheme(String theme) throws DAOException
    {
        Connection connection = null;
        PreparedStatement rowsNumberPS = null;
        try
        {
            connection = connectionPool.getConnection();
            rowsNumberPS = connection.prepareStatement(COUNT_SURVEYS_BY_THEME);
            rowsNumberPS.setString(1, theme);
            ResultSet rowsNumberRS = rowsNumberPS.executeQuery();
            int rowsNumber = 0;
            if (rowsNumberRS.next())
            {
                rowsNumber = rowsNumberRS.getInt("SurveysNumber");
            }
            return rowsNumber;
        }
        catch (InterruptedException | SQLException e)
        {
            String message = "Exception during surveys number reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(rowsNumberPS);
        }
    }

    /**
     * Reads and returns list of pairs of survey name and questions number in survey, that were passed by {@link User} from the database.
     * Ordinal numbers of pairs, that included in pairs list and pairs number depend on "pageNum" and "pageLength".
     * 
     * @param username
     *            username by which surveys will be searched
     * @param pageNum
     *            page number, on which will be shown users list
     * @param pageLength
     *            number of users on page
     * @return list of pairs of survey name and questions number in survey
     */
    public List<Pair<String, Integer>> getByUsername(String username, int pageNum, int pageLength)
        throws DAOException
    {
        List<Pair<String, Integer>> surveysList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_SURVEY_PASSED_BY_USER);
            ps.setString(1, username);
            ps.setInt(2, (pageNum - 1) * pageLength);
            ps.setInt(3, pageLength);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst())
            {
                surveysList = null;
            }
            while (rs.next())
            {
                surveysList.add(Pair.of(rs.getString("SurveyName"), rs.getInt("QuestionsNumber")));
            }
            return surveysList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading surveys from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns surveys number by user from the database.
     * 
     * @return surveys number by user
     */
    public int getSurveysNumberByUser(String user) throws DAOException
    {
        Connection connection = null;
        PreparedStatement rowsNumberPS = null;
        try
        {
            connection = connectionPool.getConnection();
            rowsNumberPS = connection.prepareStatement(COUNT_SURVEYS_BY_USER);
            rowsNumberPS.setString(1, user);
            ResultSet rowsNumberRS = rowsNumberPS.executeQuery();
            int rowsNumber = 0;
            if (rowsNumberRS.next())
            {
                rowsNumber = rowsNumberRS.getInt("SurveysNumber");
            }
            return rowsNumber;
        }
        catch (InterruptedException | SQLException e)
        {
            String message = "Exception during surveys number reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(rowsNumberPS);
        }
    }

    /**
     * Reads survey from the database by SurveyName and returns it.
     * 
     * @param surveyName
     * @return survey
     */
    public Survey getByName(String surveyName) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_BY_SURVEY_NAME);
            ps.setString(1, surveyName);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return createSurvey(rs);
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading survey from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Checks if Survey "surveyName" is passed by User "username"
     * 
     * @param username
     * @param surveyName
     * @return true if Survey is passed by this User
     */
    public boolean isPassed(String username, String surveyName) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_SURVEY_BY_SURVEY_NAME_AND_USERNAME);
            ps.setString(1, username);
            ps.setString(2, surveyName);
            ResultSet rs = ps.executeQuery();
            return rs.isBeforeFirst();
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns list of surveys that were passed by the most number of users
     * 
     * @param popularSurveysNumber
     *            surveys number that should be read
     * @return popular surveys list
     */
    public List<Survey> getPopularSurveys(int popularSurveysNumber)
        throws DAOException
    {
        List<Survey> surveysList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_POPULAR);
            ps.setInt(1, popularSurveysNumber);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                surveysList.add(createSurvey(rs));
            }
            return surveysList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading surveys from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Creates survey, initializes it's fields with values gotten from ResultSet rs and returns it.
     * 
     * @param rs
     *            ResultSet object that was gotten from the database
     * @return created survey
     */
    private Survey createSurvey(ResultSet rs) throws SQLException
    {
        Survey survey = new Survey();
        survey.setSurveyID((long) rs.getInt("SurveyID"));
        survey.setSurveyName(rs.getString("SurveyName"));
        return survey;
    }
}
