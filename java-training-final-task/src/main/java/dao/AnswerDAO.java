package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import entity.Answer;
import exception.DAOException;

public class AnswerDAO extends AbstractDAO
{
    private static final Logger logger = Logger.getLogger(AnswerDAO.class);

    private static final String SELECT_BY_SURVEY_ID_USER_ID_AND_QUESTION_ID =
            "SELECT * FROM answers WHERE UserID = ? AND SurveyID = ? AND QuestionID = ? ";
    private static final String UPDATE =
            "UPDATE answers SET AnswerBody = ?,  UserID = ?, SurveyID = ?, QuestionID = ?, AnswerDate = ? WHERE AnswerID= ? ";
    private static final String DELETE_BY_QUESTION_ID = "DELETE FROM answers WHERE QuestionID=?";
    private static final String INSERT =
            "INSERT INTO answers(AnswerBody, UserID, SurveyID, QuestionID, AnswerDate) VALUES (?, ?, ?, ?, ?)";
    private final static String SELECT_BY_SURVEY_ID_AND_QUESTION_ID =
            "SELECT * from answers WHERE QuestionID = ? and SurveyID = ? LIMIT ?, ?;";
    private final static String COUNT_ANSWERS_BY_SURVEY_ID_AND_QUESTION_ID =
            "SELECT count(distinct UserID) as AnswersNumber from answers WHERE QuestionID = ? and SurveyID = ?";

    public AnswerDAO() throws DAOException
    {
    }

    /**
     * Updates existing answer in the database
     * 
     * @param answer
     */
    public void update(Answer answer) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(UPDATE);
            ps.setLong(6, answer.getAnswerID());
            setValuesForPreparedStatement(ps, answer).executeUpdate();
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during element update";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Deletes answer from the database by AnswerID
     * 
     * @param id
     *            answer ID in the database
     */
    public void delete(Long id) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(DELETE_BY_QUESTION_ID);
            ps.setLong(1, id);
            ps.executeUpdate();
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during element removal";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Inserts answer into the database
     * 
     * @param answer
     *            Answer object that should be inserted into the database
     * @return inserted answer's AnswerID
     */
    public long create(Answer answer) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            long generatedKey = setValuesForPreparedStatement(ps, answer).executeUpdate();
            return generatedKey;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during element insertion";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads answer from the database by UserID, SurveyID and QuestionID and returns it.
     * 
     * @param userID
     * @param surveyID
     * @param questionID
     * @return user
     */
    public Answer getByUserIDSurveyIDQuestionID(String userID, String surveyID, String questionID) throws DAOException
    {
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_BY_SURVEY_ID_USER_ID_AND_QUESTION_ID);
            ps.setString(1, userID);
            ps.setString(2, surveyID);
            ps.setString(3, questionID);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst())
            {
                return null;
            }
            rs.next();
            return createAnswer(rs);
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during reading answer from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads answers for certain question in certain survey from different users from the database and returns answers list. Ordinal numbers
     * of answers, that included in answers list and answers number depend on "pageNum" and "pageLength".
     * 
     * @param surveyID
     * @param questionID
     * @param pageNum
     *            page number, on which will be shown users list
     * @param pageLength
     *            number of users on page
     * @return answers list
     */
    public List<Answer> getByQuestionIDAndSurveyID(Long surveyID, Long questionID, int pageNum,
        int pageLength) throws DAOException
    {
        List<Answer> answersList = new LinkedList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        try
        {
            connection = connectionPool.getConnection();
            ps = connection.prepareStatement(SELECT_BY_SURVEY_ID_AND_QUESTION_ID);
            ps.setLong(1, questionID);
            ps.setLong(2, surveyID);
            ps.setInt(3, (pageNum - 1) * pageLength);
            ps.setInt(4, pageLength);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst())
            {
                answersList = null;
            }
            while (rs.next())
            {
                answersList.add(createAnswer(rs));
            }
            return answersList;
        }
        catch (SQLException | InterruptedException e)
        {
            String message = "Exception during answers reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(ps);
        }
    }

    /**
     * Reads and returns answers number for certain question in certain survey from different users from the database.
     * 
     * @param surveyID
     * @param questionID
     * @return answers number
     */
    public int getAnswersNumberByQuestionIDAndSurveyID(Long surveyID, Long questionID)
        throws DAOException
    {
        Connection connection = null;
        PreparedStatement rowsNumberPS = null;
        try
        {
            connection = connectionPool.getConnection();
            rowsNumberPS = connection.prepareStatement(COUNT_ANSWERS_BY_SURVEY_ID_AND_QUESTION_ID);
            rowsNumberPS.setLong(1, questionID);
            rowsNumberPS.setLong(2, surveyID);
            ResultSet rowsNumberRS = rowsNumberPS.executeQuery();
            int rowsNumber = 0;
            if (rowsNumberRS.next())
            {
                rowsNumber = rowsNumberRS.getInt("AnswersNumber");
            }
            return rowsNumber;
        }
        catch (InterruptedException | SQLException e)
        {
            String message = "Exception during answers number reading from the database";
            logger.error(message, e);
            throw new DAOException(message, e);
        }
        finally
        {
            returnConnectionToPool(connection);
            closeStatement(rowsNumberPS);
        }
    }

    /**
     * Creates answer, initializes it's fields with values gotten from ResultSet rs and return it.
     * 
     * @param rs
     *            ResultSet object that was gotten from the database
     * @return created answer
     */
    public static Answer createAnswer(ResultSet rs) throws SQLException
    {
        Answer answer = new Answer();
        answer.setAnswerID(rs.getLong("AnswerID"));
        answer.setAnswerBody(rs.getString("AnswerBody"));
        answer.setUserID(rs.getLong("UserID"));
        answer.setSurveyID(rs.getLong("SurveyID"));
        answer.setQuestionID(rs.getLong("QuestionID"));
        answer.setAnswerDate(rs.getDate("AnswerDate"));
        return answer;
    }

    /**
     * Initializes PreparedStatement parameters with values of Answer object
     * 
     * @param ps
     *            PreparedStatement which should be initialized
     * @param user
     *            Answer object which values are used to initialize PrepareStatement
     * @return initialized PreparedStatement object
     */
    private PreparedStatement setValuesForPreparedStatement(PreparedStatement ps, Answer answer) throws SQLException
    {
        ps.setString(1, answer.getAnswerBody());
        ps.setLong(2, answer.getUserID());
        ps.setLong(3, answer.getSurveyID());
        ps.setLong(4, answer.getQuestionID());
        ps.setDate(5, new java.sql.Date(answer.getAnswerDate().getTime()));
        return ps;
    }
}
