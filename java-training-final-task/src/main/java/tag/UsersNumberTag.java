package tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import dao.UserDAO;
import exception.DAOException;

public class UsersNumberTag extends TagSupport
{

    private static final Logger logger = Logger.getLogger(UsersNumberTag.class);
    private static final long serialVersionUID = -5667683788650966543L;

    /**
     * Reads registered users number from the database and show it on jsp page in castom tag.
     * 
     * @return registered users number
     */
    @Override
    public int doStartTag() throws JspException
    {
        try
        {
            UserDAO userDAO = new UserDAO();
            JspWriter out = pageContext.getOut();
            out.write(userDAO.getUsersNumber().toString());
        }
        catch (IOException | DAOException e)
        {
            String message = "Exception during tag handling";
            logger.error(message, e);
            throw new JspException(message, e);
        }
        return SKIP_BODY;
    }
}
