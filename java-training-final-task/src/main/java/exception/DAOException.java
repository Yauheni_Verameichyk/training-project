package exception;

public class DAOException extends Exception
{
    private static final long serialVersionUID = -685961634098008376L;

    public DAOException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DAOException(String message)
    {
        super(message);
    }
}
