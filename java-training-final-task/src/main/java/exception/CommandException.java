package exception;

public class CommandException extends Exception
{
    private static final long serialVersionUID = -685961634098008376L;

    public CommandException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public CommandException(String message)
    {
        super(message);
    }
}
