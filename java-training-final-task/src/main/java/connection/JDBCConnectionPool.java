package connection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import util.PropertyReader;

public class JDBCConnectionPool
{
    private static JDBCConnectionPool connectionPool;

    BlockingQueue<Connection> availableConnections;

    private JDBCConnectionPool() throws NumberFormatException, IOException
    {
        availableConnections = new ArrayBlockingQueue<Connection>(
                new Integer(PropertyReader.getProperty("connections.number", "config.properties")));
    }

    /**
     * Returns JDBCConnectionPool object. Implements pattern Singleton. If object doesn't exist method creates this object.
     * 
     * @return JDBCConnectionPool object
     */
    public static JDBCConnectionPool getConnectionPool() throws NumberFormatException, IOException, SQLException
    {
        if (connectionPool == null)
        {
            connectionPool = new JDBCConnectionPool();
            connectionPool.initializeConnectionPool();
        }
        return connectionPool;
    }

    /**
     * Gets connection from "availableConnections" and returns it. If no connection available waits until connection appears.
     * 
     * @return connection from "availableConnections"
     */
    public Connection getConnection() throws InterruptedException
    {
        return availableConnections.take();
    }

    /**
     * Returns connection back to connections pool.
     * 
     * @param connection
     */
    public void returnConnection(Connection connection) throws InterruptedException
    {
        availableConnections.put(connection);
    }

    /**
     * Reads connection number from property file, creates connections and adds their to "availableConnections"
     */
    private void initializeConnectionPool() throws SQLException, IOException
    {
        for (int i = 0; i < new Integer(PropertyReader.getProperty("connections.number", "config.properties")); i++)
        {
            availableConnections.add(createNewConnection());
        }
    }

    /**
     * Creates and returns new connection
     * 
     * @return connection
     */
    private Connection createNewConnection() throws SQLException, IOException
    {
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        Connection connection =
                DriverManager.getConnection(PropertyReader.getProperty("survey.DB.url", "config.properties"),
                        PropertyReader.getPropertyFile("dbConnectionConfig.properties"));
        return connection;
    }
}
