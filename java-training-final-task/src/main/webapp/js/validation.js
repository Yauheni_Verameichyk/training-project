﻿function validateForm() {
	var result = true;
	var FILL_FIELD, INCORRECT_USERNAME_LENGTH, INCORRECT_USERNAME, PWD_NOT_EQUAL, INCORRECT_PASS_LENGTH, BAD_FIRST_SYMBOL, WRONG_PASS, EMAIL_WITHOUT_DOT, EMAIL_WITHOUT_A;
	jQuery.i18n.properties({
		name : 'message',
		path : 'js/locale/',
		mode : 'both',
		language : document.getElementsByTagName('html')[0].getAttribute('lang'),
		callback : function() {
			FILL_FIELD = js.fill.field;
			INCORRECT_USERNAME_LENGTH = js.incorrect.username.length;
			INCORRECT_USERNAME = js.incorrect.username;
			PWD_NOT_EQUAL = js.pwd.not.equal;
			INCORRECT_PASS_LENGTH = js.incorrect.pass.length;
			BAD_FIRST_SYMBOL = js.bad.first.symbol;
			WRONG_PASS = js.wrong.pass;
			EMAIL_WITHOUT_DOT = js.email.without.dot;
			EMAIL_WITHOUT_A = js.email.without.a;
		}
	});
	var errUname = document.getElementById("err-uname"), errPwd1 = document
			.getElementById("err-pwd1"), errPwd2 = document
			.getElementById("err-pwd2");
	errUname.innerHTML = "";
	errPwd1.innerHTML = "";
	errPwd2.innerHTML = "";

	var usr = document.testForm.username.value, pwd1 = document.testForm.pass1.value, pwd2 = document.testForm.pass2.value;
	if (usr.search(/[a-z]/i) !== 0) {
		errUname.innerHTML = BAD_FIRST_SYMBOL;
		result = false;
	}
	if (usr.length < 5) {
		errUname.innerHTML = INCORRECT_USERNAME_LENGTH;
		result = false;
	}
	if (usr.search(/[^\w]/) !== -1) {
		errUname.innerHTML = INCORRECT_USERNAME;
		result = false;
	} 
	if (!usr) {
		errUname.innerHTML = FILL_FIELD;
		result = false;
	}

	if (!pwd1) {
		errPwd1.innerHTML = FILL_FIELD;
		result = false;
	}
	if (!pwd2) {
		errPwd2.innerHTML = FILL_FIELD;
		result = false;
	}
	if (pwd1.search(/[A-Z]/) == -1) {
		errPwd1.innerHTML = WRONG_PASS;
		result = false;
	}
	if (pwd1.search(/[a-z]/) == -1) {
		errPwd1.innerHTML = WRONG_PASS;
		result = false;
	}
	if (pwd1.search(/[1-9]/) == -1) {
		errPwd1.innerHTML = WRONG_PASS;
		result = false;
	} 
	if (pwd1.length < 6) {
		errPwd1.innerHTML = INCORRECT_PASS_LENGTH;
		result = false;
	} 
	if (pwd1 && pwd2 && pwd1 !== pwd2) {
		errPwd1.innerHTML = PWD_NOT_EQUAL;
		errPwd2.innerHTML = PWD_NOT_EQUAL;
		document.testForm.pass1.value = "";
		document.testForm.pass2.value = ""; 
		result = false;
	} 

	var emailError = document.getElementById("emailError");
	emailError.innerHTML = "";
	var emailElement = document.getElementById("email");
	var email = emailElement.value;
	if (email.search(/[@]/) == -1) {
		emailError.innerHTML = EMAIL_WITHOUT_A;
		result = false;

	}
	if (email.search(/[.]/) == -1) {
		emailError.innerHTML = EMAIL_WITHOUT_DOT;
		result = false;
	}
	if (!email) {
		emailError.innerHTML = FILL_FIELD;
		result = false;
	}

	return result;
}
