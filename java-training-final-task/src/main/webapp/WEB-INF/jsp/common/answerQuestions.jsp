﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class = "outputForm">
            <span>${survey.surveyName}</span>
            <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="post">
               <input name="command" type="hidden" value="answer_questions">
               <input name="surveyID" type="hidden" value="${survey.surveyID}">			
               <table>
                  <c:forEach items="${questionsList}" var="entry">
                     <tr>
                        <td class = "leftColumn">
                           <input name="questionID" type="hidden" value="${entry.questionID}">		
                           <c:out value="${entry.questionBody}"/>
                        </td>
                        <td class = "rightColumn">
                           <input name="answer" type="text">
                        </td>
                     </tr>
                  </c:forEach>
               </table>
               <div class = "submit">
                  <fmt:message key="answer.questions.send.answers" var="buttonValue" />
                  <input type="submit" value="${buttonValue}"> 
               </div>
            </form>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>