﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class = "outputForm">
            <span>
               <fmt:message key="passed.surveys.list.surveys.passed.by.user" />
               <b> ${ user.username }</b>: 
            </span>
            <br>
            <div>
               <table>
                  <c:choose>
                     <c:when test="${ surveysList != null }">
                        <tr>
                           <td class = "leftColumn">
                              <span>
                                 <fmt:message key="survey.name" />
                                 :
                              </span>
                           </td>
                           <td class = "rightColumn">
                              <span>
                                 <fmt:message key="questions.number" />
                                 :
                              </span>
                           </td>
                        </tr>
                     </c:when>
                     <c:otherwise>
                        <span>
                           <fmt:message key="passed.surveys.list.none.answers" />
                           .
                        </span>
                        <br>					
                     </c:otherwise>
                  </c:choose>
                  <c:forEach items="${surveysList}" var="entry">
                     <tr>
                        <td class = "leftColumn">
                           <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                              <input name="surveyName" type="hidden" value="<c:out value="${entry.key}"/>">
                              <input name="command" type="hidden" value="show_questions_in_passed_survey">
                              <input type="submit" value="<c:out value="${entry.key}"/>" class="commandLink">	
                           </form>
                        </td>
                        <td class = "rightColumn">
                           <span> ${entry.value}</span>
                        </td>
                     </tr>
                  </c:forEach>
               </table>
               <c:choose>
                  <c:when test="${ pagesNumber > 1 }">
                     <span>
                        <fmt:message key="pages" />
                        :
                     </span>
                     <c:forEach begin="1" end="${pagesNumber}" var="i">
                        <c:choose>
                           <c:when test="${currentPage eq i}">
                              <span class = "link">${i}</span>
                           </c:when>
                           <c:otherwise>
                              <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="GET">
                                 <input name="command" type="hidden" value="show_surveys_passed_by_user">
                                 <input class="commandLink" type="submit" name="page" value = "${i}"><br>							
                              </form>
                           </c:otherwise>
                        </c:choose>
                     </c:forEach>
                  </c:when>
               </c:choose>
            </div>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>