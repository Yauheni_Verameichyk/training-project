﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class = "outputForm">
            <div>
               <table>
                  <tr>
                     <td class = "leftColumn">
                        <h3>
                           <c:out value="${survey.surveyName}"/>
                        </h3>
                     </td>
                     <td class = "rightColumn">
                        <c:choose>
                           <c:when test="${ user == null }">
                              <fmt:message key="questions.list.in.survey.only.for.registered" />
                              .
                           </c:when>
                           <c:otherwise>
                              <c:choose>
                                 <c:when test="${ isPassed == false }">
                                    <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                                       <input name="command" type="hidden" value="pass_survey">
                                       <fmt:message key="questions.list.in.survey.pass.survey" var="buttonValue" />
                                       <input type="submit" value="${buttonValue}" class="commandLink">	
                                    </form>
                                 </c:when>
                                 <c:when test="${ isPassed == true }">
                                    <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                                       <input name="command" type="hidden" value="pass_survey">
                                       <fmt:message key="repass.survey" var="buttonValue" />
                                       <input type="submit" value="${buttonValue}" class="commandLink">	
                                    </form>
                                 </c:when>
                              </c:choose>
                           </c:otherwise>
                        </c:choose>
                        <c:choose>
                           <c:when test="${ user.role == 'ADMIN' }">
                              <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                                 <input name="command" type="hidden" value="show_users_passed_survey">
                                 <fmt:message key="questions.list.in.survey.users.passed.survey" var="buttonValue" />
                                 <input type="submit" value="${buttonValue}" class="commandLink">	
                              </form>
                              <br>
                              <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                                 <input name="command" type="hidden" value="show_questions_list_with_answers">
                                 <fmt:message key="questions.list.in.survey.answers.for.questions" var="buttonValue" />
                                 <input type="submit" value="${buttonValue}" class="commandLink">	
                              </form>
                              <br>
                              <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                                 <input name="command" type="hidden" value="go_to_themes_list_for_adding_questions_in_survey">
                                 <fmt:message key="questions.list.in.survey.add.questions.in.survey" var="buttonValue" />
                                 <input type="submit" value="${buttonValue}" class="commandLink">	
                              </form>
                              <br>
                              <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                                 <input name="surveyName" type="hidden" value="${surveyName}">										
                                 <input name="command" type="hidden" value="show_questions_list_for_deleting_from_survey">
                                 <fmt:message key="questions.list.in.survey.delete.questions.from.survey" var="buttonValue" />
                                 <input type="submit" value="${buttonValue}" class="commandLink">	
                              </form>
                              <br>
                              <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="post">
                                 <input name="surveyName" type="hidden" value="${surveyName}">										
                                 <input name="command" type="hidden" value="delete_survey">
                                 <fmt:message key="questions.list.in.survey.delete.survey" var="buttonValue" />
                                 <input type="submit" value="${buttonValue}" class="commandLink">	
                              </form>
                              <br>
                           </c:when>
                        </c:choose>
                     </td>
                  </tr>
                  <tr>
                     <td class = "leftColumn">
                        <span>
                           <fmt:message key="question" />
                           : 
                        </span>
                     </td>
                     <td class = "rightColumn">
                        <span>
                           <fmt:message key="theme" />
                           :
                        </span>
                     </td>
                  </tr>
                  <c:forEach items="${questionsList}" var="entry">
                     <tr>
                        <td class = "leftColumn">
                           <span>
                              <c:out value="${entry.questionBody}"/>
                           </span>
                        </td>
                        <td class = "rightColumn">
                           <span><fmt:message key="${fn:toLowerCase(entry.theme)}"/></span>
                        </td>
                     </tr>
                  </c:forEach>
               </table>
               <c:choose>
                  <c:when test="${ pagesNumber > 1 }">
                     <span>
                        <fmt:message key="pages" />
                        :
                     </span>
                     <c:forEach begin="1" end="${pagesNumber}" var="i">
                        <c:choose>
                           <c:when test="${currentPage eq i}">
                              <span class = "link">${i}</span>
                           </c:when>
                           <c:otherwise>
                              <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="GET">
                                 <input name="command" type="hidden" value="show_questions_in_survey">
                                 <input class="commandLink" type="submit" name="page" value = "${i}"><br>							
                              </form>
                           </c:otherwise>
                        </c:choose>
                     </c:forEach>
                  </c:when>
               </c:choose>
            </div>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>