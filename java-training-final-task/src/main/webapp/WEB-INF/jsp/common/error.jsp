﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css"
         href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class="exceptionText">
            <c:choose>
               <c:when test="${details != null}">
                  <h4>
                     <fmt:message key="exception.exception.headline" />
                  </h4>
                  <p  class="headline">
                     <fmt:message key="exception.exception.message" />
                     :	${message}<br>
                  </p>
                  <fmt:message key="exception.exception.details" />
                  :<br>	
                  <div class="stackTrace">${details}<br></div>
               </c:when>
               <c:otherwise>
                  <fmt:message key="error" />
               </c:otherwise>
            </c:choose>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>