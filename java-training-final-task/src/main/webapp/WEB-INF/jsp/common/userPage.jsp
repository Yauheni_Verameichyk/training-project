﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class = "outputForm">
            <h3  class="headline">
               <fmt:message key="welcome" />
               , ${ user.username }
            </h3>
            <table class = "leftColumn">
               <tr >
                  <td>
                     <span>
                        <fmt:message key="username" />
                        :
                     </span>
                  </td>
                  <td>
                     <span>
                        <c:out value="${ user.username }"/>
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>
                     <span>
                        <fmt:message key="sex" />
                        :
                     </span>
                  </td>
                  <td>
                  	 <c:choose>
                       <c:when test="${ user.sex != null }">
                          <span><fmt:message key="${fn:toLowerCase(user.sex)}"/></span>
                       </c:when>
                     </c:choose>
                  </td>
               </tr>
               <tr>
                  <td>
                     <span>
                        <fmt:message key="birth.date" />
                        :
                     </span>
                  </td>
                  <td>
                     <span> ${ user.birthDate }</span>
                  </td>
               </tr>
               <tr>
                  <td>
                     <span>e-mail:</span>
                  </td>
                  <td>
                     <span>
                        <c:out value="${ user.email }"/>
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>
                     <span>
                        <fmt:message key="user.page.role" />
                        :
                     </span>
                  </td>
                  <td>
                     <span><fmt:message key="${fn:toLowerCase(user.role)}"/></span>
                  </td>
               </tr>
            </table>
            <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
               <input name="command" type="hidden" value="show_surveys_passed_by_user">
               <fmt:message key="user.page.passed.surveys" var="buttonValue" />
               <input type="submit" value="${buttonValue}" class="commandLink">	
            </form>
            <br>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>