﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html lang="${language}">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css"
         href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <aside class="popular">
         <h4>
            <fmt:message key="facePage.popular" />
            :
         </h4>
         <c:forEach items="${surveysList}" var="entry">
            <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
               <input name="surveyName" type="hidden" value="<c:out value="${entry.surveyName}"/>">
               <input name="command" type="hidden" value="show_questions_in_survey">
               <input type="submit" value="<c:out value="${entry.surveyName}"/>" class="popularLink">	
            </form><br>
         </c:forEach>
      </aside>
      <section>
         <div class="text">
            <h2 class="headline">
               <fmt:message key="welcome" />
               !
            </h2>
            <span>
               Во!Прос - независимый центр изучения общественного мнения
               русскоязычной части пользователей Интернета. <br> Здесь Вы
               можете выразить Ваше мнение по самым актуальным вопросам, а так же
               узнать результаты проведённых ранее соцопросов, как на самые
               злободневные темы, так и на темы актуальные всегда. Результаты
               соцопросов становятся доступны сразу же после голосования.
            </span>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>