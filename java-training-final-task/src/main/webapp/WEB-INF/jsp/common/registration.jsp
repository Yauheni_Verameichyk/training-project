﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html lang = "${language}">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script  src="${pageContext.request.contextPath}/js/jquery.i18n.properties.min.js"></script>
      <script  src="${pageContext.request.contextPath}/js/validation.js"></script>
   </head>
   <body>
      <my:headTag/>
      <section>
         <h3  class="headline">
            <fmt:message key="registration.registration" />
         </h3>
         <div class = "outputForm">
            <form onsubmit="javascript:return validateForm()" action="${pageContext.request.contextPath}/SurveyServlet" method="post" name ="testForm">
               <input id="command"  name="command" type="hidden" value="registration">
               <table id="container">
                  <tr>
                     <td>
                        <span>
                           <fmt:message key="username" />
                           :
                        </span>
                     </td>
                     <td>
                        <input type="text" name="username">
                     </td>
                     <td>
                        <span class="err" id="err-uname">${ usernameError }</span>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <span>
                           <fmt:message key="password" />
                           :
                        </span>
                     </td>
                     <td>
                        <input type="password" name="pass1">
                     </td>
                     <td>
                        <span class="err" id="err-pwd1"></span>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <span>
                           <fmt:message key="registration.repeat.password" />
                           :
                        </span>
                     </td>
                     <td>
                        <input type="password" name="pass2">
                     </td>
                     <td>
                        <span class="err" id="err-pwd2"></span>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <span>
                           <fmt:message key="birth.date" />
                           : 
                        </span>
                     </td>
                     <td>
                        <fmt:message key="registration.enter.your.birth.date" var="buttonValue" />
                        <input type="date" name="birthDate" max="2010-01-01" min="1900-01-01" title="${buttonValue}">
                     </td>
                     <td>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <span>
                           <fmt:message key="sex" />
                           :
                        </span>
                     </td>
                     <td>
                        <input type="radio" name="sex" value="MALE">
                        <fmt:message key="registration.male" />
                        <br> <input type="radio" name="sex" value="FEMALE">
                        <fmt:message key="registration.female" />
                     </td>
                     <td>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <span>e-mail:</span>
                     </td>
                     <td>
                        <input type="text" name="email" id="email"><br>
                     </td>
                     <td>
                        <span class="err" id="emailError"></span>
                     </td>
                  </tr>
               </table>
               <div class = "submit">
                  <fmt:message key="registration.confirm.registration" var="buttonValue" />
                  <input type="submit" value="${buttonValue}"> 
               </div>
            </form>
         </div>
      </section>
      <my:footTag/>
      
   </body>
</html>