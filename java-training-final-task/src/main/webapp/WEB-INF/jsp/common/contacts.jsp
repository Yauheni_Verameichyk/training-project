﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css"
         href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class="text">
            <span  class="headline">
            	
              220030,<fmt:message key="contacts.republic.belarus" />,<br>
			   <fmt:message key="contacts.adress" /> 22<br>
				<fmt:message key="contacts.phone" />.: +375 44 771 50 28<br>
				e-mail: zhenyaveremeychik@yandex.by
            </span>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>