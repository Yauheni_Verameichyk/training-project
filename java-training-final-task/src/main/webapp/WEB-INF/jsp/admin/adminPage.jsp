﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css"
         href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class="text">
            <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
               <input name="command" type="hidden" value="go_to_create_survey_page">
               <fmt:message key="admin.page.create.survey" var="buttonValue" />
               <input type="submit" value="${buttonValue}" class="commandLink">	
            </form>
            <br>
            <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
               <input name="command" type="hidden" value="go_to_create_question_page">
               <fmt:message key="admin.page.create.question" var="buttonValue" />
               <input type="submit" value="${buttonValue}" class="commandLink">	
            </form>
            <br>
            <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
               <input name="command" type="hidden" value="go_to_themes_list_for_deleting_question">
               <fmt:message key="admin.page.delete.question" var="buttonValue" />
               <input type="submit" value="${buttonValue}" class="commandLink">	
            </form>
            <br>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>