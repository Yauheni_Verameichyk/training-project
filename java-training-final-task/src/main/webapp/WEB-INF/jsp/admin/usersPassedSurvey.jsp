﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css"	href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class = "outputForm">
            <c:choose>
               <c:when test="${usersList == null}">
                  <span>
                     <fmt:message key="survey" />
                     <c:out value="${survey.surveyName}"/>
                     <fmt:message key="users.passed.survey.no.users.passed" />
                     .
                  </span>
               </c:when>
               <c:otherwise>
                  <h3  class="headline">
                     <fmt:message key="users.passed.survey.users.passed" />
                     <c:out value="${survey.surveyName}"/>
                     .
                  </h3>
                  <div>
                     <c:forEach items="${usersList}" var="entry">
                        <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                           <input name="userName" type="hidden" value="${entry.username}">
                           <input name="command" type="hidden" value="show_answers_by_user">
                           <input type="submit" value="${entry.username}" class="commandLink">	
                        </form>
                        <br>
                     </c:forEach>
                  </div>
                  <c:choose>
                     <c:when test="${ pagesNumber > 1 }">
                        <span>
                           <fmt:message key="pages" />
                           :
                        </span>
                        <c:forEach begin="1" end="${pagesNumber}" var="i">
                           <c:choose>
                              <c:when test="${currentPage eq i}">
                                 <span class = "link">${i}</span>
                              </c:when>
                              <c:otherwise>
                                 <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="GET">
                                    <input name="command" type="hidden" value="show_users_passed_survey">
                                    <input class="commandLink" type="submit" name="page" value = "${i}"><br>							
                                 </form>
                              </c:otherwise>
                           </c:choose>
                        </c:forEach>
                     </c:when>
                  </c:choose>
               </c:otherwise>
            </c:choose>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>