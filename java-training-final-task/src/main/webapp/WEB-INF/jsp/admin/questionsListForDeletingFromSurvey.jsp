﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class = "outputForm">
            <h3  class="headline">
               <c:out value="${survey.surveyName}"/>
            </h3>
            <div>
               <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="post">
                  <c:forEach items="${questionsList}" var="entry">
                     <input type="checkbox" name="checkbox" value="${entry.questionID}">
                     <c:out value="${entry.questionBody}"/>
                     <br>
                  </c:forEach>
                  <input name="command" type="hidden" value="delete_questions_from_survey">
                  <input name="theme" type="hidden" value="${theme}">
                  <input name="page" type="hidden" value="${currentPage}">
                  <fmt:message key="questions.list.for.deleting.from.survey.delete.from.survey" var="buttonValue" />
                  <input type="submit" value="${buttonValue}" class="commandLink">	
               </form><br>
               <c:choose>
                  <c:when test="${ pagesNumber > 1 }">
                     <span>
                        <fmt:message key="pages" />
                        :
                     </span>
                     <c:forEach begin="1" end="${pagesNumber}" var="i">
                        <c:choose>
                           <c:when test="${currentPage eq i}">
                              <span class = "link">${i}</span>
                           </c:when>
                           <c:otherwise>
                              <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="GET">
                                 <input name="command" type="hidden" value="show_questions_list_for_deleting_from_survey">
                                 <input class="commandLink" type="submit" name="page" value = "${i}"><br>							
                              </form>
                           </c:otherwise>
                        </c:choose>
                     </c:forEach>
                  </c:when>
               </c:choose>
               <c:choose>
                  <c:when test="${ successMessage != null }">
                     <div class = "success">
                        <span >${successMessage}</span>
                     </div>
                  </c:when>
               </c:choose>
            </div>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>