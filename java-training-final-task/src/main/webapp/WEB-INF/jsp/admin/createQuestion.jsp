﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css"
         href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class="text">
            <span>
               <fmt:message key="create.question.create.question.page" />
               :
            </span><br>
            <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="post">
               <input name="command" type="hidden" value="create_question">
               <p>
                  <fmt:message key="create.question.enter.question" />
                  :
               </p>
               <input name="questionBody" type="text" size="15" required><br>
               <div class = "createQuestion">
                  <p>
                     <fmt:message key="create.question.choose.theme" />
                     :
                  </p>
                  <c:forEach items="${themesList}" var="entry">    				      					
                     <input type="radio" name="themeName" value="${entry}" required> <fmt:message key="${fn:toLowerCase(entry)}" /><br>											
                  </c:forEach>
               </div>
               <fmt:message key="create.question.create.question" var="buttonValue" />
               <input type="submit" value="${buttonValue}" class="commandLink">	
            </form>
            <br>
            <c:choose>
               <c:when test="${ successMessage != null }">
                  <div class = "success">
                     <span >${successMessage}</span>
                  </div>
               </c:when>
            </c:choose>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>