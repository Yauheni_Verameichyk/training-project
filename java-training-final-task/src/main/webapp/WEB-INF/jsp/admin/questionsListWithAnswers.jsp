﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class = "outputForm">
            <div>
               <table>
                  <tr>
                     <td class = "leftColumn">
                        <h3>
                           <fmt:message key="survey" />
                           :
                           <c:out value="${survey.surveyName}"/>
                        </h3>
                     </td>
                  </tr>
                  <tr>
                     <td class = "leftColumn">
                        <span>
                           <fmt:message key="question" />
                           : 
                        </span>
                     </td>
                     <td class = "rightColumn">
                        <span>
                           <fmt:message key="questions.list.for.admin.users.answered" />
                           :
                        </span>
                     </td>
                  </tr>
                  <c:forEach items="${questionsList}" var="entry">
                     <tr>
                        <td class = "leftColumn">
                           <span>
                              <c:out value="${entry.key.questionBody}"/>
                           </span>
                        </td>
                        <td class = "rightColumn">
                           <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                              <input name="questionID" type="hidden" value="${entry.key.questionID}">
                              <input name="questionBody" type="hidden" value="${entry.key.questionBody}">
                              <input name="command" type="hidden" value="show_answers_for_question">
                              <input type="submit" value="${entry.value}" class="commandLink">	
                           </form>
                        </td>
                     </tr>
                  </c:forEach>
               </table>
               <c:choose>
                  <c:when test="${ pagesNumber > 1 }">
                     <span>
                        <fmt:message key="pages" />
                        :
                     </span>
                     <c:forEach begin="1" end="${pagesNumber}" var="i">
                        <c:choose>
                           <c:when test="${currentPage eq i}">
                              <span class = "link">${i}</span>
                           </c:when>
                           <c:otherwise>
                              <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="GET">
                                 <input name="command" type="hidden" value="show_questions_list_with_answers">
                                 <input class="commandLink" type="submit" name="page" value = "${i}"><br>							
                              </form>
                           </c:otherwise>
                        </c:choose>
                     </c:forEach>
                  </c:when>
               </c:choose>
            </div>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>