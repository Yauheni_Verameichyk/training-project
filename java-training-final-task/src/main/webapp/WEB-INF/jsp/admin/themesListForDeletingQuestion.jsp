﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css"
         href="${pageContext.request.contextPath}/css/style.css">
      <title>
         Во!Прос - 
         <fmt:message key="surveys" />
      </title>
   </head>
   <body>
      <my:headTag/>
      <section>
         <div class = "outputForm">
            <h3  class="headline">
               <fmt:message key="choose.theme" />
               :
            </h3>
            <div>
               <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                  <input name="theme" type="hidden" value="${entry}">
                  <input name="command" type="hidden" value="show_questions_list_for_deleting_by_theme">
                  <fmt:message key="show.all.questions" var="buttonValue" />
                  <input type="submit" value="${buttonValue}" class="commandLink">	
               </form>
               <br>
               <c:forEach items="${themesList}" var="entry">
                  <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                     <input name="theme" type="hidden" value="${entry}">
                     <input name="command" type="hidden" value="show_questions_list_for_deleting_by_theme">
                     <fmt:message key="${fn:toLowerCase(entry)}" var="buttonValue" />
                     <input type="submit" value="${buttonValue}" class="commandLink">	
                  </form>
                  <br>
               </c:forEach>
            </div>
         </div>
      </section>
      <my:footTag/>
   </body>
</html>