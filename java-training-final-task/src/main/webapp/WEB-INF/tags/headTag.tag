﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "ct" uri = "/WEB-INF/tld/usersNumber"%>
<%@ tag body-content="empty" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<header>
   <div class="pagesList">
      <form class = "commandLink" action="${pageContext.request.contextPath}/SurveyServlet">
         <input id="command"  name="command" type="hidden" value="go_to_main_page">
         <fmt:message key="header.pagesList.main.page" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "commandLink" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
         <input id="command"  name="command" type="hidden" value="show_all_surveys">
         <fmt:message key="header.pagesList.surveys" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "commandLink" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
         <input id="command"  name="command" type="hidden" value="show_themes_list">
         <fmt:message key="header.pagesList.themes" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "commandLink" action="${pageContext.request.contextPath}/SurveyServlet">
         <input id="command"  name="command" type="hidden" value="go_to_order_survey_page">
         <fmt:message key="header.pagesList.order.survey" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "commandLink" action="${pageContext.request.contextPath}/SurveyServlet">
         <input id="command"  name="command" type="hidden" value="go_to_about_us_page">
         <fmt:message key="header.pagesList.about.us" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "commandLink" action="${pageContext.request.contextPath}/SurveyServlet">
         <input id="command"  name="command" type="hidden" value="go_to_contacts_page">
         <fmt:message key="header.pagesList.contacts" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
       <div class="language">
         <form class = "commandLink" action="${pageContext.request.contextPath}/SurveyServlet">
            <input id="command"  name="command" type="hidden" value="go_to_main_page">
            <input id="language" name="language" type="submit" value="en" class="commandLink">	
         </form>
         <span>/</span>
         <form class = "commandLink" action="${pageContext.request.contextPath}/SurveyServlet">
            <input id="command"  name="command" type="hidden" value="go_to_main_page">
            <input id="language" name="language" type="submit" value="ru" class="commandLink">	
         </form>
      </div>
   </div>
   <div class="logo">
      <img src="${pageContext.request.contextPath}/img/logo.PNG" width=340
      <fmt:message key="header.logo" var="buttonValue" />
      height=100 alt="${buttonValue}">
   </div>
   <div class="register">
      <div>
         <c:choose>
            <c:when test="${ user != null }">
               <span class="registrationForm">
                  <fmt:message key="header.register.username" />
                  : <b> ${ user.username }</b> 
               </span>
               <br>						
               <form class = "registrationForm" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                  <input id="command"  name="command" type="hidden" value="go_to_user_page">
                  <fmt:message key="header.register.user.page" var="buttonValue" />
                  <input type="submit" value="${buttonValue}" class="registrationLink">	
               </form>
               <span>/</span>
               <form class = "registrationForm" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                  <input id="command"  name="command" type="hidden" value="logout">
                  <fmt:message key="header.register.lodout" var="buttonValue" />
                  <input type="submit" value="${buttonValue}" class="registrationLink">	
               </form>
               <c:choose>
                  <c:when test="${ user.role == 'ADMIN' }">
                     <form class = "registrationForm" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                        <input id="command"  name="command" type="hidden" value="go_to_admin_page">
                        <fmt:message key="administrators.options" var="buttonValue" />
                        <input type="submit" value="${buttonValue}" class="registrationLink">	
                     </form>
                     <br>
                  </c:when>
               </c:choose>
            </c:when>
            <c:otherwise>
               <form class="registrationForm" action="${pageContext.request.contextPath}/SurveyServlet" method="post">
                  <input id="command"  name="command" type="hidden" value="login">
                  <fmt:message key="username" var="usernamePlaceholder" />
                  <input id="username" name="username" type="text" size="15" name="text"	placeholder="${usernamePlaceholder}"><br>
                  <fmt:message key="password" var="passwordPlaceholder" />
                  <input id="password" name="password" type="password" size="15" name="text" placeholder="${passwordPlaceholder}"><br>	
                  <fmt:message key="header.register.lodin" var="buttonValue" />
                  <input type="submit" value="${buttonValue}" class="registrationLink">			
               </form>
               <br>
               <form class = "registrationForm" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
                  <input id="command"  name="command" type="hidden" value="go_to_registration_page">
                  <fmt:message key="header.register.register" var="buttonValue" />
                  <input type="submit" value="${buttonValue}" class="registrationLink">	
               </form>
            </c:otherwise>
         </c:choose>
      </div>
   </div>
   <div class = "usersNumber"><span ><fmt:message key="header.already.registered"/> <ct:usersNumber/> <fmt:message key="header.users"/></span></div>
   <div class="topicList">
      <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
         <input id="themeName"  name="themeName" type="hidden" value="AUTO">
         <input id="command"  name="command" type="hidden" value="show_surveys_list_by_theme">
         <fmt:message key="topic.list.auto" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
         <input id="themeName"  name="themeName" type="hidden" value="SOCIETY">
         <input id="command"  name="command" type="hidden" value="show_surveys_list_by_theme">
         <fmt:message key="topic.list.society" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
         <input id="themeName"  name="themeName" type="hidden" value="CAREER">
         <input id="command"  name="command" type="hidden" value="show_surveys_list_by_theme">
         <fmt:message key="topic.list.career" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
         <input id="themeName"  name="themeName" type="hidden" value="POLITICS">
         <input id="command"  name="command" type="hidden" value="show_surveys_list_by_theme">
         <fmt:message key="topic.list.politics" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
         <input id="themeName"  name="themeName" type="hidden" value="RELIGION">
         <input id="command"  name="command" type="hidden" value="show_surveys_list_by_theme">
         <fmt:message key="topic.list.religion" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
      <form class = "link" action="${pageContext.request.contextPath}/SurveyServlet" method="get">
         <input id="themeName"  name="themeName" type="hidden" value="SPORT">
         <input id="command"  name="command" type="hidden" value="show_surveys_list_by_theme">
         <fmt:message key="topic.list.sport" var="buttonValue" />
         <input type="submit" value="${buttonValue}" class="commandLink">	
      </form>
   </div>
</header>