﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ tag body-content="empty" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/locale/message" />
<!DOCTYPE html>
<footer>
   <span>
      <fmt:message key="footer.text" />
      "Java Web Development".
   </span>
   <br>
   <span>
      © 
      <fmt:message key="footer.copyright" />
      2017.
   </span>
</footer>