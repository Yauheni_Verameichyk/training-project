package dao;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import connection.JDBCConnectionPool;
import entity.Survey;
import exception.DAOException;

public class SurveyDAOTest
{
    @BeforeClass
    public static void init() throws SQLException, ClassNotFoundException, IOException, InterruptedException
    {
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        initDatabase();
    }

    @AfterClass
    public static void destroy() throws SQLException, ClassNotFoundException, IOException
    {
        try (Connection connection = getConnection(); Statement statement = connection.createStatement();)
        {
            statement.executeUpdate("DROP TABLE surveys");
            connection.commit();
            statement.executeUpdate("DROP TABLE surveys_m2m_questions");
            connection.commit();
            statement.executeUpdate("DROP TABLE answers");
            connection.commit();
            statement.executeUpdate("DROP TABLE users");
            connection.commit();
        }
    }

    @Test
    public void shouldGetSurveyByName() throws DAOException, SQLException, InterruptedException
    {
        SurveyDAO surveyDAO = getSurveyDAO();
        Survey survey = surveyDAO.getByName("Test survey");
        assertThat(survey, equalTo(createSurvey((long) 26, "Test survey")));
    }

    @Test
    public void shouldDeleteSurveyAndCatchExceptionDuringAttemptToGetDeletedSurvey()
        throws DAOException, SQLException, InterruptedException
    {
        SurveyDAO surveyDAO = getSurveyDAO();
        Survey survey = surveyDAO.getByName("SurveyForDeleting");
        surveyDAO.delete(survey.getSurveyID());
        try
        {
            surveyDAO.getByName("SurveyForDeleting");
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during reading survey from the database"));
        }
    }

    @Test
    public void shouldFailDuringDeleting()
        throws DAOException, SQLException, InterruptedException
    {
        SurveyDAO surveyDAO = getSurveyDAOThatThrowsException();
        try
        {
            surveyDAO.delete((long) 27);
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during element removal"));
        }
    }

    @Test
    public void shouldReturnTrueIfSurveyIsPassedByUser() throws DAOException, SQLException, InterruptedException
    {
        SurveyDAO surveyDAO = getSurveyDAO();
        assertThat(surveyDAO.isPassed("Jhon", "Another test survey"), equalTo(true));
    }

    @Test
    public void shouldFailCheckingIfSurveyIsPassed()
        throws DAOException, SQLException, InterruptedException
    {
        SurveyDAO surveyDAO = getSurveyDAOThatThrowsException();
        try
        {
            surveyDAO.isPassed("Jhon", "Another test survey");
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during reading from the database"));
        }
    }

    @Test
    public void shouldInsertSurvey() throws DAOException, SQLException, InterruptedException
    {
        SurveyDAO surveyDAO = getSurveyDAO();
        Survey survey = createSurvey((long) 28, "Survey for inserting");
        surveyDAO.create(survey);
        assertThat(survey, equalTo(surveyDAO.getByName("Survey for inserting")));
    }

    @Test
    public void shouldFailDuringInserting()
        throws DAOException, SQLException, InterruptedException
    {
        SurveyDAO surveyDAO = getSurveyDAOThatThrowsException();
        try
        {
            surveyDAO.create(new Survey());
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during element insertion"));
        }
    }

    @Test
    public void shouldReturnOnePopularSurvey() throws DAOException, SQLException, InterruptedException
    {
        SurveyDAO surveyDAO = getSurveyDAO();
        List<Survey> popularSurveys = surveyDAO.getPopularSurveys(1);
        List<Survey> expectedPopularSurveys = new LinkedList<>();
        expectedPopularSurveys.add(createSurvey((long) 2, "Another test survey"));
        assertThat(popularSurveys, equalTo(expectedPopularSurveys));
    }

    @Test
    public void shouldFailWhileTryingToGetPopularSurvey()
        throws DAOException, SQLException, InterruptedException
    {
        SurveyDAO surveyDAO = getSurveyDAOThatThrowsException();
        try
        {
            surveyDAO.getPopularSurveys(1);
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during reading surveys from the database"));
        }
    }

    private static SurveyDAO getSurveyDAO() throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenReturn(getConnection());
        SurveyDAO surveyDAO = new SurveyDAO();
        Whitebox.setInternalState(surveyDAO, JDBCConnectionPool.class, connectionPool);
        return surveyDAO;
    }

    private static SurveyDAO getSurveyDAOThatThrowsException()
        throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenThrow(SQLException.class);
        SurveyDAO surveyDAO = new SurveyDAO();
        Whitebox.setInternalState(surveyDAO, JDBCConnectionPool.class, connectionPool);
        return surveyDAO;
    }

    private static Survey createSurvey(Long surveyID, String surveyName)
    {
        Survey survey = new Survey();
        survey.setSurveyID(surveyID);
        survey.setSurveyName(surveyName);
        return survey;
    }

    private static void initDatabase() throws SQLException
    {
        try (Connection connection = getConnection(); Statement statement = connection.createStatement();)
        {
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS surveys (SurveyID int NOT NULL IDENTITY , SurveyName varchar(30))");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS surveys_m2m_questions (SurveyID int NOT NULL, QuestionID int)");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS answers (AnswerID int NOT NULL IDENTITY, AnswerBody varchar(200) "
                            + "DEFAULT NULL, UserID int NOT NULL, SurveyID int NOT NULL, QuestionID int NOT NULL, "
                            + "AnswerDate date DEFAULT NULL, PRIMARY KEY (AnswerID))");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS users (UserID int NOT NULL IDENTITY, Username varchar(50), "
                            + "Password varchar(32), Sex varchar(6), BirthDate date, Email varchar(45), RoleID  int)");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO surveys VALUES (26,'Test survey'),(2,'Another test survey'),(27,'SurveyForDeleting');");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO answers VALUES (10,'Yes',1,2,1,'2008-06-23'),(12,'Test',1,1,2,'2008-06-23'),"
                            + "(13,'No',2,2,3,'2008-06-23'),(14,'No',2,2,2,'2008-06-23');");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO users VALUES (1,'Jhon','25d55ad283aa400af464c76d713c07ad','M','1993-09-09','qwe@yandex.by',1),"
                            + "(2,'Ivan','d8578edf8458ce06fbc5bb76a58c5ca4','M','1985-09-09','rty@gmail.com',2);");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO surveys_m2m_questions VALUES (2,10);");
            connection.commit();
        }
    }

    private static Connection getConnection() throws SQLException
    {
        return DriverManager.getConnection("jdbc:hsqldb:mem:surveysdb", "root", "pass");
    }
}
