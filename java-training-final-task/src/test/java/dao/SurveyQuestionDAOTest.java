package dao;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import connection.JDBCConnectionPool;
import exception.DAOException;

public class SurveyQuestionDAOTest
{
    @BeforeClass
    public static void init() throws SQLException, ClassNotFoundException, IOException, InterruptedException
    {
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        initDatabase();
    }

    @AfterClass
    public static void destroy() throws SQLException, ClassNotFoundException, IOException
    {
        try (Connection connection = getConnection(); Statement statement = connection.createStatement();)
        {
            statement.executeUpdate("DROP TABLE surveys");
            connection.commit();
            statement.executeUpdate("DROP TABLE surveys_m2m_questions");
            connection.commit();
            statement.executeUpdate("DROP TABLE questions");
            connection.commit();
        }
    }

    @Test
    public void shouldAddQuestionToSurvey() throws DAOException, SQLException, InterruptedException
    {
        SurveyQuestionDAO surveyQuestionDAO = getSurveyQuestionDAO();
        surveyQuestionDAO.addQuestionToSurvey((long) 3, (long) 5);
        assertThat(consistsValues((long) 3, (long) 5), equalTo(true));
    }

    @Test
    public void shouldFailDuringInserting()
        throws DAOException, SQLException, InterruptedException
    {
        SurveyQuestionDAO surveyQuestionDAO = getSurveyQuestionDAOThatThrowsException();
        try
        {
            surveyQuestionDAO.addQuestionToSurvey((long) 3, (long) 5);
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during insertion"));
        }
    }

    @Test
    public void shouldDeleteQuestionFromSurvey() throws DAOException, SQLException, InterruptedException
    {
        SurveyQuestionDAO surveyQuestionDAO = getSurveyQuestionDAO();
        surveyQuestionDAO.deleteQuestionFromSurvey((long) 3, (long) 8);
        assertThat(consistsValues((long) 3, (long) 8), equalTo(false));
    }

    @Test
    public void shouldFailDuringDeleting()
        throws DAOException, SQLException, InterruptedException
    {
        SurveyQuestionDAO surveyQuestionDAO = getSurveyQuestionDAOThatThrowsException();
        try
        {
            surveyQuestionDAO.deleteQuestionFromSurvey((long) 3, (long) 8);
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during removal"));
        }
    }

    @Test
    public void shouldReturnTrueIfSurveyIsEmpty() throws DAOException, SQLException, InterruptedException
    {
        SurveyQuestionDAO surveyQuestionDAO = getSurveyQuestionDAO();
        assertThat(surveyQuestionDAO.isEmpty("Another test survey"), equalTo(true));
    }

    @Test
    public void shouldReturnFalseIfSurveyIsNotEmpty() throws DAOException, SQLException, InterruptedException
    {
        SurveyQuestionDAO surveyQuestionDAO = getSurveyQuestionDAO();
        assertThat(surveyQuestionDAO.isEmpty("Test survey"), equalTo(false));
    }

    @Test
    public void shouldFailDuringChekingIfSurveyIsEmpty()
        throws DAOException, SQLException, InterruptedException
    {
        SurveyQuestionDAO surveyQuestionDAO = getSurveyQuestionDAOThatThrowsException();
        try
        {
            surveyQuestionDAO.isEmpty("Test survey");
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during reading from the database"));
        }
    }

    private static boolean consistsValues(Long surveyID, Long questionID) throws SQLException
    {
        Connection connection = DAOTestUtil.getConnection();
        PreparedStatement ps = connection.prepareStatement(
                "SELECT * FROM surveys_m2m_questions where SurveyID = ? and QuestionID = ?;");
        ps.setLong(1, surveyID);
        ps.setLong(2, questionID);
        ResultSet rs = ps.executeQuery();
        return rs.isBeforeFirst();
    }

    private static SurveyQuestionDAO getSurveyQuestionDAO() throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenReturn(getConnection());
        SurveyQuestionDAO surveyQuestionDAO = new SurveyQuestionDAO();
        Whitebox.setInternalState(surveyQuestionDAO, JDBCConnectionPool.class, connectionPool);
        return surveyQuestionDAO;
    }

    private static SurveyQuestionDAO getSurveyQuestionDAOThatThrowsException()
        throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenThrow(SQLException.class);
        SurveyQuestionDAO surveyQuestionDAO = new SurveyQuestionDAO();
        Whitebox.setInternalState(surveyQuestionDAO, JDBCConnectionPool.class, connectionPool);
        return surveyQuestionDAO;
    }

    private static void initDatabase() throws SQLException
    {
        try (Connection connection = getConnection(); Statement statement = connection.createStatement();)
        {
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS surveys (SurveyID int NOT NULL IDENTITY , SurveyName varchar(30))");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS answers (AnswerID int NOT NULL IDENTITY, "
                            + "AnswerBody varchar(200) DEFAULT NULL, UserID int NOT NULL, SurveyID int NOT NULL, "
                            + "QuestionID int NOT NULL, AnswerDate date DEFAULT NULL, PRIMARY KEY (AnswerID))");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS questions (QuestionID int NOT NULL IDENTITY , "
                            + "QuestionBody varchar(30), ThemeID int NOT NULL)");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS surveys_m2m_questions (SurveyID int NOT NULL, QuestionID int)");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO surveys VALUES (3,'Test survey'),(5,'Another test survey');");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO questions  VALUES (5,'To be?', 1),(8,'QuestionForDeleting', 2);");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO surveys_m2m_questions VALUES (3,10), (3,8);");
            connection.commit();
        }
    }

    private static Connection getConnection() throws SQLException
    {
        return DriverManager.getConnection("jdbc:hsqldb:mem:surveysdb", "root", "pass");
    }
}
