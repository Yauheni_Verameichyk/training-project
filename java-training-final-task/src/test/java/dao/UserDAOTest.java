package dao;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import connection.JDBCConnectionPool;
import entity.Role;
import entity.Sex;
import entity.User;
import exception.CommandException;
import exception.DAOException;
import util.CommandUtil;

public class UserDAOTest
{
    @BeforeClass
    public static void init() throws SQLException, ClassNotFoundException, IOException, InterruptedException
    {
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        initDatabase();
    }

    @AfterClass
    public static void destroy() throws SQLException, ClassNotFoundException, IOException
    {
        try (Connection connection = DAOTestUtil.getConnection(); Statement statement = connection.createStatement();)
        {
            statement.executeUpdate("DROP TABLE users");
            connection.commit();
            statement.executeUpdate("DROP TABLE roles");
            connection.commit();
        }
    }

    @Test
    public void shouldGetUserByUsernameAndPassword()
        throws DAOException, SQLException, InterruptedException, CommandException, ParseException
    {
        UserDAO userDAO = getUserDAO();
        User actual = userDAO.getByUsernameAndPass("pinaple1993", CommandUtil.hash("12345678"));
        assertThat(actual, equalTo(createUser((long) 3, "pinaple1993", "25d55ad283aa400af464c76d713c07ad", Sex.MALE,
                DAOTestUtil.getDate("1993-09-09"), "qwe@yandex.by", Role.ADMIN)));
    }

    @Test
    public void shouldGetUserByUsernameAndPasswordThatNotExist()
        throws DAOException, SQLException, InterruptedException, CommandException, ParseException
    {
        UserDAO userDAO = getUserDAO();
        User actual = userDAO.getByUsernameAndPass("pinaple19932", CommandUtil.hash("12345678"));
        assertThat(actual, equalTo(null));
    }

    @Test
    public void shouldFailWhileTryingToGetUserByUsernameAndPassword()
        throws DAOException, SQLException, InterruptedException, ParseException, CommandException
    {
        UserDAO userDAO = getUserDAOThatThrowsException();
        try
        {
            userDAO.getByUsernameAndPass("pinaple1993", CommandUtil.hash("12345678"));
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during reading user from the database"));
        }
    }

    @Test
    public void shouldGetUserByUsername()
        throws DAOException, SQLException, InterruptedException, CommandException, ParseException
    {
        UserDAO userDAO = getUserDAO();
        User actual = userDAO.getByUsername("pinaple1993");
        assertThat(actual, equalTo(createUser((long) 3, "pinaple1993", "25d55ad283aa400af464c76d713c07ad", Sex.MALE,
                DAOTestUtil.getDate("1993-09-09"), "qwe@yandex.by", Role.ADMIN)));
    }

    @Test
    public void shouldGetUserByUsernameThatNotExist()
        throws DAOException, SQLException, InterruptedException, CommandException, ParseException
    {
        UserDAO userDAO = getUserDAO();
        User actual = userDAO.getByUsername("pinaple1992");
        assertThat(actual, equalTo(null));
    }

    @Test
    public void shouldFailWhileTryingToGetUserByUsername()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        UserDAO userDAO = getUserDAOThatThrowsException();
        try
        {
            userDAO.getByUsername("pinaple1993");
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during reading user from the database"));
        }
    }

    @Test
    public void shouldCreateUser()
        throws DAOException, SQLException, InterruptedException, CommandException, ParseException
    {
        UserDAO userDAO = getUserDAO();
        User user = createUser((long) 5, "User for inserting", "25d55ad283aa400af464c76d713c07ad", Sex.MALE,
                DAOTestUtil.getDate("1993-09-09"), "qwe@yandex.by", Role.ADMIN);
        userDAO.create(user);
        assertThat(userDAO.getByUsername("User for inserting"), equalTo(user));
    }

    @Test
    public void shouldCreateUserWithSexAndBirthDateEqualToNull()
        throws DAOException, SQLException, InterruptedException, CommandException, ParseException
    {
        UserDAO userDAO = getUserDAO();
        User user = createUser((long) 6, "User for inserting with nulls", "25d55ad283aa400af464c76d713c07ad", null,
                null, "qwe@yandex.by", Role.ADMIN);
        userDAO.create(user);
        assertThat(userDAO.getByUsername("User for inserting with nulls"), equalTo(user));
    }

    @Test
    public void shouldFailDuringInserting()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        UserDAO userDAO = getUserDAOThatThrowsException();
        try
        {
            userDAO.create(new User());
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during element insertion"));
        }
    }

    @Test
    public void shouldReturnUsersNumber()
        throws DAOException, SQLException, InterruptedException
    {
        UserDAO userDAO = getUserDAO();
        assertThat(userDAO.getUsersNumber(), equalTo(4));
    }

    @Test
    public void shouldFailDuringTryingToGetUsersNumber()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        UserDAO userDAO = getUserDAOThatThrowsException();
        try
        {
            userDAO.getUsersNumber();
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during reading from the database"));
        }
    }

    private static UserDAO getUserDAO() throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenReturn(DAOTestUtil.getConnection());
        UserDAO userDAO = new UserDAO();
        Whitebox.setInternalState(userDAO, JDBCConnectionPool.class, connectionPool);
        return userDAO;
    }

    private static UserDAO getUserDAOThatThrowsException()
        throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenThrow(SQLException.class);
        UserDAO userDAO = new UserDAO();
        Whitebox.setInternalState(userDAO, JDBCConnectionPool.class, connectionPool);
        return userDAO;
    }

    private static User createUser(Long userID, String userName, String password, Sex sex, Date birthDate, String email,
        Role role)
    {
        User user = new User();
        user.setUserID(userID);
        user.setUsername(userName);
        user.setPassword(password);
        user.setSex(sex);
        user.setEmail(email);
        user.setBirthDate(birthDate);
        user.setRole(role);
        return user;
    }

    private static void initDatabase() throws SQLException
    {
        try (Connection connection = DAOTestUtil.getConnection(); Statement statement = connection.createStatement();)
        {
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS roles (RoleID int NOT NULL, RoleName varchar(15))");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS users (UserID int NOT NULL IDENTITY, Username varchar(50), "
                            + "Password varchar(32), Sex varchar(6), BirthDate date, Email varchar(45), RoleID  int)");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO users VALUES (3,'pinaple1993','25d55ad283aa400af464c76d713c07ad','M','1993-09-09','qwe@yandex.by',1),"
                            + "(4,'qwe','d8578edf8458ce06fbc5bb76a58c5ca4','M','1985-09-09','rty@gmail.com',2);");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO roles VALUES (1,'ADMIN'),(2,'USER');");
            connection.commit();
        }
    }
}
