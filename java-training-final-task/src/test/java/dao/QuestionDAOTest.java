package dao;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import connection.JDBCConnectionPool;
import entity.Question;
import entity.Theme;
import exception.DAOException;

public class QuestionDAOTest
{
    @BeforeClass
    public static void init() throws SQLException, ClassNotFoundException, IOException, InterruptedException
    {
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        initDatabase();
    }

    @AfterClass
    public static void destroy() throws SQLException, ClassNotFoundException, IOException
    {
        try (Connection connection = DAOTestUtil.getConnection(); Statement statement = connection.createStatement();)
        {
            statement.executeUpdate("DROP TABLE themes");
            connection.commit();
            statement.executeUpdate("DROP TABLE surveys_m2m_questions");
            connection.commit();
            statement.executeUpdate("DROP TABLE answers");
            connection.commit();
            statement.executeUpdate("DROP TABLE questions");
            connection.commit();
            statement.executeUpdate("DROP TABLE surveys");
            connection.commit();
        }
    }

    @Test
    public void shouldDeleteQuestion()
        throws DAOException, SQLException, InterruptedException
    {
        QuestionDAO questionDAO = getQuestionDAO();
        Question question = getQuestionFromDataBase((long) 4);
        questionDAO.deleteQuestion(question.getQuestionID());
        assertThat(getQuestionFromDataBase((long) 4), equalTo(null));
    }

    @Test
    public void shouldFailDuringDeleting()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        QuestionDAO questionDAO = getQuestionDAOThatThrowsException();
        try
        {
            questionDAO.deleteQuestion((long) 2);
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during element removal"));
        }
    }

    @Test
    public void shouldInsertQuestion() throws DAOException, SQLException, InterruptedException
    {
        QuestionDAO questionDAO = getQuestionDAO();
        Question question = createQuestion((long) 5, "Question for inserting", Theme.AUTO);
        questionDAO.create(question);
        assertThat(question, equalTo(getQuestionFromDataBase((long) 5)));
    }

    @Test
    public void shouldFailDuringInserting()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        QuestionDAO questionDAO = getQuestionDAOThatThrowsException();
        try
        {
            questionDAO.create(new Question());
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during element insertion"));
        }
    }

    @Test
    public void shouldReturnQuestionsList() throws DAOException, SQLException, InterruptedException
    {
        QuestionDAO questionDAO = getQuestionDAO();
        List<Question> questionsList = questionDAO.getQuestionsFromSurvey("Test survey");
        assertThat(questionsList, equalTo(getQuestionsList()));
    }

    @Test
    public void shouldFailDuringTryingToGetQuestionsList()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        QuestionDAO questionDAO = getQuestionDAOThatThrowsException();
        try
        {
            questionDAO.getQuestionsFromSurvey("Test survey");
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during reading questions from the database"));
        }
    }

    private static List<Question> getQuestionsList()
    {
        List<Question> questionsList = new LinkedList<Question>();
        questionsList.add(createQuestion((long) 1, "To be?", Theme.POLITICS));
        questionsList.add(createQuestion((long) 2, "Not to be?", Theme.POLITICS));
        questionsList.add(createQuestion((long) 3, "Test question", Theme.ECONOMICS));
        return questionsList;
    }

    private static QuestionDAO getQuestionDAO() throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenReturn(DAOTestUtil.getConnection());
        QuestionDAO questionDAO = new QuestionDAO();
        Whitebox.setInternalState(questionDAO, JDBCConnectionPool.class, connectionPool);
        return questionDAO;
    }

    private static QuestionDAO getQuestionDAOThatThrowsException()
        throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenThrow(SQLException.class);
        QuestionDAO questionDAO = new QuestionDAO();
        Whitebox.setInternalState(questionDAO, JDBCConnectionPool.class, connectionPool);
        return questionDAO;
    }

    private static Question createQuestion(Long questionID, String questionBody, Theme theme)
    {
        Question question = new Question();
        question.setQuestionID(questionID);
        question.setQuestionBody(questionBody);
        question.setTheme(theme);
        return question;
    }

    private static Question getQuestionFromDataBase(Long questionID) throws DAOException, SQLException
    {
        Connection connection = DAOTestUtil.getConnection();
        PreparedStatement ps = connection.prepareStatement(
                "Select QuestionID, QuestionBody, ThemeName from questions join themes on questions.ThemeID = themes.ThemeID where QuestionID = ?");
        ps.setLong(1, questionID);
        ResultSet rs = ps.executeQuery();
        if (!rs.isBeforeFirst())
        {
            return null;
        }
        rs.next();
        return QuestionDAO.createQuestion(rs);
    }

    private static void initDatabase() throws SQLException
    {
        try (Connection connection = DAOTestUtil.getConnection(); Statement statement = connection.createStatement();)
        {
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS themes (ThemeID int NOT NULL IDENTITY , ThemeName varchar(15))");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS surveys_m2m_questions (SurveyID int NOT NULL, QuestionID int)");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS answers (AnswerID int NOT NULL IDENTITY, AnswerBody varchar(200) DEFAULT NULL, UserID int NOT NULL, "
                            + "SurveyID int NOT NULL, QuestionID int NOT NULL, AnswerDate date DEFAULT NULL, PRIMARY KEY (AnswerID))");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS questions (QuestionID int NOT NULL IDENTITY , QuestionBody varchar(30), ThemeID int NOT NULL)");
            connection.commit();
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS surveys (SurveyID int NOT NULL IDENTITY , SurveyName varchar(30))");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO surveys VALUES (1,'Test survey'),(2,'Another test survey'),(3,'SurveyForDeleting');");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO themes  VALUES (9,'AUTO'),(7,'CAREER'),(2,'ECONOMICS'),(6,'EDUCATION'),(1,'POLITICS'),(5,'RELIGION'),"
                            + "(4,'SCIENCE'),(3,'SOCIETY'),(8,'SPORT');");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO answers VALUES (1,'Yes',1,1,1,'2008-06-23'),(2,'Test',1,1,2,'2008-06-23'),"
                            + "(3,'No',2,2,3,'2008-06-23'),(4,'No',2,2,2,'2008-06-23');");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO questions  VALUES (1,'To be?', 1),(2,'Not to be?', 1),(3,'Test question', 2),(4,'QuestionForDeleting', 2);");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO surveys_m2m_questions VALUES (1,1),(1,2),(1,3),(2,2),(2,3),(2,4);");
            connection.commit();
        }
    }
}
