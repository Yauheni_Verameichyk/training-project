package dao;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import connection.JDBCConnectionPool;
import entity.Answer;
import exception.DAOException;

public class AnswerDAOTest
{
    @BeforeClass
    public static void init() throws SQLException, ClassNotFoundException, IOException, InterruptedException
    {
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        initDatabase();
    }

    @AfterClass
    public static void destroy() throws SQLException, ClassNotFoundException, IOException
    {
        try (Connection connection = DAOTestUtil.getConnection(); Statement statement = connection.createStatement();)
        {
            statement.executeUpdate("DROP TABLE answers");
            connection.commit();
        }
    }

    @Test
    public void shouldGetAnswerByUserIDQuestionIDSurveyID()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        AnswerDAO answerDAO = getAnswerDAO();
        Answer answer = answerDAO.getByUserIDSurveyIDQuestionID("2", "3", "11");
        assertThat(answer,
                equalTo(createAnswer((long) 7, "Yes", (long) 2, (long) 3, (long) 11,
                        DAOTestUtil.getDate("2008-06-23"))));
    }

    @Test
    public void shouldFailWhileTryingGetAnswerByUserIDQuestionIDSurveyID()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        AnswerDAO answerDAO = getAnswerDAOThatThrowsException();
        try
        {
            answerDAO.getByUserIDSurveyIDQuestionID("2", "3", "11");
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during reading answer from the database"));
        }
    }

    @Test
    public void shouldUpdateAnswer()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        AnswerDAO answerDAO = getAnswerDAO();
        Answer answer = answerDAO.getByUserIDSurveyIDQuestionID("2", "3", "12");
        answer.setAnswerBody("Changed question");
        answerDAO.update(answer);
        assertThat(answerDAO.getByUserIDSurveyIDQuestionID("2", "3", "12"),
                equalTo(createAnswer((long) 5, "Changed question", (long) 2, (long) 3, (long) 12,
                        DAOTestUtil.getDate("2008-06-23"))));
    }

    @Test
    public void shouldFailDuringUpdating()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        AnswerDAO answerDAO = getAnswerDAOThatThrowsException();
        try
        {
            answerDAO.update(new Answer());
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during element update"));
        }
    }

    @Test
    public void shouldDeleteAnswer()
        throws SQLException, InterruptedException, DAOException
    {
        AnswerDAO answerDAO = getAnswerDAO();
        Answer answer = answerDAO.getByUserIDSurveyIDQuestionID("2", "3", "13");
        answerDAO.delete(answer.getQuestionID());
        answer = answerDAO.getByUserIDSurveyIDQuestionID("2", "3", "13");
        assertThat(answer, equalTo(null));
    }

    @Test
    public void shouldFailDuringDeleting()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        AnswerDAO answerDAO = getAnswerDAOThatThrowsException();
        try
        {
            answerDAO.delete((long) 2);
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during element removal"));
        }
    }

    @Test
    public void shouldInsertAnswer() throws DAOException, SQLException, InterruptedException, ParseException
    {
        AnswerDAO answerDAO = getAnswerDAO();
        Answer answer =
                createAnswer((long) 8, "Answer for inserting", (long) 3, (long) 4, (long) 15,
                        DAOTestUtil.getDate("2008-06-23"));
        answerDAO.create(answer);
        assertThat(answer, equalTo(answerDAO.getByUserIDSurveyIDQuestionID("3", "4", "15")));
    }

    @Test
    public void shouldFailDuringInserting()
        throws DAOException, SQLException, InterruptedException, ParseException
    {
        AnswerDAO answerDAO = getAnswerDAOThatThrowsException();
        try
        {
            answerDAO.create(new Answer());
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception during element insertion"));
        }
    }

    private static AnswerDAO getAnswerDAO() throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenReturn(DAOTestUtil.getConnection());
        AnswerDAO answerDAO = new AnswerDAO();
        Whitebox.setInternalState(answerDAO, JDBCConnectionPool.class, connectionPool);
        return answerDAO;
    }

    private static AnswerDAO getAnswerDAOThatThrowsException() throws InterruptedException, SQLException, DAOException
    {
        JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
        Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
        when(connectionPool.getConnection()).thenThrow(SQLException.class);
        AnswerDAO answerDAO = new AnswerDAO();
        Whitebox.setInternalState(answerDAO, JDBCConnectionPool.class, connectionPool);
        return answerDAO;
    }

    private static Answer createAnswer(Long answerID, String answerBody, Long userID, Long surveyID, Long questionID,
        Date answerDate)
    {
        Answer answer = new Answer();
        answer.setAnswerID(answerID);
        answer.setAnswerBody(answerBody);
        answer.setUserID(userID);
        answer.setSurveyID(surveyID);
        answer.setQuestionID(questionID);
        answer.setAnswerDate(answerDate);
        return answer;
    }

    private static void initDatabase() throws SQLException
    {
        try (Connection connection = DAOTestUtil.getConnection(); Statement statement = connection.createStatement();)
        {
            statement.execute(
                    "CREATE TABLE IF NOT EXISTS answers (AnswerID int NOT NULL IDENTITY, AnswerBody varchar(200) DEFAULT NULL,"
                            + " UserID int NOT NULL, SurveyID int NOT NULL, QuestionID int NOT NULL, AnswerDate date DEFAULT NULL, PRIMARY KEY (AnswerID))");
            connection.commit();
            statement.executeUpdate(
                    "INSERT INTO answers VALUES (7,'Yes',2,3,11,'2008-06-23'),(5,'Test',2,3,12,'2008-06-23'),(6,'No',2,3,13,'2008-06-23');");
            connection.commit();
        }
    }
}
