package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DAOTestUtil
{
    public static Connection getConnection() throws SQLException
    {
        return DriverManager.getConnection("jdbc:hsqldb:mem:surveysdb", "root", "pass");
    }

    public static Date getDate(String dateString) throws ParseException
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse(dateString);
        return date;
    }
}
