package dao;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import connection.JDBCConnectionPool;
import exception.DAOException;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JDBCConnectionPool.class)
public class AbstractDAOTest
{
    @Test
    public void shouldFailWhileTryingGetConnectionPool()
        throws DAOException, SQLException, InterruptedException, ParseException, NumberFormatException, IOException
    {
        try
        {
            PowerMockito.mockStatic(JDBCConnectionPool.class);
            when(JDBCConnectionPool.getConnectionPool()).thenThrow(SQLException.class);
            new QuestionDAO();
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception in connection establishment"));
        }
    }

    @Test
    public void shouldFailWhileTryingToCloseStatement()
        throws DAOException, SQLException, InterruptedException, ParseException, NumberFormatException, IOException
    {
        try
        {
            JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
            PreparedStatement preparedStatement = mock(PreparedStatement.class);
            Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
            doThrow(SQLException.class).when(preparedStatement).close();
            AbstractDAO abstractDAO = new QuestionDAO();
            abstractDAO.closeStatement(preparedStatement);
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception in statement closing"));
        }
    }

    @Test
    public void shouldFailWhileTryingToReturnConnectionToPool()
        throws DAOException, SQLException, InterruptedException, ParseException, NumberFormatException, IOException
    {
        try
        {
            JDBCConnectionPool connectionPool = mock(JDBCConnectionPool.class);
            Connection connection = mock(Connection.class);
            doThrow(InterruptedException.class).when(connectionPool).returnConnection(connection);
            Whitebox.setInternalState(JDBCConnectionPool.class, connectionPool);
            AbstractDAO abstractDAO = new QuestionDAO();
            abstractDAO.returnConnectionToPool(connection);
            fail();
        }
        catch (DAOException e)
        {
            assertThat(e.getMessage(), equalTo("Exception in connection returning"));
        }
    }
}
