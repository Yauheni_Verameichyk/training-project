package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Test;

import exception.CommandException;

public class CommandUtilTest
{
    @Test
    public void shouldReturnTextFromPropertiesFile()
        throws IOException
    {
        assertThat(CommandUtil.getMessage("en", "questions.were.added"), equalTo("Questions were added"));
    }

    @Test
    public void shouldReturnPasswordHash()
        throws IOException, CommandException
    {
        assertThat(CommandUtil.hash("asd"), equalTo("7815696ecbf1c96e6894b779456d330e"));
    }
}
