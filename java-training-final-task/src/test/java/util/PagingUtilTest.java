package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Test;

import exception.CommandException;

public class PagingUtilTest
{
    @Test
    public void shouldReturnCurrentPage()
        throws IOException, CommandException
    {
        String requestParam = "2";
        assertThat(PagingUtil.getCurrentPage(requestParam), equalTo(2));
    }

    @Test
    public void shouldReturnOne()
        throws IOException, CommandException
    {
        assertThat(PagingUtil.getCurrentPage(null), equalTo(1));
    }

    @Test
    public void shouldReturnPagesNumberIfRowsNumberMultipleOfRecordsPerPage()
        throws IOException, CommandException
    {
        assertThat(PagingUtil.getPagesNumber(40, 5), equalTo(8));
    }

    @Test
    public void shouldReturnPagesNumberIfRowsNumberNotMultipleOfRecordsPerPage()
        throws IOException, CommandException
    {
        assertThat(PagingUtil.getPagesNumber(41, 5), equalTo(9));
    }

    @Test
    public void shouldReturnRecordsPerPageNumber()
        throws IOException, CommandException
    {
        assertThat(PagingUtil.getRecordsPerPage(), equalTo(5));
    }
}
