package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Properties;

import org.junit.Test;

public class PropertyReaderUtilTest
{
    @Test
    public void shouldReturnPropertyFromFile()
        throws IOException
    {
        assertThat(PropertyReader.getProperty("user", "test-config.properties"), equalTo("root"));
    }

    @Test
    public void shouldReturnPropertyFile()
        throws IOException
    {
        assertThat(PropertyReader.getPropertyFile("test-config.properties"), equalTo(getPropertiesFile()));
    }

    private Properties getPropertiesFile()
    {
        Properties prop = new Properties();
        prop.put("user", "root");
        prop.put("password", "pass");
        prop.put("autoReconnect", "true");
        prop.put("characterEncoding", "UTF-8");
        prop.put("useUnicode", "true");
        return prop;
    }
}
