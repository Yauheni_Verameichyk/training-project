package util;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.hamcrest.Matchers;
import org.junit.Test;

import exception.CommandException;

public class ExceptionHandlingUtilTest
{
    @Test
    public void shouldThrowAndHandleException()
        throws IOException
    {
        HttpServletRequest request = mock(HttpServletRequest.class);
        String expectedUrl = "/WEB-INF/jsp/common/error.jsp";
        try
        {
            throw new CommandException(null);
        }
        catch (CommandException e)
        {
            String actualUrl = ExceptionHandlingUtil.handleException(request, e);
            assertThat(actualUrl, Matchers.equalTo(expectedUrl));
        }
    }
}
