-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: surveydb
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `AnswerID` int(11) NOT NULL AUTO_INCREMENT,
  `AnswerBody` varchar(200) DEFAULT NULL,
  `UserID` int(11) NOT NULL,
  `SurveyID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `AnswerDate` date DEFAULT NULL,
  PRIMARY KEY (`AnswerID`),
  KEY `fk_Answers_User1` (`UserID`),
  KEY `fk_answers_surveys_m2m_questions1_idx` (`SurveyID`,`QuestionID`),
  CONSTRAINT `fk_Answers_User1` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_answers_surveys_m2m_questions1` FOREIGN KEY (`SurveyID`, `QuestionID`) REFERENCES `surveys_m2m_questions` (`SurveyID`, `QuestionID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='Consists answers to all questions from all surveys to all users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,'Да',2,1,1,'2017-04-13'),(2,'Нет',2,1,4,'2017-04-13'),(3,'Нейтрально',2,1,5,'2017-04-13'),(4,'Да',2,3,11,'2008-06-23'),(5,'Православие',2,3,12,'2008-06-23'),(6,'Нет',2,3,13,'2008-06-23'),(7,'Прорыв в науке',1,2,6,'2012-06-25'),(8,'Да',1,2,7,'2012-06-25'),(9,'Высшее',1,2,14,'2012-06-25'),(10,'Нет',1,3,11,'2012-06-25'),(11,'Атеизм',1,3,12,'2012-06-25'),(12,'Нет',1,3,13,'2012-06-25'),(13,'Никакую',3,1,1,'2017-06-03'),(14,'Нет',3,1,4,'2017-06-03'),(15,'Прорыв',4,2,6,'2017-07-10'),(16,'Сама',4,2,7,'2017-07-10'),(17,'22',4,2,8,'2017-07-10'),(18,'Высшее',4,2,14,'2017-07-10'),(19,'Отрицательно',4,2,17,'2017-07-10'),(20,'Положительно',2,32,17,'2017-07-15'),(21,'Все равно',2,32,52,'2017-07-15'),(22,'Ананас',2,32,53,'2017-07-15'),(23,'Не знаю',2,32,54,'2017-07-15'),(24,'Нет',4,33,40,'2017-07-18'),(25,'Никакой',4,33,42,'2017-07-18'),(26,'Нет',4,33,45,'2017-07-18'),(27,'0',4,33,46,'2017-07-18'),(28,'0',4,33,47,'2017-07-18'),(29,'Нигде',4,33,49,'2017-07-18'),(30,'Ну его',4,32,17,'2017-07-18'),(31,'Нет',4,32,52,'2017-07-18'),(32,'нет',4,32,53,'2017-07-18'),(33,'нет',4,32,54,'2017-07-18');
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `QuestionID` int(11) NOT NULL AUTO_INCREMENT,
  `QuestionBody` varchar(255) NOT NULL,
  `ThemeID` int(11) NOT NULL,
  PRIMARY KEY (`QuestionID`),
  KEY `fk_Questions_Themes1_idx` (`ThemeID`),
  CONSTRAINT `fk_Questions_Themes1` FOREIGN KEY (`ThemeID`) REFERENCES `themes` (`ThemeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='Consists questions that may be used in different surveys Has 1:n relationship with "answers" because one question may have many answers for different users, but one answer belongs only to the particular question and user';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,'Какую политическую партию Вы поддерживаете?',1),(2,'Какой вид рекламы Вас больше всего раздражает?',3),(3,'Как Вы относитесь к увеличению пенсионного возраста?',3),(4,'Интересуетесь ли Вы новостями политики?',1),(5,'Следите ли Вы за исполнением предвыборных обещаний политиков?',1),(6,'Большой адронный коллайдер - угроза человечеству или прорыв в науке?',4),(7,'Сами ли Вы переустанавливаете ОС на домашнем ПК?',4),(8,'Сколько книг вы читаете в месяц?',3),(9,'Волнует ли Вас курс рубля?',2),(10,'Брали ли Вы когда-нибудь кредит?',2),(11,'Верите ли Вы в Бога?',5),(12,'Представителем какой религии Вы являетесь?',5),(13,'Часто ли Вы молитесь?',5),(14,'Какое у Вас образование?',6),(17,'Как вы относитесь в предмету \"Основы религиозной культуры\" в школах?',6),(40,'Занимаетесь ли вы спортом?',8),(41,'Как привлечь молодежь к спорту?',8),(42,'Какой вид спорта вы предпочитаете?',8),(43,'Есть ли у Вас, спортивный разряд?',8),(45,'Следите ли вы за спортивными новостями?',8),(46,'Сколько раз в неделю вы занимаетесь спортом?',8),(47,'Сколько часов вы уделяете спортивным занятиям?',8),(49,'Где вы предпочитаете заниматься?',8),(51,'Довольны ли Вы программой внеурочной деятельности, предложенной школой?',6),(52,'Устраивает ли Вас то, как в школе следят за посещаемостью?',6),(53,'Удовлетворены ли Вы количеством учащихся в классе?',6),(54,'Удовлетворены ли Вы условиями, созданными в школе?',6),(56,'<script>alert(\"AAAAA\");</script>',4);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(15) NOT NULL,
  PRIMARY KEY (`RoleID`),
  UNIQUE KEY `RoleName_UNIQUE` (`RoleName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Consists user roles which determine user''s level of access. Has 1:n relationship with "users" because one role can be granted to many users, but a user can have only one role.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ADMIN'),(2,'USER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `SurveyID` int(11) NOT NULL AUTO_INCREMENT,
  `SurveyName` varchar(45) NOT NULL,
  PRIMARY KEY (`SurveyID`),
  UNIQUE KEY `SurveyName_UNIQUE` (`SurveyName`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='Consists surveys that can be passed by users. Has n:m relationship with "questions", because one question may be present in many surveys, and one survey may have many questions. Has 1:n relationship with "answers" because as one survey has many questions, it may also have many answers, but one answer belongs only to the particular survey';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys`
--

LOCK TABLES `surveys` WRITE;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
INSERT INTO `surveys` VALUES (26,'Влияние соцсетей'),(2,'Наука и образование'),(30,'Отношение к политике'),(31,'Покупательское поведение'),(1,'Политика'),(3,'Религия и вера в обществе'),(33,'Спорт и молодежь'),(25,'Финансовая грамотность'),(27,'Цензура и общество'),(32,'Школа глазами родителей');
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveys_m2m_questions`
--

DROP TABLE IF EXISTS `surveys_m2m_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys_m2m_questions` (
  `SurveyID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  PRIMARY KEY (`SurveyID`,`QuestionID`),
  KEY `fk_Surveys_has_Questions_Questions1_idx` (`QuestionID`),
  KEY `fk_Surveys_has_Questions_Surveys1_idx` (`SurveyID`),
  CONSTRAINT `fk_Surveys_has_Questions_Questions1` FOREIGN KEY (`QuestionID`) REFERENCES `questions` (`QuestionID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Surveys_has_Questions_Surveys1` FOREIGN KEY (`SurveyID`) REFERENCES `surveys` (`SurveyID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='surveys_m2m_questions - Summary table for surveys and questions representing n:m relationship and used to avoid multiple similar entries in "surveys"';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys_m2m_questions`
--

LOCK TABLES `surveys_m2m_questions` WRITE;
/*!40000 ALTER TABLE `surveys_m2m_questions` DISABLE KEYS */;
INSERT INTO `surveys_m2m_questions` VALUES (1,1),(1,4),(26,4),(1,5),(2,6),(26,6),(2,7),(26,7),(2,8),(3,11),(3,12),(3,13),(2,14),(2,17),(3,17),(32,17),(33,40),(33,41),(33,42),(33,45),(33,46),(33,47),(33,49),(32,52),(32,53),(32,54);
/*!40000 ALTER TABLE `surveys_m2m_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes` (
  `ThemeID` int(11) NOT NULL AUTO_INCREMENT,
  `ThemeName` varchar(15) NOT NULL,
  PRIMARY KEY (`ThemeID`),
  UNIQUE KEY `ThemeName_UNIQUE` (`ThemeName`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Consists topics list. Has 1:n relationship with "questions" because one theme may be applicable to many questions, but a question can have only one theme.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes`
--

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` VALUES (9,'AUTO'),(7,'CAREER'),(2,'ECONOMICS'),(6,'EDUCATION'),(1,'POLITICS'),(5,'RELIGION'),(4,'SCIENCE'),(3,'SOCIETY'),(8,'SPORT');
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Sex` varchar(6) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Email` varchar(45) NOT NULL,
  `RoleID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Username_UNIQUE` (`Username`),
  KEY `fk_users_roles1_idx` (`RoleID`),
  CONSTRAINT `fk_users_roles1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='Consists information about all users in the system. Has 1:n relationship with "answers" because one user may answer to many questions and have many answers, but one answer belongs only to the particular user';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Jhon','25d55ad283aa400af464c76d713c07ad','M','1993-09-09','qwe@yandex.by',1),(2,'Ivan','d8578edf8458ce06fbc5bb76a58c5ca4','M','1985-09-09','rty@gmail.com',2),(3,'pinaple1993','698d51a19d8a121ce581499d7b701668','M','1999-06-17','bnn@mail.ru',2),(4,'qwe','7815696ecbf1c96e6894b779456d330e','F','1993-11-22','zxcvbn@mail.ru',1),(5,'Anna','5fa72358f0b4fb4f2c5d7de8c9a41846','F','1977-09-09','bggggnn@mail.ru',2),(6,'Korvin','eb89f40da6a539dd1b1776e522459763','M','2000-12-09','bntgyun@mail.ru',2),(13,'Prokudovich','7cc8231eef73a69bf441a66d3f995c98','M','1992-02-07','prokudovich@mail.ru',2),(15,'Mantis2','ca8a9b90299f374ee32330ee560d0635','M',NULL,'Mantis@yandex.by',2),(16,'Mantis','c4df707bf000f588a25e95a3d3e55f69','M',NULL,'mantis@mail.ru',2),(17,'TestUser','b144ef4a51d09eb8878c77bdc4ee8e6e',NULL,NULL,'qwe@yande.ry',2),(18,'Ananas','10cbd6ba64ab5ce6533c3c0540776829','M',NULL,'ananas@mail.ru',2),(19,'Qwe123','b144ef4a51d09eb8878c77bdc4ee8e6e','F',NULL,'Qwe@123.ru',2),(20,'Ulfric','7def94d57f6bc2cc59ee8f465699789a','M',NULL,'Qwe@123.ru',2),(21,'Fallout','1ff2872d0ec056ddfc4d9bf50e3d4335','M',NULL,'Qwe@123.ru',2),(22,'Qaxwsx','1bd2966f9f5279800dea7b2e67cd6c84','M',NULL,'Qaz@wqe.1',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-23 13:57:56
