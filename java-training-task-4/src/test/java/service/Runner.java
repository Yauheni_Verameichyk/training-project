package service;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import entity.DestinationStation;
import entity.Passenger;
import entity.RailwayPlatform;
import entity.RailwayTrack;
import entity.Train;

public class Runner
{
    public static void main(String[] args)
    {
        List<Passenger> passangers = new CopyOnWriteArrayList<>();
        Passenger pass1 = new Passenger(DestinationStation.MINSK);
        Passenger pass2 = new Passenger(DestinationStation.GRODNO);
        Passenger pass3 = new Passenger(DestinationStation.BREST);
        Passenger pass4 = new Passenger(DestinationStation.MINSK);
        Passenger pass5 = new Passenger(DestinationStation.BREST);
        Passenger pass6 = new Passenger(DestinationStation.LIDA);
        Passenger pass7 = new Passenger(DestinationStation.ORSHA);
        Passenger pass8 = new Passenger(DestinationStation.MINSK);
        Passenger pass9 = new Passenger(DestinationStation.GRODNO);
        Passenger pass10 = new Passenger(DestinationStation.BREST);
        passangers.add(pass1);
        passangers.add(pass2);
        passangers.add(pass3);
        passangers.add(pass4);
        passangers.add(pass5);
        passangers.add(pass6);
        passangers.add(pass7);
        passangers.add(pass8);
        passangers.add(pass9);
        passangers.add(pass10);
        ReentrantLock lockerOne = new ReentrantLock(true);
        ReentrantLock lockerTwo = new ReentrantLock(true);
        RailwayTrack trackOne = new RailwayTrack(lockerOne, "Track 1");
        RailwayTrack trackTwo = new RailwayTrack(lockerTwo, "Track 2");
        RailwayPlatform.getRailwayPlatform(trackOne, trackTwo);
        ExecutorService executor = Executors.newFixedThreadPool(5);
        executor.execute(new Train(DestinationStation.MINSK, passangers));
        executor.execute(new Train(DestinationStation.BREST, passangers));
        executor.execute(new Train(DestinationStation.GRODNO, passangers));
        executor.execute(new Train(DestinationStation.ORSHA, passangers));
        executor.execute(new Train(DestinationStation.LIDA, passangers));
        executor.shutdown();
    }
}
