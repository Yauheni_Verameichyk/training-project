package service;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.junit.Test;

import entity.DestinationStation;
import entity.Passenger;
import entity.Train;

public class PassengerServiceTest
{

    @Test
    public void shouldTransferPassengerToTrainToGrodno()
    {
        Train trainToGrodno = getTrainToGrodnoWithOnePassengerToGrodno();
        Train trainToMinsk = getTrainToMinskWithOnePassengerToGrodno();
        Passenger passenger = getTrainToMinskWithOnePassengerToGrodno().getPassengers().get(0);
        PassengerService.transferToOtherTrain(trainToMinsk, trainToGrodno, passenger);
        assertTrue(trainToGrodno.getPassengers().contains(passenger));
    }

    private static Train getTrainToMinskWithOnePassengerToGrodno()
    {
        List<Passenger> passangers = new CopyOnWriteArrayList<>();
        Passenger pass = new Passenger(DestinationStation.GRODNO);
        passangers.add(pass);
        Train train = new Train(DestinationStation.MINSK, passangers);
        return train;
    }

    private static Train getTrainToGrodnoWithOnePassengerToGrodno()
    {
        List<Passenger> passangers = new CopyOnWriteArrayList<>();
        Passenger pass = new Passenger(DestinationStation.GRODNO);
        passangers.add(pass);
        Train train = new Train(DestinationStation.GRODNO, passangers);
        return train;
    }
}
