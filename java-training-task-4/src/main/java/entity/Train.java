package entity;

import java.util.List;

import service.TrainService;

public class Train implements Runnable
{
    private DestinationStation destination;
    private List<Passenger> passengers;
    private volatile long departureTime;

    public Train()
    {
    }

    public Train(DestinationStation destination, List<Passenger> passengers)
    {
        this.destination = destination;
        this.passengers = passengers;
    }

    @Override
    public void run()
    {
        RailwayTrack trackOne = RailwayPlatform.getRailwayPlatform().getRealwayTrackOne();
        RailwayTrack trackTwo = RailwayPlatform.getRailwayPlatform().getRealwayTrackTwo();
        if (destination == DestinationStation.MINSK | destination == DestinationStation.ORSHA)
        {
            TrainService.arriveAtPlatform(trackOne, trackTwo, this);
        }
        else
        {
            TrainService.arriveAtPlatform(trackTwo, trackOne, this);
        }
    }

    public DestinationStation getDestination()
    {
        return destination;
    }

    public void setDestination(DestinationStation destination)
    {
        this.destination = destination;
    }

    public List<Passenger> getPassengers()
    {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers)
    {
        this.passengers = passengers;
    }

    public long getDepartureTime()
    {
        return departureTime;
    }

    public void setDepartureTime(long departureTime)
    {
        this.departureTime = departureTime;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (departureTime ^ (departureTime >>> 32));
        result = prime * result + ((destination == null) ? 0 : destination.hashCode());
        result = prime * result + ((passengers == null) ? 0 : passengers.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Train other = (Train) obj;
        if (departureTime != other.departureTime)
            return false;
        if (destination != other.destination)
            return false;
        if (passengers == null)
        {
            if (other.passengers != null)
                return false;
        }
        else if (!passengers.equals(other.passengers))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Train [destination=" + destination + ", passengers=" + passengers + ", departureTime="
                + departureTime + "]";
    }
}
