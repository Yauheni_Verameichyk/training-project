package entity;

public class RailwayPlatform
{
    private static RailwayPlatform railwayPlatform;

    private RailwayTrack trackOne;
    private RailwayTrack trackTwo;

    private RailwayPlatform()
    {
    }

    private RailwayPlatform(RailwayTrack trackOne, RailwayTrack trackTwo)
    {
        this.trackOne = trackOne;
        this.trackTwo = trackTwo;
    }

    public static RailwayPlatform getRailwayPlatform(RailwayTrack trackOne,
        RailwayTrack trackTwo)
    {
        if (railwayPlatform == null)
        {
            railwayPlatform = new RailwayPlatform(trackOne, trackTwo);
        }
        return railwayPlatform;
    }

    public static RailwayPlatform getRailwayPlatform()
    {
        return railwayPlatform;
    }

    public RailwayTrack getRealwayTrackOne()
    {
        return trackOne;
    }

    public RailwayTrack getRealwayTrackTwo()
    {
        return trackTwo;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((trackOne == null) ? 0 : trackOne.hashCode());
        result = prime * result + ((trackTwo == null) ? 0 : trackTwo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RailwayPlatform other = (RailwayPlatform) obj;
        if (trackOne == null)
        {
            if (other.trackOne != null)
                return false;
        }
        else if (!trackOne.equals(other.trackOne))
            return false;
        if (trackTwo == null)
        {
            if (other.trackTwo != null)
                return false;
        }
        else if (!trackTwo.equals(other.trackTwo))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "RailwayPlatform [trackOne=" + trackOne + ", trackTwo=" + trackTwo + "]";
    }

}
