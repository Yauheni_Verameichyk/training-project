package entity;

import java.util.concurrent.locks.ReentrantLock;

public class RailwayTrack
{
    private ReentrantLock locker;
    private String trackName;
    private Train train;

    public RailwayTrack()
    {
    }

    public RailwayTrack(ReentrantLock locker, String trackName)
    {
        this.locker = locker;
        this.trackName = trackName;
    }

    public Train getTrain()
    {
        return train;
    }

    public void setTrain(Train currentTrain)
    {
        this.train = currentTrain;
    }

    public String getTrackName()
    {
        return trackName;
    }

    public void setTrackName(String trackName)
    {
        this.trackName = trackName;
    }

    public ReentrantLock getLocker()
    {
        return locker;
    }

    public void setLocker(ReentrantLock locker)
    {
        this.locker = locker;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((locker == null) ? 0 : locker.hashCode());
        result = prime * result + ((trackName == null) ? 0 : trackName.hashCode());
        result = prime * result + ((train == null) ? 0 : train.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RailwayTrack other = (RailwayTrack) obj;
        if (locker == null)
        {
            if (other.locker != null)
                return false;
        }
        else if (!locker.equals(other.locker))
            return false;
        if (trackName == null)
        {
            if (other.trackName != null)
                return false;
        }
        else if (!trackName.equals(other.trackName))
            return false;
        if (train == null)
        {
            if (other.train != null)
                return false;
        }
        else if (!train.equals(other.train))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "RailwayTrack [locker=" + locker + ", trackName=" + trackName + ", train=" + train + "]";
    }
}
