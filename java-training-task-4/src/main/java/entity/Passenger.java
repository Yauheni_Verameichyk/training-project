package entity;

import java.util.Calendar;
import java.util.Random;

import org.apache.log4j.Logger;

import service.PassengerService;

public class Passenger
{
    private static final Logger logger = Logger.getLogger(Passenger.class);

    private DestinationStation destination;

    public Passenger()
    {
    }

    public Passenger(DestinationStation destination)
    {
        this.destination = destination;
    }

    public void decideIfTransferToOtherTrain(Train currentTrain, Train nearbyTrain)
    {
        Random rand = new Random();
        long transferTime = rand.nextInt(50);
        if (destination == nearbyTrain.getDestination()
                && isEnoughTimeForTransfer(nearbyTrain, transferTime))
        {
            logger.info("Passenger transfered from train to " + currentTrain.getDestination() + " to train to "
                    + destination);
            PassengerService.transferToOtherTrain(currentTrain, nearbyTrain, this);
        }
    }

    private boolean isEnoughTimeForTransfer(Train nearbyTrain, long timeToChangeTrain)
    {
        return nearbyTrain.getDepartureTime() - Calendar.getInstance().getTimeInMillis() > timeToChangeTrain;
    }

    public DestinationStation getDestination()
    {
        return destination;
    }

    public void setDestination(DestinationStation destination)
    {
        this.destination = destination;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((destination == null) ? 0 : destination.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Passenger other = (Passenger) obj;
        if (destination != other.destination)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Passenger [destination=" + destination + "]";
    }
}
