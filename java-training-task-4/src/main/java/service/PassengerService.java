package service;

import entity.Passenger;
import entity.Train;

public class PassengerService
{
    public static void disembarkPassengersFromTrain(Train from, Train to)
    {
        for (Passenger passenger : from.getPassengers())
        {
            passenger.decideIfTransferToOtherTrain(from, to);
        }
    }

    public static void transferToOtherTrain(Train fromTrain, Train toTrain, Passenger passenger)
    {
        fromTrain.getPassengers().remove(passenger);
        toTrain.getPassengers().add(passenger);
    }
}
