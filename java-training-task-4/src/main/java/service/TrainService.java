package service;

import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import entity.RailwayTrack;
import entity.Train;

public class TrainService
{
    private static final Logger logger = Logger.getLogger(TrainService.class);

    public static void arriveAtPlatform(RailwayTrack currentTrack, RailwayTrack nearbyTrack, Train currentTrain)
    {
        ReentrantLock locker = currentTrack.getLocker();
        locker.lock();
        currentTrack.setTrain(currentTrain);
        try
        {
            Random rand = new Random();
            int timeOnStation = rand.nextInt(1000);
            currentTrain.setDepartureTime(Calendar.getInstance().getTimeInMillis() + timeOnStation);
            logger.info("Train to " + currentTrain.getDestination() + " arrived at " + currentTrack.getTrackName());
            if (nearbyTrack.getTrain() != null)
            {
                PassengerService.disembarkPassengersFromTrain(currentTrain, nearbyTrack.getTrain());
                PassengerService.disembarkPassengersFromTrain(nearbyTrack.getTrain(), currentTrain);
            }
            Thread.sleep(timeOnStation);
            logger.info("Train to " + currentTrain.getDestination() + " departed from " + currentTrack.getTrackName());
        }
        catch (InterruptedException e)
        {
            logger.error("Error while train is on station", e);
        }
        finally
        {
            currentTrack.setTrain(null);
            locker.unlock();
        }
    }
}
