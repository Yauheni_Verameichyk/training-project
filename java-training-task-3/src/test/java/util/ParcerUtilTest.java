package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static util.ParserUtil.parseParagraph;
import static util.ParserUtil.parseSentence;
import static util.ParserUtil.parseWord;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import entity.Sentence;
import entity.Symbol;
import entity.Word;

public class ParcerUtilTest
{
    private static final String WORD = "ThirdTask";
    private static final String SENTENCE = "Third task! ";
    private static final String PARAGRAPH = "Third task! It is almost done.";

    @Test
    public void shouldReturnParsedWord()
    {
        assertThat(parseWord(WORD), equalTo(getExpectedWord()));
    }

    @Test
    public void shouldReturnParsedSentence()
    {
        assertThat(parseSentence(SENTENCE), equalTo(getExpectedSentence()));
    }

    @Test
    public void shouldReturnParsedParagraph()
    {
        Sentence actual = parseParagraph(PARAGRAPH).getSentences().get(0);
        assertThat(actual, equalTo(getExpectedSentence()));
    }

    private static Sentence getExpectedSentence()
    {
        Sentence expectedSentence = new Sentence();
        List<Object> sentenceElements = new ArrayList<>();
        Word third = getWord("Third");
        Symbol symbol1 = new Symbol();
        symbol1.setSymbol(" ");
        Word task = getWord("task");
        Symbol symbol2 = new Symbol();
        symbol2.setSymbol("!");
        Symbol symbol3 = new Symbol();
        symbol3.setSymbol(" ");
        sentenceElements.add(third);
        sentenceElements.add(symbol1);
        sentenceElements.add(task);
        sentenceElements.add(symbol2);
        sentenceElements.add(symbol3);
        expectedSentence.setSentenceElements(sentenceElements);
        return expectedSentence;
    }

    private static Word getWord(String string)
    {
        List<Symbol> symbols = new ArrayList<>();
        Word word = new Word();
        String stringArray[] = string.split("");
        for (int i = 0; i < stringArray.length; i++)
        {
            Symbol symbol = new Symbol();
            symbol.setSymbol(stringArray[i]);
            symbols.add(symbol);
        }
        word.setSymbols(symbols);
        return word;
    }

    private static Word getExpectedWord()
    {
        Word expectedWord = new Word();
        List<Symbol> symbols = new ArrayList<>();
        String word[] = WORD.split("");
        for (int i = 0; i < word.length; i++)
        {
            Symbol symbol = new Symbol();
            symbol.setSymbol(word[i]);
            symbols.add(symbol);
        }
        expectedWord.setSymbols(symbols);
        return expectedWord;
    }
}
