package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static util.ParserUtil.parseParagraph;
import static util.WordUtil.getWordsBeginWithVowel;
import static util.WordUtil.getWordsFromParagraph;
import static util.WordUtil.sordWordsByLetterNumber;
import static util.WordUtil.sortWordsByAlphabeticalOrder;
import static util.WordUtil.sortWordsByDescendVowelProportion;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import entity.Symbol;
import entity.Word;

public class WordUtilTest
{
    private static final String PARAGRAPH = "Third task! It is almost done.";

    @Test
    public void shouldReturnWordsFromParagraph()
    {
        List<Word> actual = getWordsFromParagraph(parseParagraph(PARAGRAPH));
        assertThat(actual, equalTo(getExpectedWords()));
    }

    @Test
    public void shouldReturnDescendingSortedWords()
    {
        List<Word> actual = getWordsFromParagraph(parseParagraph(PARAGRAPH));
        sortWordsByDescendVowelProportion(actual);
        assertThat(actual, equalTo(getDescendingSortedWords()));
    }

    @Test
    public void shouldReturnWordsBeginWithVowel()
    {
        List<Word> actual = getWordsBeginWithVowel(getExpectedWords());
        assertThat(actual, equalTo(getExpectedWordsBeginWithVowel()));
    }

    @Test
    public void shouldSortWordsByAlphabeticalNumber()
    {
        List<Word> actual = getExpectedWords();
        sortWordsByAlphabeticalOrder(actual);
        assertThat(actual, equalTo(getExpectedWordsSortedByAlphabeticalNumber()));
    }

    @Test
    public void shouldSortWordsByLetterNumber()
    {
        List<Word> actual = getExpectedWords();
        sordWordsByLetterNumber(actual, "i");
        assertThat(actual, equalTo(getExpectedWordsSortedByLetterNumber()));
    }

    private static List<Word> getExpectedWordsBeginWithVowel()
    {
        List<Word> words = new ArrayList<>();
        Word it = getWord("It");
        Word is = getWord("is");
        Word almost = getWord("almost");
        words.add(it);
        words.add(is);
        words.add(almost);
        return words;
    }

    private static List<Word> getExpectedWordsSortedByAlphabeticalNumber()
    {
        List<Word> words = new ArrayList<>();
        Word third = getWord("Third");
        Word task = getWord("task");
        Word it = getWord("It");
        Word is = getWord("is");
        Word almost = getWord("almost");
        Word done = getWord("done");
        words.add(almost);
        words.add(done);
        words.add(is);
        words.add(it);
        words.add(task);
        words.add(third);
        return words;
    }

    private static List<Word> getExpectedWordsSortedByLetterNumber()
    {
        List<Word> words = new ArrayList<>();
        Word third = getWord("Third");
        Word task = getWord("task");
        Word it = getWord("It");
        Word is = getWord("is");
        Word almost = getWord("almost");
        Word done = getWord("done");
        words.add(almost);
        words.add(done);
        words.add(task);
        words.add(is);
        words.add(it);
        words.add(third);
        return words;
    }

    private static List<Word> getExpectedWords()
    {
        List<Word> words = new ArrayList<>();
        Word third = getWord("Third");
        Word task = getWord("task");
        Word it = getWord("It");
        Word is = getWord("is");
        Word almost = getWord("almost");
        Word done = getWord("done");
        words.add(third);
        words.add(task);
        words.add(it);
        words.add(is);
        words.add(almost);
        words.add(done);
        return words;
    }

    private static List<Word> getDescendingSortedWords()
    {
        List<Word> words = new ArrayList<>();
        Word third = getWord("Third");
        Word task = getWord("task");
        Word it = getWord("It");
        Word is = getWord("is");
        Word almost = getWord("almost");
        Word done = getWord("done");
        words.add(it);
        words.add(is);
        words.add(done);
        words.add(almost);
        words.add(task);
        words.add(third);
        return words;
    }

    private static Word getWord(String string)
    {
        List<Symbol> symbols = new ArrayList<>();
        Word word = new Word();
        String stringArray[] = string.split("");
        for (int i = 0; i < stringArray.length; i++)
        {
            Symbol symbol = new Symbol();
            symbol.setSymbol(stringArray[i]);
            symbols.add(symbol);
        }
        word.setSymbols(symbols);
        return word;
    }
}
