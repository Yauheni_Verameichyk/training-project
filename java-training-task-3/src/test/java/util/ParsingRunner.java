package util;

import static util.ParserUtil.parseText;
import static util.ReaderUtil.readTextFromFile;
import static util.WordUtil.getWordsBeginWithVowel;
import static util.WordUtil.getWordsFromText;
import static util.WordUtil.sordWordsByLetterNumber;
import static util.WordUtil.sortWordsByAlphabeticalOrder;
import static util.WordUtil.sortWordsByAscendVowelProportion;
import static util.WordUtil.sortWordsByDescendVowelProportion;
import static util.WriterUtil.writeTextIntoFile;

import java.util.List;

import org.junit.Test;

import entity.Text;
import entity.Word;
import exception.FileReadException;
import exception.FileWriteException;

public class ParsingRunner
{
    @Test
    public void runParsing() throws FileReadException, FileWriteException
    {
        final String path = "src//test//resources//txt//originalText.txt";
        final String path2 = "src//test//resources//txt//finalText.txt";
        Text text = parseText(readTextFromFile(path));
        List<Word> words = getWordsFromText(text);
        System.out.println(words);
        sortWordsByDescendVowelProportion(words);
        System.out.println(words);
        sortWordsByAscendVowelProportion(words);
        System.out.println(words);
        writeTextIntoFile(path2, text);
        List<Word> wordsBeginWithVowel = getWordsBeginWithVowel(words);
        System.out.println(wordsBeginWithVowel);
        sortWordsByAlphabeticalOrder(wordsBeginWithVowel);
        System.out.println(wordsBeginWithVowel);
        sordWordsByLetterNumber(words, "a");
        System.out.println(words);
    }
}
