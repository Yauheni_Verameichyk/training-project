package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static util.ReaderUtil.readTextFromFile;

import org.junit.Test;

import exception.FileReadException;

public class ReaderUtilTest
{
    private static final String PATH = "src//test//resources//txt//textExample.txt";
    private static final String WRONG_PATH = "src//test//resourcfes//txt//textExample.txt";

    @Test
    public void shouldReturnTextFromFile() throws FileReadException
    {
        String expected = "Third task! It is almost done.";
        String actual = readTextFromFile(PATH);
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void shouldFailIfFilePathToPointsIncorrect() throws FileReadException
    {
        try
        {
            readTextFromFile(WRONG_PATH);
            fail();
        }
        catch (FileReadException e)
        {
            assertThat(e.getMessage(), equalTo("Unable to read file " + WRONG_PATH));
        }
    }
}
