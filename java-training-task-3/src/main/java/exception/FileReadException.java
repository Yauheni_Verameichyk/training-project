package exception;

public class FileReadException extends Exception
{

    private static final long serialVersionUID = 1266013719460696901L;

    public FileReadException()
    {
    }

    public FileReadException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public FileReadException(String message)
    {
        super(message);
    }

    public FileReadException(Throwable cause)
    {
        super(cause);
    }

}
