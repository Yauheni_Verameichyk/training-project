package exception;

public class FileWriteException extends Exception
{

    private static final long serialVersionUID = 1266013459460696901L;

    public FileWriteException()
    {
    }

    public FileWriteException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public FileWriteException(String message)
    {
        super(message);
    }

    public FileWriteException(Throwable cause)
    {
        super(cause);
    }

}
