package entity;

import java.util.List;

public final class Paragraph
{
    private List<Sentence> sentences;

    public List<Sentence> getSentences()
    {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences)
    {
        this.sentences = sentences;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sentences == null) ? 0 : sentences.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Paragraph other = (Paragraph) obj;
        if (sentences == null)
        {
            if (other.sentences != null)
                return false;
        }
        else if (!sentences.equals(other.sentences))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        String paragraph = "";
        for (int i = 0; i < sentences.size(); i++)
            paragraph += sentences.get(i);
        paragraph = paragraph + System.lineSeparator();
        return paragraph;
    }
}
