package entity;

import java.util.List;

public class Sentence
{
    private List<Object> sentenceElements;

    public List<Object> getSentenceElements()
    {
        return sentenceElements;
    }

    public void setSentenceElements(List<Object> sentenceElements)
    {
        this.sentenceElements = sentenceElements;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sentenceElements == null) ? 0 : sentenceElements.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Sentence other = (Sentence) obj;
        if (sentenceElements == null)
        {
            if (other.sentenceElements != null)
                return false;
        }
        else if (!sentenceElements.equals(other.sentenceElements))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        String sentenceElement = "";
        for (int i = 0; i < sentenceElements.size(); i++)
            sentenceElement += sentenceElements.get(i);
        return sentenceElement;
    }
}
