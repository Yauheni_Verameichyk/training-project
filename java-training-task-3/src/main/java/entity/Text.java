package entity;

import java.util.List;

public class Text
{
    private List<Object> textBlocks;

    public List<Object> getTextElements()
    {
        return textBlocks;
    }

    public void setTextElements(List<Object> textBlocks)
    {
        this.textBlocks = textBlocks;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((textBlocks == null) ? 0 : textBlocks.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Text other = (Text) obj;
        if (textBlocks == null)
        {
            if (other.textBlocks != null)
                return false;
        }
        else if (!textBlocks.equals(other.textBlocks))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        String textBlock = "";
        for (int i = 0; i < textBlocks.size(); i++)
            textBlock += textBlocks.get(i);
        return textBlock;
    }

}
