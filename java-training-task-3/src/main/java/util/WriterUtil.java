package util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import exception.FileWriteException;

public class WriterUtil
{
    private static final Logger logger = Logger.getLogger(WriterUtil.class);

    public static void writeTextIntoFile(String path, Object object) throws FileWriteException
    {
        File file = new File(path);
        try
        {
            FileUtils.writeStringToFile(file, object.toString(), "UTF-8");
        }
        catch (IOException e)
        {
            logger.error("Unable to write file " + path);
            throw new FileWriteException("Unable to write file " + path, e);
        }
    }
}
