package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import entity.Paragraph;
import entity.Sentence;
import entity.Text;
import entity.Word;

public class WordUtil
{
    private static final String VOWEL = "[eyuioaEYUIOA]";
    private static final String WORD_BEGIN_WITH_VOWEL = "\\b[eyuioaEYUIOA]+[\\w]+";

    public static List<Word> getWordsFromText(Text text)
    {
        List<Word> words = new ArrayList<>();
        for (Object element : text.getTextElements())
        {
            if (element instanceof Paragraph)
            {
                words.addAll(getWordsFromParagraph((Paragraph) element));
            }
        }
        return words;
    }

    public static List<Word> getWordsFromParagraph(Paragraph paragraph)
    {
        List<Word> words = new ArrayList<>();
        for (Sentence element : paragraph.getSentences())
        {
            words.addAll(getWordsFromSentence(element));
        }
        return words;
    }

    public static List<Word> getWordsFromSentence(Sentence sentence)
    {
        List<Word> words = new ArrayList<>();
        for (Object element : sentence.getSentenceElements())
        {
            if (element instanceof Word)
            {
                words.add((Word) element);
            }
        }
        return words;
    }

    public static void sortWordsByAscendVowelProportion(List<Word> words)
    {
        Comparator<Word> comparator = new Comparator<Word>()
        {
            @Override
            public int compare(Word one, Word two)
            {
                return getVowelQuantity(one) * 100 / one.toString().length()
                        - getVowelQuantity(two) * 100 / two.toString().length();
            }
        };
        Collections.sort(words, comparator);
    }

    public static void sortWordsByDescendVowelProportion(List<Word> words)
    {
        Comparator<Word> comparator = new Comparator<Word>()
        {
            @Override
            public int compare(Word one, Word two)
            {
                return getVowelQuantity(two) * 100 / two.toString().length()
                        - getVowelQuantity(one) * 100 / one.toString().length();
            }
        };
        Collections.sort(words, comparator);
    }

    public static List<Word> getWordsBeginWithVowel(List<Word> words)
    {
        List<Word> wordsBeginWithVowel = new ArrayList<>();
        Pattern beginWithVowelPattern = Pattern.compile(WORD_BEGIN_WITH_VOWEL);
        for (Word word : words)
        {
            Matcher beginWithVowelMatcher = beginWithVowelPattern.matcher(word.toString());
            if (beginWithVowelMatcher.matches())
            {
                wordsBeginWithVowel.add(word);
            }
        }
        return wordsBeginWithVowel;
    }

    public static void sortWordsByAlphabeticalOrder(List<Word> words)
    {
        Comparator<Word> comparator = new Comparator<Word>()
        {
            @Override
            public int compare(Word word1, Word word2)
            {
                int x = String.CASE_INSENSITIVE_ORDER.compare(word1.toString(), word2.toString());
                if (x == 0)
                {
                    x = word1.toString().compareTo(word2.toString());
                }
                return x;
            }
        };
        Collections.sort(words, comparator);
    }

    public static void sordWordsByLetterNumber(List<Word> words, String letter)
    {
        Comparator<Word> comparator = new Comparator<Word>()
        {
            @Override
            public int compare(Word word1, Word word2)
            {
                int x = getLetterQuantity(word1, letter) - getLetterQuantity(word2, letter);
                if (x == 0)
                {
                    x = String.CASE_INSENSITIVE_ORDER.compare(word1.toString(), word2.toString());
                    if (x == 0)
                    {
                        x = word1.toString().compareTo(word2.toString());
                    }
                }
                return x;
            }
        };
        Collections.sort(words, comparator);
    }

    private static int getLetterQuantity(Word word, String letter)
    {
        Pattern letterPattern = Pattern.compile("[" + letter.toLowerCase() +
                "]|[" + letter.toUpperCase() + "]");
        Matcher letterMatcher = letterPattern.matcher(word.toString());
        int letterQuantity = 0;
        while (letterMatcher.find())
        {
            letterQuantity++;
        }
        return letterQuantity;
    }

    private static int getVowelQuantity(Word word)
    {
        Pattern vowelPattern = Pattern.compile(VOWEL);
        Matcher vowelMatcher = vowelPattern.matcher(word.toString());
        int vowelQuantity = 0;
        while (vowelMatcher.find())
        {
            vowelQuantity++;
        }
        return vowelQuantity;
    }
}
