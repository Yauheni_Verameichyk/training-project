package util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import exception.FileReadException;

public class ReaderUtil
{
    private static final Logger logger = Logger.getLogger(ReaderUtil.class);

    public static String readTextFromFile(String path) throws FileReadException
    {
        File file = new File(path);
        try
        {
            String content = FileUtils.readFileToString(file, "UTF-8");
            return content;
        }
        catch (IOException e)
        {
            logger.error("Unable to read file " + path);
            throw new FileReadException("Unable to read file " + path, e);
        }
    }
}
