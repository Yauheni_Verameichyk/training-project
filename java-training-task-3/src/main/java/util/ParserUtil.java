package util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import entity.CodeBlock;
import entity.Paragraph;
import entity.Sentence;
import entity.Symbol;
import entity.Text;
import entity.Word;

public class ParserUtil
{
    private static final String PARAGRAPH_SPLIT_REGEX = "[^\\s][\\s\\S]*?[\\n\\r]";
    private static final String CODE_BLOCK_SPLIT_REGEX = "([^\\n]*(\\{[\\s\\S]*?\\n)*(\\}))";
    private static final String SENTENCE_SPLIT_REGEX = "[\\s\\S]*?([.\\!?][\\s]|[\\n\\r]|$)[\\s]?";
    private static final String WORD_SPLIT_REGEX = "([a-zA-Z]+('[a-zA-Z]+)?)";
    private static final String SYMBOL_SPLIT_REGEX = ".";

    public static Text parseText(String text)
    {
        List<Object> textBlocks = new ArrayList<>();
        Pattern codeBlockPattern = Pattern.compile(CODE_BLOCK_SPLIT_REGEX);
        Matcher codeBlockMatcher = codeBlockPattern.matcher(text);
        Pattern paragraphPattern = Pattern.compile(PARAGRAPH_SPLIT_REGEX);
        int previousIndex = 0;
        int currentIndex = 0;
        while (codeBlockMatcher.find())
        {
            currentIndex = codeBlockMatcher.start();
            Matcher paragraphMatcher = paragraphPattern.matcher(text.substring(previousIndex, currentIndex));
            while (paragraphMatcher.find())
            {
                Paragraph parsedParagraph = parseParagraph(paragraphMatcher.group());
                textBlocks.add(parsedParagraph);
            }
            CodeBlock codeBlock = new CodeBlock();
            codeBlock.setCodeBlock(codeBlockMatcher.group());
            textBlocks.add(codeBlock);
            previousIndex = codeBlockMatcher.end();
        }
        Matcher paragraphMatcher = paragraphPattern.matcher(text.substring(previousIndex));
        while (paragraphMatcher.find())
        {
            Paragraph parsedParagraph = parseParagraph(paragraphMatcher.group());
            textBlocks.add(parsedParagraph);
        }
        Text parsedText = new Text();
        parsedText.setTextElements(textBlocks);
        return parsedText;
    }

    public static Paragraph parseParagraph(String paragraph)
    {
        List<Sentence> sentences = new ArrayList<>();
        Pattern sentencePattern = Pattern.compile(SENTENCE_SPLIT_REGEX);
        Matcher sentenceMatcher = sentencePattern.matcher(paragraph);
        while (sentenceMatcher.find())
        {
            Sentence sentence = parseSentence(sentenceMatcher.group());
            sentences.add(sentence);
        }
        Paragraph parsedParagraph = new Paragraph();
        parsedParagraph.setSentences(sentences);
        return parsedParagraph;
    }

    public static Sentence parseSentence(String sentence)
    {
        List<Object> sentenceElements = new ArrayList<>();
        Pattern wordPattern = Pattern.compile(WORD_SPLIT_REGEX);
        Matcher wordMatcher = wordPattern.matcher(sentence);
        Pattern symbolPattern = Pattern.compile(SYMBOL_SPLIT_REGEX);
        int previousIndex = 0;
        int currentIndex = 0;
        while (wordMatcher.find())
        {
            currentIndex = wordMatcher.start();
            Matcher symbolMatcher = symbolPattern.matcher(sentence.substring(previousIndex, currentIndex));
            while (symbolMatcher.find())
            {
                Symbol symbol = new Symbol();
                symbol.setSymbol(symbolMatcher.group());
                sentenceElements.add(symbol);
            }
            Word parsedWord = parseWord(wordMatcher.group());
            sentenceElements.add(parsedWord);
            previousIndex = wordMatcher.end();
        }
        Matcher symbolMatcher = symbolPattern.matcher(sentence.substring(previousIndex));
        while (symbolMatcher.find())
        {
            Symbol symbol = new Symbol();
            symbol.setSymbol(symbolMatcher.group());
            sentenceElements.add(symbol);
        }
        Sentence parsedSentence = new Sentence();
        parsedSentence.setSentenceElements(sentenceElements);
        return parsedSentence;
    }

    public static Word parseWord(String word)
    {
        List<Symbol> symbols = new ArrayList<>();
        Pattern symbolPattern = Pattern.compile(SYMBOL_SPLIT_REGEX);
        Matcher symbolMatcher = symbolPattern.matcher(word);
        while (symbolMatcher.find())
        {
            Symbol symbol = new Symbol();
            symbol.setSymbol(symbolMatcher.group());
            symbols.add(symbol);
        }
        Word parsedWord = new Word();
        parsedWord.setSymbols(symbols);
        return parsedWord;
    }
}
