<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib  prefix="c"   uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pursing result</title>
</head>
<body>
	<H1> Paper list ${parser}: </H1>
	<table border = 1>
			<tr  height = 50>
				<th width = 100>Element number</th>
				<th width = 100>Title</th>
				<th width = 100>Type</th>
				<th width = 100>Monthly</th>
				<th width = 100>Color</th>
				<th width = 100>Page number</th>
				<th width = 100>Glossy</th>
				<th width = 100>Index</th>
			</tr>
		<c:forEach var="elem" items="${paperList}" varStatus="status">
			<tr  height = 50>
				<td width = 100><c:out value="${ status.count + (currentPage-1)*5}" /></td>
				<td width = 100><c:out value="${ elem.title }" /></td>
				<td width = 100><c:out value="${ elem.type }" /></td>
				<td width = 100><c:out value="${ elem.monthly }" /></td>
				<td width = 100><c:out value="${ elem.chars.color }" /></td>
				<td width = 100><c:out value="${ elem.chars.pageNumber }" /></td>
				<td width = 100><c:out value="${ elem.chars.glossy }" /></td>
				<td width = 100><c:out value="${ elem.chars.index }" /></td>
			</tr>
		</c:forEach>
	</table>
            <c:forEach begin="1" end="${pagesNumber}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <td>${i}</td>
                    </c:when>
                    <c:otherwise>              
                    <td>
                    <form action="PaperServlet" method="GET">
							<input type="submit" name="page" value = "${i}"><br>							
					</form>           
                    </td>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
<a href = index.jsp> Change XML-file</a>
</body>
</html>