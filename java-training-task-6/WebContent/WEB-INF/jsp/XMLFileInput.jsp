<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>XML input page</title>
</head>
<body>
	<form action="PaperServlet" method="POST" enctype="multipart/form-data">
		<p>Add XML:</p>
		<input name="XMLFile" type="file"> 
		<input type="submit" value="Execute"><br> 
		<p>Choose parser type</p>
		<input type="radio" name="parserType" value="SAX" checked="checked"> SAX<br> 
		<input type="radio" name="parserType" value="StAX"> StAX<br> 
		<input type="radio"	name="parserType" value="DOM"> DOM
	</form>
</body>
</html>