package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import entity.Paper;
import entity.Paper.Chars;
import exception.ParserException;

public class PaperStAXBuilder
{
    private static final Logger logger = Logger.getLogger(PaperStAXBuilder.class);

    private List<Paper> papers;
    private XMLInputFactory inputFactory;

    public PaperStAXBuilder()
    {
        papers = new ArrayList<>();
        inputFactory = XMLInputFactory.newInstance();
    }

    public List<Paper> getPapers()
    {
        return papers;
    }

    public void buildListPapers(InputStream fileContent) throws IOException, ParserException, NumberFormatException
    {
        XMLStreamReader reader = null;
        String name;
        try
        {
            reader = inputFactory.createXMLStreamReader(fileContent);
            while (reader.hasNext())
            {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT)
                {
                    name = reader.getLocalName();
                    if (PaperEnum.valueOf(name.toUpperCase()) == PaperEnum.PAPER)
                    {
                        Paper currentPaper = buildPaper(reader);
                        papers.add(currentPaper);
                    }
                }
            }
        }
        catch (XMLStreamException e)
        {
            String message = "StAX parsing error";
            logger.error(message, e);
            throw new ParserException(message, e);
        }
    }

    public Paper buildPaper(XMLStreamReader reader)
        throws NumberFormatException, ParserException, XMLStreamException
    {
        Paper paper = new Paper();
        String elementName;
        while (reader.hasNext())
        {
            int type = reader.next();
            switch (type)
            {
                case XMLStreamConstants.START_ELEMENT:
                    elementName = reader.getLocalName();
                    switch (PaperEnum.valueOf(elementName.toUpperCase()))
                    {
                        case TITLE:
                            paper.setTitle(getXMLText(reader));
                            break;
                        case TYPE:
                            paper.setType(getXMLText(reader));
                            break;
                        case MONTHLY:
                            paper.setMonthly(getXMLText(reader).equals("true"));
                            break;
                        case CHARS:
                            paper.setChars(builtChars(reader));
                            break;
                        default:
                            String message = "Unknown element in tag paper";
                            logger.error(message);
                            throw new ParserException(message);
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    elementName = reader.getLocalName();
                    if (PaperEnum.valueOf(elementName.toUpperCase()) == PaperEnum.PAPER)
                    {
                        return paper;
                    }
                    break;
            }
        }
        String message = "Unknown element in tag paper";
        logger.error(message);
        throw new XMLStreamException(message);
    }

    public Chars builtChars(XMLStreamReader reader)
        throws NumberFormatException, XMLStreamException, ParserException
    {
        Paper.Chars chars = new Paper.Chars();
        String elementName;
        while (reader.hasNext())
        {
            int type = reader.next();
            switch (type)
            {
                case XMLStreamConstants.START_ELEMENT:
                    elementName = reader.getLocalName();
                    switch (PaperEnum.valueOf(elementName.toUpperCase()))
                    {
                        case COLOR:
                            chars.setColor(getXMLText(reader).equals("true"));
                            break;
                        case PAGENUMBER:
                            chars.setPageNumber(new Integer(getXMLText(reader)));
                            break;
                        case GLOSSY:
                            chars.setGlossy(getXMLText(reader).equals("true"));
                            break;
                        case INDEX:
                            chars.setIndex(new Integer(getXMLText(reader)));
                            break;
                        default:
                            String message = "Unknown element in tag chars";
                            logger.error(message);
                            throw new ParserException(message);
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    elementName = reader.getLocalName();
                    if (PaperEnum.valueOf(elementName.toUpperCase()) == PaperEnum.CHARS)
                    {
                        return chars;
                    }
                    break;
            }
        }
        String message = "Unknown element in tag chars";
        logger.error(message);
        throw new ParserException(message);
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException
    {
        String text = null;
        if (reader.hasNext())
        {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
