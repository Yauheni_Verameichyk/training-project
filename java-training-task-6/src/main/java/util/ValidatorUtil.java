package util;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

public class ValidatorUtil
{
    public static boolean isValid(InputStream fileContent)
    {
        try
        {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(
                    new StreamSource(classLoader.getResourceAsStream("xsd\\paperSchema.xsd")));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(fileContent));
        }
        catch (IOException | SAXException e)
        {
            return false;
        }
        return true;
    }
}
