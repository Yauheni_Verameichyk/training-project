package util;

public enum PaperEnum
{
    PAPERS,
    PAPER,
    TITLE,
    TYPE,
    MONTHLY,
    COLOR,
    PAGENUMBER,
    GLOSSY,
    INDEX,
    CHARS;
}
