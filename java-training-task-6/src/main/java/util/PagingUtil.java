package util;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entity.Paper;

public class PagingUtil
{
    public static Map<String, Object> doPaging(List<Paper> papers, Integer page)
        throws NumberFormatException, IOException
    {
        Map<String, Object> result = new HashMap<>();
        int recordsPerPage = new Integer(PropertyReader.getProperty("recordsPerPage"));
        if (page == null)
        {
            page = new Integer(PropertyReader.getProperty("page"));
        }
        int firstPageElementNumber = (page - 1) * recordsPerPage;
        int lastPageElementNumber = (page - 1) * recordsPerPage + recordsPerPage;
        int pagesNumber;
        if ((papers.size() % recordsPerPage) != 0)
        {
            pagesNumber = papers.size() / recordsPerPage + 1;
        }
        else
        {
            pagesNumber = papers.size() / recordsPerPage;
        }
        if (papers.size() < lastPageElementNumber)
        {
            lastPageElementNumber = papers.size();
        }
        result.put("currentPage", page);
        result.put("pagesNumber", pagesNumber);
        result.put("paperList", papers.subList(firstPageElementNumber, lastPageElementNumber));
        return result;
    }

}
