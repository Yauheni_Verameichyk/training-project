package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import entity.Paper;
import exception.ParserException;

public class PaperSAXBuilder
{
    private static final Logger logger = Logger.getLogger(PaperSAXBuilder.class);

    private List<Paper> papers;
    private PaperHandler paperHundler;
    private XMLReader reader;

    public PaperSAXBuilder() throws ParserException
    {
        paperHundler = new PaperHandler();
        try
        {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(paperHundler);
        }
        catch (SAXException e)
        {
            String message = "SAX-parser error";
            logger.error(message, e);
            throw new ParserException(message, e);
        }
    }

    public List<Paper> getPapers()
    {
        return papers;
    }

    public void buildListPapers(InputStream fileContent) throws ParserException, IOException
    {
        try
        {
            reader.parse(new InputSource(fileContent));
        }
        catch (SAXException e)
        {
            String message = "SAX-parser error";
            logger.error(message, e);
            throw new ParserException(message, e);
        }
        papers = paperHundler.getPapers();
    }
}
