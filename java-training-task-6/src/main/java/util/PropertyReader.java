package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader
{
    public static String getProperty(String property) throws IOException
    {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Properties prop = new Properties();
        InputStream input = classLoader.getResourceAsStream("properties/config.properties");
        prop.load(input);
        return prop.getProperty(property);
    }
}
