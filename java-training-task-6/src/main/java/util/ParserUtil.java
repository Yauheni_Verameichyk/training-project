package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import entity.Paper;
import exception.ParserException;
import exception.UnknownParcerException;

public class ParserUtil
{

    public static List<Paper> getPaperListFromXML(InputStream fileContent, String parserType)
        throws ParserException, IOException, UnknownParcerException
    {
        parserType = parserType.toUpperCase();
        ParserEnum parser = ParserEnum.valueOf(parserType);
        switch (parser)
        {
            case SAX:
                PaperSAXBuilder saxBuilder = new PaperSAXBuilder();
                saxBuilder.buildListPapers(fileContent);
                return saxBuilder.getPapers();
            case STAX:
                PaperStAXBuilder staxBuilder = new PaperStAXBuilder();
                staxBuilder.buildListPapers(fileContent);
                return staxBuilder.getPapers();
            case DOM:
                PaperDOMBuilder domBuilder = new PaperDOMBuilder();
                domBuilder.buildListPapers(fileContent);
                return domBuilder.getPapers();
            default:
                throw new UnknownParcerException("Unknown parser");
        }
    }
}
