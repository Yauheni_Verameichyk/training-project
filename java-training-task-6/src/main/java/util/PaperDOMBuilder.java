package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import entity.Paper;
import exception.ParserException;

public class PaperDOMBuilder
{
    private static final Logger logger = Logger.getLogger(PaperDOMBuilder.class);

    private List<Paper> papers;
    private DocumentBuilder docBuilder;

    public PaperDOMBuilder() throws ParserException
    {
        papers = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try
        {
            docBuilder = factory.newDocumentBuilder();
        }
        catch (ParserConfigurationException e)
        {
            String message = "Parser configuration error";
            logger.error(message, e);
            throw new ParserException(message, e);
        }
    }

    public List<Paper> getPapers()
    {
        return papers;
    }

    public void buildListPapers(InputStream fileContent) throws ParserException
    {
        Document doc = null;
        try
        {
            doc = docBuilder.parse(fileContent);
            Element rootElement = doc.getDocumentElement();
            NodeList paperList = rootElement.getElementsByTagName("paper");
            for (int i = 0; i < paperList.getLength(); i++)
            {
                Element paperElement = (Element) paperList.item(i);
                Paper paper = buildPaper(paperElement);
                papers.add(paper);
            }
        }
        catch (IOException e)
        {
            String message = "I/O error";
            logger.error(message, e);
            throw new ParserException(message, e);
        }
        catch (SAXException e)
        {
            String message = "DOM-parser failure";
            logger.error(message, e);
            throw new ParserException(message, e);
        }
    }

    private Paper buildPaper(Element paperElement)
    {
        Paper paper = new Paper();
        Paper.Chars chars = new Paper.Chars();
        paper.setTitle(getElementTextContent(paperElement, "title"));
        paper.setType(getElementTextContent(paperElement, "type"));
        paper.setMonthly(getElementTextContent(paperElement, "monthly").equals("true"));

        chars.setColor(getElementTextContent(paperElement, "color").equals("true"));
        chars.setPageNumber(new Integer(getElementTextContent(paperElement, "pageNumber")));
        chars.setGlossy(getElementTextContent(paperElement, "glossy").equals("true"));
        chars.setIndex(new Integer(getElementTextContent(paperElement, "index")));

        paper.setChars(chars);
        return paper;
    }

    private static String getElementTextContent(Element element, String elementName)
    {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }
}
