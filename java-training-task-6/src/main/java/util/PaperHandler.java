package util;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import entity.Paper;

public class PaperHandler extends DefaultHandler
{
    private static final Logger logger = Logger.getLogger(PaperHandler.class);

    private List<Paper> papers;
    private Paper currentPaper;
    private Paper.Chars currentChars;
    private PaperEnum currentElement;
    private EnumSet<PaperEnum> tagsWithText;

    public PaperHandler()
    {
        papers = new ArrayList<>();
        tagsWithText = EnumSet.range(PaperEnum.TITLE, PaperEnum.INDEX);
    }

    public List<Paper> getPapers()
    {
        return papers;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrs)
    {
        switch (localName)
        {
            case "paper":
                currentPaper = new Paper();
                break;
            case "chars":
                currentChars = new Paper.Chars();
            default:
                PaperEnum temp = PaperEnum.valueOf(localName.toUpperCase());
                if (tagsWithText.contains(temp))
                {
                    currentElement = temp;
                }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName)
    {
        if ("paper".equals(localName))
        {
            papers.add(currentPaper);
        }
        if ("chars".equals(localName))
        {
            currentPaper.setChars(currentChars);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length)
    {
        String s = new String(ch, start, length).trim();
        if (currentElement != null)
        {
            switch (currentElement)
            {
                case TITLE:
                    currentPaper.setTitle(s);
                    break;
                case TYPE:
                    currentPaper.setType(s);
                    break;
                case MONTHLY:
                    currentPaper.setMonthly(s.equals("true"));
                    break;
                case COLOR:
                    currentChars.setColor(s.equals("true"));
                    break;
                case PAGENUMBER:
                    currentChars.setPageNumber(new Integer(s));
                    break;
                case GLOSSY:
                    currentChars.setGlossy(s.equals("true"));
                    break;
                case INDEX:
                    currentChars.setIndex(new Integer(s));
                    break;
                default:
                    logger.error("Unknown element");
                    throw new EnumConstantNotPresentException(currentElement.getDeclaringClass(),
                            currentElement.name());
            }
        }
        currentElement = null;
    }
}
