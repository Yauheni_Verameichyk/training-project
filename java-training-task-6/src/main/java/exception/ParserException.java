package exception;

public class ParserException extends Exception
{
    private static final long serialVersionUID = 1621472213969812095L;

    public ParserException()
    {
    }

    public ParserException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ParserException(String message)
    {
        super(message);
    }

    public ParserException(Throwable cause)
    {
        super(cause);
    }

}
