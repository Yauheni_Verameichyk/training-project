package exception;

public class UnknownParcerException extends Exception
{
    private static final long serialVersionUID = 1621472213969812095L;

    public UnknownParcerException()
    {
    }

    public UnknownParcerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public UnknownParcerException(String message)
    {
        super(message);
    }

    public UnknownParcerException(Throwable cause)
    {
        super(cause);
    }

}
