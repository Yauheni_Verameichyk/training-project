package exception;

public class InvalidXMLException extends Exception
{
    private static final long serialVersionUID = 1621472213969812095L;

    public InvalidXMLException()
    {
    }

    public InvalidXMLException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InvalidXMLException(String message)
    {
        super(message);
    }

    public InvalidXMLException(Throwable cause)
    {
        super(cause);
    }

}
