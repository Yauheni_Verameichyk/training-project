package exception;

public class XMLNotFoundException extends Exception
{
    private static final long serialVersionUID = 1621472213969812095L;

    public XMLNotFoundException()
    {
    }

    public XMLNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public XMLNotFoundException(String message)
    {
        super(message);
    }

    public XMLNotFoundException(Throwable cause)
    {
        super(cause);
    }

}
