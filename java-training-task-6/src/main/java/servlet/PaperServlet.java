package servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import entity.Paper;
import exception.InvalidXMLException;
import exception.ParserException;
import exception.UnknownParcerException;
import exception.XMLNotFoundException;
import util.PagingUtil;
import util.ParserUtil;
import util.ValidatorUtil;

@WebServlet("/PaperServlet")
@MultipartConfig
public class PaperServlet extends HttpServlet
{
    private static final long serialVersionUID = 9221457078543670780L;
    private static final Logger logger = Logger.getLogger(PaperServlet.class);

    public PaperServlet()
    {
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        response.setContentType("text/jsp");
        List<Paper> papers = (List<Paper>) request.getSession().getAttribute("paperList");
        Map<String, Object> pagingResult;
        if (request.getParameter("page") != null)
        {
            Integer page = Integer.parseInt(request.getParameter("page"));
            pagingResult = PagingUtil.doPaging(papers, page);
        }
        else
        {
            pagingResult = PagingUtil.doPaging(papers, null);
        }

        request.setAttribute("paperList", pagingResult.get("paperList"));
        request.setAttribute("pagesNumber", pagingResult.get("pagesNumber"));
        request.setAttribute("currentPage", pagingResult.get("currentPage"));
        request.getRequestDispatcher("/WEB-INF/jsp/parsingResult.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        Part filePart = request.getPart("XMLFile");
        InputStream fileContent = filePart.getInputStream();
        HttpSession session = request.getSession();
        String parserType = request.getParameter("parserType");
        List<Paper> papers;
        try
        {
            if (fileContent.available() == 0)
            {
                throw new XMLNotFoundException("XML file not found or is empty");
            }
            if (!ValidatorUtil.isValid(fileContent))
            {
                throw new InvalidXMLException("XML file isn't valid");
            }
            papers = ParserUtil.getPaperListFromXML(fileContent, parserType);
            session.setAttribute("parser", "parsed by " + parserType + "-parser");
            String paperList = "paperList";
            String pagesNumber = "pagesNumber";
            String currentPage = "currentPage";
            session.setAttribute(paperList, papers);
            Map<String, Object> pagingResult = PagingUtil.doPaging(papers, null);
            request.setAttribute(paperList, pagingResult.get(paperList));
            request.setAttribute(pagesNumber, pagingResult.get(pagesNumber));
            request.setAttribute(currentPage, pagingResult.get(currentPage));
            request.getRequestDispatcher("/WEB-INF/jsp/parsingResult.jsp").forward(request, response);
        }
        catch (ParserException | UnknownParcerException | XMLNotFoundException | InvalidXMLException e)
        {
            logger.error(e.getMessage(), e);
            request.setAttribute("error", e.getMessage());
            request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
        }
    }
}
