package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Test;

import entity.Paper;
import exception.ParserException;

public class PaperStAXBuilderTest
{
    private final static String BASE_PATH = "src\\test\\resources\\xml\\";
    private final static String EVEN_NOT_XML = BASE_PATH + "evenNotXML.txt";
    private final static String PAPER_WITH_TWO_ELEMENT = "src\\test\\resources\\xml\\papersWithTwoElements.xml";

    @Test
    public void shouldBuildListPapers() throws ParserException, IOException
    {
        PaperStAXBuilder builder = new PaperStAXBuilder();
        builder.buildListPapers(TestUtil.readXML(PAPER_WITH_TWO_ELEMENT));
        List<Paper> papers = builder.getPapers();
        assertThat(papers, equalTo(TestUtil.getPapers()));
    }

    @Test
    public void shouldFailIfFileIncorrect() throws ParserException, IOException
    {
        try
        {
            InputStream fileContent = new FileInputStream(EVEN_NOT_XML);
            PaperStAXBuilder builder = new PaperStAXBuilder();
            builder.buildListPapers(fileContent);
            fail();
        }
        catch (ParserException e)
        {
            assertThat(e.getMessage(), equalTo("StAX parsing error"));
        }
    }
}
