package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import exception.ParserException;

public class PagingUtilTest
{

    @Test
    public void shouldReturnMapWithPagesNumberCurrentPageAndPaperList()
        throws ParserException, NumberFormatException, IOException
    {
        Map<String, Object> actual = PagingUtil.doPaging(TestUtil.getPapers(), null);
        Map<String, Object> expected = new HashMap<>();
        expected.put("currentPage", 1);
        expected.put("pagesNumber", 1);
        expected.put("paperList", TestUtil.getPapers());
        assertThat(actual, equalTo(expected));
    }
}
