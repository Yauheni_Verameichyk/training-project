package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Test;

import entity.Paper;
import exception.ParserException;

public class PaperSAXBuilderTest
{
    private final static String BASE_PATH = "src\\test\\resources\\xml\\";
    private final static String EVEN_NOT_XML = BASE_PATH + "evenNotXML.txt";
    private final static String PAPER_WITH_TWO_ELEMENT = BASE_PATH + "papersWithTwoElements.xml";

    @Test
    public void shouldBuildListPapers() throws ParserException, IOException
    {
        PaperSAXBuilder builder = new PaperSAXBuilder();
        builder.buildListPapers(TestUtil.readXML(PAPER_WITH_TWO_ELEMENT));
        List<Paper> papers = builder.getPapers();
        assertThat(papers, equalTo(TestUtil.getPapers()));
    }

    @Test
    public void shouldFailIfFileIncorrect() throws ParserException, IOException
    {
        try
        {
            InputStream fileContent = new FileInputStream(EVEN_NOT_XML);
            PaperSAXBuilder builder = new PaperSAXBuilder();
            builder.buildListPapers(fileContent);
            fail();
        }
        catch (ParserException e)
        {
            assertThat(e.getMessage(), equalTo("SAX-parser error"));
        }
    }
}
