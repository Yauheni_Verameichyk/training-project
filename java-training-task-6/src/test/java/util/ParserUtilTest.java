package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import entity.Paper;
import exception.ParserException;
import exception.UnknownParcerException;

public class ParserUtilTest
{
    private final static String PAPER_WITH_TWO_ELEMENT = "src\\test\\resources\\xml\\papersWithTwoElements.xml";

    @Test
    public void shouldReturnPaperListParsedBySAXParser()
        throws ParserException, NumberFormatException, IOException, UnknownParcerException
    {
        List<Paper> paperList = ParserUtil.getPaperListFromXML(TestUtil.readXML(PAPER_WITH_TWO_ELEMENT), "SAX");
        assertThat(paperList, equalTo(TestUtil.getPapers()));
    }

    @Test
    public void shouldReturnPaperListParsedByStAXParser()
        throws ParserException, NumberFormatException, IOException, UnknownParcerException
    {
        List<Paper> paperList = ParserUtil.getPaperListFromXML(TestUtil.readXML(PAPER_WITH_TWO_ELEMENT), "StAX");
        assertThat(paperList, equalTo(TestUtil.getPapers()));
    }

    @Test
    public void shouldReturnPaperListParsedByDOMParser()
        throws ParserException, NumberFormatException, IOException, UnknownParcerException
    {
        List<Paper> paperList = ParserUtil.getPaperListFromXML(TestUtil.readXML(PAPER_WITH_TWO_ELEMENT), "DOM");
        assertThat(paperList, equalTo(TestUtil.getPapers()));
    }
}
