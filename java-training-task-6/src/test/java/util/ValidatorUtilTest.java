package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class ValidatorUtilTest
{
    private final static String BASE_PATH = "src\\test\\resources\\xml\\";
    private final static String INVALID_XML = BASE_PATH + "invalidXML.xml";
    private final static String PAPER_WITH_TWO_ELEMENT = BASE_PATH + "papersWithTwoElements.xml";

    @Test
    public void shouldReturnTrueIfXMLValid()
    {
        assertThat(ValidatorUtil.isValid(TestUtil.readXML(PAPER_WITH_TWO_ELEMENT)), equalTo(true));
    }

    @Test
    public void shouldReturnFalseIfXMLNotValid()
    {
        assertThat(ValidatorUtil.isValid(TestUtil.readXML(INVALID_XML)), equalTo(false));
    }
}
