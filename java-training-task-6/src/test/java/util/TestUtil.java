package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import entity.Paper;

public class TestUtil
{
    public static List<Paper> getPapers()
    {
        List<Paper> papers = new ArrayList<>();
        papers.add(getPaperPlanet());
        papers.add(getPaperSB());
        return papers;
    }

    public static Paper getPaperPlanet()
    {
        Paper paper = new Paper();
        paper.setTitle("Planet");
        paper.setType("magazine");
        paper.setMonthly(true);
        Paper.Chars chars = new Paper.Chars();
        chars.setColor(true);
        chars.setPageNumber(60);
        chars.setGlossy(true);
        chars.setIndex(234567);
        paper.setChars(chars);
        return paper;
    }

    public static Paper getPaperSB()
    {
        Paper paper = new Paper();
        paper.setTitle("SB");
        paper.setType("newspaper");
        paper.setMonthly(false);
        Paper.Chars chars = new Paper.Chars();
        chars.setColor(true);
        chars.setPageNumber(20);
        chars.setGlossy(false);
        chars.setIndex(234567);
        paper.setChars(chars);
        return paper;
    }

    public static InputStream readXML(String filePath)
    {
        File file = new File(filePath);
        InputStream input = null;
        try
        {
            input = new FileInputStream(file);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return input;
    }
}
