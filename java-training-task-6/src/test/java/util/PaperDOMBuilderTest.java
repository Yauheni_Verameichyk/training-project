package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import org.junit.Test;

import entity.Paper;
import exception.ParserException;

public class PaperDOMBuilderTest
{
    private final static String BASE_PATH = "src\\test\\resources\\xml\\";
    private final static String EVEN_NOT_XML = BASE_PATH + "evenNotXML.txt";
    private final static String PAPER_WITH_TWO_ELEMENT = BASE_PATH + "papersWithTwoElements.xml";

    @Test
    public void shouldBuildListPapers() throws ParserException
    {
        PaperDOMBuilder builder = new PaperDOMBuilder();
        builder.buildListPapers(TestUtil.readXML(PAPER_WITH_TWO_ELEMENT));
        List<Paper> papers = builder.getPapers();
        assertThat(papers, equalTo(TestUtil.getPapers()));
    }

    @Test
    public void shouldFailIfFileIncorrect() throws ParserException, FileNotFoundException
    {
        try
        {
            InputStream fileContent = new FileInputStream(EVEN_NOT_XML);
            PaperDOMBuilder builder = new PaperDOMBuilder();
            builder.buildListPapers(fileContent);
            fail();
        }
        catch (ParserException e)
        {
            assertThat(e.getMessage(), equalTo("DOM-parser failure"));
        }
    }
}
