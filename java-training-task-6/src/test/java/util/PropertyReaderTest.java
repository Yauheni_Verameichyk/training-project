package util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Test;

import exception.ParserException;

public class PropertyReaderTest
{

    @Test
    public void shouldReturnPropertyFromFile()
        throws ParserException, NumberFormatException, IOException
    {
        assertThat(PropertyReader.getProperty("recordsPerPage"), equalTo("5"));
    }
}
