'use strict';

/**
 * Development config
 */
module.exports = function(_path) {

  return {
    context: _path,
    debug: false,
    devtool: 'eval',
    devServer: {
      contentBase: './dist',
      info: true,
      hot: false,
      inline: true
    }
  }
};