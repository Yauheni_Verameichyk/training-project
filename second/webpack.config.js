let webpack = require('webpack')

module.exports = {
  devtool: 'inline-sourcemap',
  context: __dirname,
  entry: {

    'app': './src/main.ts'
  },
  output: {
    path: __dirname,
    filename: 'my-first-webpack.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: 'ts-loader',
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
}