package exception;

public class IncorrectNumberException extends Exception
{
    private static final long serialVersionUID = 1621472213969812095L;

    public IncorrectNumberException()
    {
    }

    public IncorrectNumberException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public IncorrectNumberException(String message)
    {
        super(message);
    }

    public IncorrectNumberException(Throwable cause)
    {
        super(cause);
    }

}
