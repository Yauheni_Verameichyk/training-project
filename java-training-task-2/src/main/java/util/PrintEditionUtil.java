package util;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import entity.PrintEdition;
import entity.enumeration.Genre;
import entity.enumeration.SortOrder;

public class PrintEditionUtil
{
    public static void sortByPagesNumber(LinkedList<PrintEdition> list, SortOrder order)
    {
        PrintEditionComparator comp = new PrintEditionComparator(order);
        Collections.sort(list, comp);
    }

    public static int getPagesSumInGenre(LinkedList<PrintEdition> list, Genre genre)
    {
        Iterator<PrintEdition> listIterator = list.iterator();
        int pagesSum = 0;
        while (listIterator.hasNext())
        {
            PrintEdition currentPrintEdition = listIterator.next();
            if (currentPrintEdition.getGenre() == genre)
            {
                pagesSum += currentPrintEdition.getPagesNumber();
            }
        }
        return pagesSum;
    }

    public static LinkedList<PrintEdition> getPrintEditionsByGenre(LinkedList<PrintEdition> list, Genre genre)
    {
        Iterator<PrintEdition> listIterator = list.iterator();
        LinkedList<PrintEdition> sortedList = new LinkedList<PrintEdition>();
        while (listIterator.hasNext())
        {
            PrintEdition currentPrintEdition = listIterator.next();
            if (currentPrintEdition.getGenre() == genre)
            {
                sortedList.add(currentPrintEdition);
            }
        }
        return sortedList;
    }

    public static LinkedList<PrintEdition> getPrintEditionsByPageNumber(LinkedList<PrintEdition> list, int minPageNumber,
        int maxPageNumber)
    {
        Iterator<PrintEdition> listIterator = list.iterator();
        LinkedList<PrintEdition> sortedList = new LinkedList<PrintEdition>();
        while (listIterator.hasNext())
        {
            PrintEdition currentPrintEdition = listIterator.next();
            if (currentPrintEdition.getPagesNumber() >= minPageNumber && currentPrintEdition.getPagesNumber() <= maxPageNumber)
            {
                sortedList.add(currentPrintEdition);
            }
        }
        return sortedList;
    }

    public static LinkedList<PrintEdition> getPrintEditionsByName(LinkedList<PrintEdition> list, String name)
    {
        Iterator<PrintEdition> listIterator = list.iterator();
        LinkedList<PrintEdition> sortedList = new LinkedList<PrintEdition>();
        while (listIterator.hasNext())
        {
            PrintEdition currentPrintEdition = listIterator.next();
            if (name.equals(currentPrintEdition.getName()))
            {
                sortedList.add(currentPrintEdition);
            }
        }
        return sortedList;
    }

    public static LinkedList<PrintEdition> getPrintEditionsByPageNumberGenreAndName(LinkedList<PrintEdition> list,
        int minPageNumber,
        int maxPageNumber, Genre genre, String name)
    {
        LinkedList<PrintEdition> sortedList = getPrintEditionsByPageNumber(list, minPageNumber, maxPageNumber);
        sortedList = getPrintEditionsByGenre(sortedList, genre);
        sortedList = getPrintEditionsByName(sortedList, name);
        return sortedList;
    }
}
