package util;

import java.util.Comparator;

import entity.PrintEdition;
import entity.enumeration.SortOrder;

public class PrintEditionComparator implements Comparator<PrintEdition>
{
    private SortOrder order;

    public PrintEditionComparator(SortOrder order)
    {
        this.order = order;
    }

    @Override
    public int compare(PrintEdition one, PrintEdition two)
    {
        if (order == SortOrder.DESCEND)
        {
            return two.getPagesNumber() - one.getPagesNumber();
        }
        else
        {
            return one.getPagesNumber() - two.getPagesNumber();
        }
    }
}
