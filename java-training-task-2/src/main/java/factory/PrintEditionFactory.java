package factory;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import entity.Book;
import entity.ChildBook;
import entity.Magazine;
import entity.Newspaper;
import entity.PrintEdition;
import entity.Schoolbook;
import entity.enumeration.Area;
import entity.enumeration.Genre;
import entity.enumeration.TargetGender;
import exception.IncorrectNumberException;

public class PrintEditionFactory
{
    private static final Logger LOGGER = Logger.getLogger(PrintEditionFactory.class);

    private static final String PAGES_NUMBER_ERROR_MESSAGE = "Pages number cannot be less than zero";
    private static final String AGE_ERROR_MESSAGE = "Age cannot be less than zero";

    public static Book createBook(int pagesNumber, Genre genre, String author, int editionYear, String name)
        throws IncorrectNumberException
    {
        Book book = new Book();
        setCommonFields(book, pagesNumber, genre, name);
        book.setAuthor(author);
        book.setEditionYear(editionYear);
        book.setName(name);
        return book;
    }

    public static ChildBook createChildBook(int pagesNumber, Genre genre, String author, int editionYear, String name,
        int recommendedAge) throws IncorrectNumberException
    {
        ChildBook book = new ChildBook();
        setCommonFields(book, pagesNumber, genre, name);
        book.setAuthor(author);
        book.setEditionYear(editionYear);
        if (recommendedAge >= 0)
        {
            book.setRecomendedAge(recommendedAge);
        }
        else
        {
            LOGGER.error(AGE_ERROR_MESSAGE);
            throw new IncorrectNumberException(AGE_ERROR_MESSAGE);
        }
        return book;
    }

    public static Schoolbook createSchoolbook(int pagesNumber, Genre genre, String author, int editionYear, String name,
        ArrayList<String> usedSources) throws IncorrectNumberException
    {
        Schoolbook book = new Schoolbook();
        setCommonFields(book, pagesNumber, genre, name);
        book.setAuthor(author);
        book.setEditionYear(editionYear);
        book.setUsedSources(usedSources);
        return book;
    }

    public static Magazine createMagazine(int pagesNumber, Genre genre, String name, int daysBetweenPublishment,
        TargetGender targetGender) throws IncorrectNumberException
    {
        Magazine magazine = new Magazine();
        setCommonFields(magazine, pagesNumber, genre, name);
        magazine.setDaysBetweenPublishment(daysBetweenPublishment);
        magazine.setTargetGender(targetGender);
        return magazine;
    }

    public static Newspaper createNewspaper(int pagesNumber, Genre genre, String name, int daysBetweenPublishment,
        Area coverageArea) throws IncorrectNumberException
    {
        Newspaper newspaper = new Newspaper();
        setCommonFields(newspaper, pagesNumber, genre, name);
        newspaper.setDaysBetweenPublishment(daysBetweenPublishment);
        newspaper.setCoverageArea(coverageArea);
        return newspaper;
    }

    private static void setCommonFields(PrintEdition printEdition, int pagesNumber, Genre genre, String name)
        throws IncorrectNumberException
    {
        if (pagesNumber >= 0)
        {
            printEdition.setPagesNumber(pagesNumber);
        }
        else
        {
            LOGGER.error(PAGES_NUMBER_ERROR_MESSAGE);
            throw new IncorrectNumberException(PAGES_NUMBER_ERROR_MESSAGE);
        }
        printEdition.setGenre(genre);
        printEdition.setName(name);
    }
}
