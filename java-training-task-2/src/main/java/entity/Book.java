package entity;

public class Book extends PrintEdition
{
    private String author;
    private int editionYear;

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public int getEditionYear()
    {
        return editionYear;
    }

    public void setEditionYear(int editionYear)
    {
        this.editionYear = editionYear;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((author == null) ? 0 : author.hashCode());
        result = prime * result + editionYear;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Book other = (Book) obj;
        if (author == null)
        {
            if (other.author != null)
                return false;
        }
        else if (!author.equals(other.author))
            return false;
        if (editionYear != other.editionYear)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return super.toString() + "Book [author=" + author + ", editionYear=" + editionYear + "]";
    }
}
