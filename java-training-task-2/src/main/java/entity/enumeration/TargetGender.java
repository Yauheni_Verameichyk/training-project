package entity.enumeration;

public enum TargetGender
{
    MALE, FEMALE, ALL
}
