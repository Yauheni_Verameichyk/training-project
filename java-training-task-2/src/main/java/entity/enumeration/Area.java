package entity.enumeration;

public enum Area
{
    COUNTRY, REGION, CITY
}
