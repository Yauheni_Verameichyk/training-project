package entity.enumeration;

public enum SortOrder
{
    ASCEND, DESCEND
}
