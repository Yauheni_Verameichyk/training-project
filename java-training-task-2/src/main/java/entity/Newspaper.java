package entity;

import entity.enumeration.Area;

public class Newspaper extends Periodical
{
    private Area coverageArea;

    public Area getCoverageArea()
    {
        return coverageArea;
    }

    public void setCoverageArea(Area coverage)
    {
        this.coverageArea = coverage;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((coverageArea == null) ? 0 : coverageArea.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Newspaper other = (Newspaper) obj;
        if (coverageArea != other.coverageArea)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return super.toString() + "Newspaper [coverageArea=" + coverageArea + "]";
    }
}
