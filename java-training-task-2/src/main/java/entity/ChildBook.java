package entity;

public class ChildBook extends Book
{
    private int recommendedAge;

    public int getRecomendedAge()
    {
        return recommendedAge;
    }

    public void setRecomendedAge(int recomendedAge)
    {
        this.recommendedAge = recomendedAge;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + recommendedAge;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChildBook other = (ChildBook) obj;
        if (recommendedAge != other.recommendedAge)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return super.toString() + "ChildBook [recomendedAge=" + recommendedAge + "]";
    }

}
