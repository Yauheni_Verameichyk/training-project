package entity;

import entity.enumeration.TargetGender;

public class Magazine extends Periodical
{
    private TargetGender targetGender;

    public TargetGender getTargetGender()
    {
        return targetGender;
    }

    public void setTargetGender(TargetGender genderTarget)
    {
        this.targetGender = genderTarget;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((targetGender == null) ? 0 : targetGender.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Magazine other = (Magazine) obj;
        if (targetGender != other.targetGender)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return super.toString() + "Magazine [targetGender=" + targetGender + "]";
    }
}
