package entity;

public abstract class Periodical extends PrintEdition
{
    private int daysBetweenPublishment;

    public int getDaysBetweenPublishment()
    {
        return daysBetweenPublishment;
    }

    public void setDaysBetweenPublishment(int daysBetweenPublishment)
    {
        this.daysBetweenPublishment = daysBetweenPublishment;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + daysBetweenPublishment;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Periodical other = (Periodical) obj;
        if (daysBetweenPublishment != other.daysBetweenPublishment)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return super.toString() + "Periodical [daysBetweenPublishment=" + daysBetweenPublishment + "]";
    }
}
