package entity;

import java.util.ArrayList;

public class Schoolbook extends Book
{
    private ArrayList<String> usedSources;

    public Schoolbook()
    {
    }

    public ArrayList<String> getUsedSources()
    {
        return usedSources;
    }

    public void setUsedSources(ArrayList<String> usedSources)
    {
        this.usedSources = usedSources;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((usedSources == null) ? 0 : usedSources.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Schoolbook other = (Schoolbook) obj;
        if (usedSources == null)
        {
            if (other.usedSources != null)
                return false;
        }
        else if (!usedSources.equals(other.usedSources))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return super.toString() + "Schoolbook [usedSources=" + usedSources + "]";
    }
}
