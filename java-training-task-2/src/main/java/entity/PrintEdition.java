package entity;

import entity.enumeration.Genre;

public abstract class PrintEdition
{
    private int pagesNumber;
    private Genre genre;
    private String name;

    public int getPagesNumber()
    {
        return pagesNumber;
    }

    public void setPagesNumber(int pagesNumber)
    {
        this.pagesNumber = pagesNumber;
    }

    public Genre getGenre()
    {
        return genre;
    }

    public void setGenre(Genre genre)
    {
        this.genre = genre;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((genre == null) ? 0 : genre.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + pagesNumber;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PrintEdition other = (PrintEdition) obj;
        if (genre != other.genre)
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (pagesNumber != other.pagesNumber)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "PrintEdition [pagesNumber=" + pagesNumber + ", genre=" + genre + ", name=" + name + "]";
    }

}
