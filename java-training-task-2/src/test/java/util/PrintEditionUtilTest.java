package util;

import static factory.PrintEditionFactory.createBook;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static util.PrintEditionUtil.getPrintEditionsByGenre;
import static util.PrintEditionUtil.getPrintEditionsByName;
import static util.PrintEditionUtil.getPrintEditionsByPageNumber;
import static util.PrintEditionUtil.getPrintEditionsByPageNumberGenreAndName;
import static util.PrintEditionUtil.getPagesSumInGenre;
import static util.PrintEditionUtil.sortByPagesNumber;

import java.util.LinkedList;

import org.junit.Test;

import entity.Book;
import entity.PrintEdition;
import entity.enumeration.Genre;
import entity.enumeration.SortOrder;
import exception.IncorrectNumberException;

public class PrintEditionUtilTest
{
    @Test
    public void shouldReturnPagesSumInGenre() throws IncorrectNumberException
    {
        LinkedList<PrintEdition> list = getPrintEditionList();
        int expected = 157;
        int actual = getPagesSumInGenre(list, Genre.HISTORY);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnListWhereGenreHistory() throws IncorrectNumberException
    {
        LinkedList<PrintEdition> list = getPrintEditionList();
        LinkedList<PrintEdition> expected = getPrintEditionListWithHistoryGenre();
        assertThat(getPrintEditionsByGenre(list, Genre.HISTORY), equalTo(expected));
    }

    @Test
    public void shouldReturnListWherePagesNumberInRange() throws IncorrectNumberException
    {
        LinkedList<PrintEdition> list = getPrintEditionList();
        LinkedList<PrintEdition> expected = getPrintEditionListWithPrintNumberInRange();
        assertThat(getPrintEditionsByPageNumber(list, 100, 200), equalTo(expected));
    }

    @Test
    public void shouldReturnListWhereGenreHystoryAndPagesNumberInRange() throws IncorrectNumberException
    {
        LinkedList<PrintEdition> actual = getPrintEditionList();
        LinkedList<PrintEdition> expected = new LinkedList<PrintEdition>();
        Book book4 = new Book();
        book4.setPagesNumber(144);
        book4.setGenre(Genre.HISTORY);
        book4.setAuthor("Somebody");
        book4.setEditionYear(2017);
        book4.setName("Book4");
        expected.add(book4);
        assertThat(getPrintEditionsByPageNumberGenreAndName(actual, 100, 200, Genre.HISTORY, "Book4"), equalTo(expected));
    }

    @Test
    public void shouldReturnListWhereNameBook4() throws IncorrectNumberException
    {
        LinkedList<PrintEdition> actual = getPrintEditionList();
        LinkedList<PrintEdition> expected = new LinkedList<PrintEdition>();
        Book book4 = new Book();
        book4.setPagesNumber(144);
        book4.setGenre(Genre.HISTORY);
        book4.setAuthor("Somebody");
        book4.setEditionYear(2017);
        book4.setName("Book4");
        expected.add(book4);
        assertThat(getPrintEditionsByName(actual, "Book4"), equalTo(expected));
    }

    @Test
    public void shouldReturnSortedListInDescendOrder() throws IncorrectNumberException
    {
        LinkedList<PrintEdition> list = getPrintEditionList();
        LinkedList<PrintEdition> expected = getSortedPrintEditionList();
        sortByPagesNumber(list, SortOrder.DESCEND);
        assertThat(list, equalTo(expected));
    }

    private static LinkedList<PrintEdition> getPrintEditionListWithHistoryGenre() throws IncorrectNumberException
    {
        Book book1 = createBook(13, Genre.HISTORY, "Somebody", 2017, "Book1");
        Book book4 = createBook(144, Genre.HISTORY, "Somebody", 2017, "Book4");
        book4.setPagesNumber(144);
        LinkedList<PrintEdition> list = new LinkedList<PrintEdition>();
        list.add(book1);
        list.add(book4);
        return list;
    }

    private static LinkedList<PrintEdition> getPrintEditionListWithPrintNumberInRange() throws IncorrectNumberException
    {
        Book book3 = createBook(143, Genre.NATURAL_SCIENCES, "Somebody", 2017, "Book3");
        Book book4 = createBook(144, Genre.HISTORY, "Somebody", 2017, "Book4");
        LinkedList<PrintEdition> list = new LinkedList<PrintEdition>();
        list.add(book3);
        list.add(book4);
        return list;
    }

    private static LinkedList<PrintEdition> getSortedPrintEditionList() throws IncorrectNumberException
    {
        Book book1 = createBook(13, Genre.HISTORY, "Somebody", 2017, "Book1");
        Book book2 = createBook(455, Genre.FANTASY, "Somebody", 2017, "Book2");
        Book book3 = createBook(143, Genre.NATURAL_SCIENCES, "Somebody", 2017, "Book3");
        Book book4 = createBook(144, Genre.HISTORY, "Somebody", 2017, "Book4");
        LinkedList<PrintEdition> list = new LinkedList<PrintEdition>();
        list.add(book2);
        list.add(book4);
        list.add(book3);
        list.add(book1);
        return list;
    }

    private static LinkedList<PrintEdition> getPrintEditionList() throws IncorrectNumberException
    {
        Book book1 = createBook(13, Genre.HISTORY, "Somebody", 2017, "Book1");
        Book book2 = createBook(455, Genre.FANTASY, "Somebody", 2017, "Book2");
        Book book3 = createBook(143, Genre.NATURAL_SCIENCES, "Somebody", 2017, "Book3");
        Book book4 = createBook(144, Genre.HISTORY, "Somebody", 2017, "Book4");
        LinkedList<PrintEdition> list = new LinkedList<PrintEdition>();
        list.add(book1);
        list.add(book2);
        list.add(book3);
        list.add(book4);
        return list;
    }
}
