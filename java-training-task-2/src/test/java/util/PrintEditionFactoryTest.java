package util;

import static factory.PrintEditionFactory.createBook;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;

import entity.enumeration.Genre;
import exception.IncorrectNumberException;

public class PrintEditionFactoryTest
{
    @Test
    public void shouldFailIfPagesNumberLessThanZero() throws IncorrectNumberException
    {
        try
        {
            createBook(-8, Genre.HISTORY, "Somebody", 2017, "Book");
            fail();
        }
        catch (IncorrectNumberException e)
        {
            assertThat(e.getMessage(), equalTo("Pages number cannot be less than zero"));
        }
    }
}
